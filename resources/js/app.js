require("./bootstrap");


/* import Vue from "../../public/js/vue.min.js"; */

import App from './App';
import router from './router'
import navigation_dashboard from './includes/Navigation';
import siderbar_dashboard from './includes/Sidebar';
import footer_dashboard from './includes/Footer';

window.Vue = require("vue");

Vue.component('navigation-dashboard', navigation_dashboard)
Vue.component('sidebar-dashboard', siderbar_dashboard)
Vue.component('footer-dashboard', footer_dashboard)


const app = new Vue({
    el: "#app",
    router,
    components: {
        App
    },
    template: '<App/>'
});


// const app = new Vue({
//     el: "#app",
//     data:{
//         menu:0
//     }
// });
