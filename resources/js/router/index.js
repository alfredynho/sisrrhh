import Vue from 'vue';
import Router from 'vue-router';

import dashboard from "../components/Dashboard.vue";
import category from "../components/Category.vue";
import product from "../components/Product.vue";
import profile from "../components/Profile.vue";
import client from "../components/Client.vue";
import provider from "../components/Providers";
import chatbot from "../components/Chatbot";
import error from "../components/Error";
import blog from "../components/Blog";

Vue.use(Router)

export default new Router({
  mode: 'history',
  base:process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: dashboard
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: dashboard
    },
    {
      path: '/categoria',
      name: 'categoria',
      component: category
    },
    {
      path: '/producto',
      name: 'producto',
      component: product
    },
    {
      path: '/chatbot',
      name: 'chatbot',
      component: chatbot
    },
    {
      path: '/perfil',
      name: 'perfil',
      component: profile
    },
    { path: '*',
      name: 'error',
      component: error
    },
    {
      path:'/blog',
      name:'blog',
      component: blog
    }
  ]
})
