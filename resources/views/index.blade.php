@extends('layouts.frontend')

@section('extracss')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection('extracss')

@section('content')

@include('frontend.includes.slides')

<div class="section small_pt">
    <div class="container">
        <div class="row align-items-center">
        	<div class="col-xl-5 col-lg-6 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                <div class="heading_s1">
                    <h2>¿Que necesita realizar?</h2>
                </div>
                <p>El Sistema de Administración de Personal Recursos Humanos le permitira:</p>
            </div>
        	<div class="col-xl-7 col-lg-6 animation" data-animation="fadeInUp" data-animation-delay="0.3s">
            	<div class="carousel_slider owl-carousel owl-theme nav_style4" data-loop="true" data-margin="30" data-dots="false" data-nav="true" data-autoplay="true" data-autoplay-timeout="3000" data-responsive='{"0":{"items": "2"}, "767":{"items": "3"}, "991":{"items": "2"}, "1199":{"items": "3"}}'>
                    <div class="items">
                        <div class="single_categories cat_style1 bg_blue">
                            <a href="#">
                                <center><img src="{{ asset('frontend/images/vacaciones_rh.png') }}" style="width:70px;height:70px;" alt="VACACIONES"></center> <br>
                                <strong>VACACIONES</strong>
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <div class="single_categories cat_style1 bg_orange">
                            <a href="#">
                                <center><img src="{{ asset('frontend/images/form_rh.png') }}" style="width:70px;height:70px;" alt="FORMULARIOS"></center> <br>
                                <strong>FORMULARIOS</strong>
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <div class="single_categories cat_style1 bg_danger">
                            <a href="#">
                                <center><img src="{{ asset('frontend/images/cursos_rh.png') }}" style="width:70px;height:70px;" alt="CURSOS"></center> <br>
                                <strong>CURSOS</strong>
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <div class="single_categories cat_style1 bg_lightgreen">
                            <a href="#">
                                <center><img src="{{ asset('frontend/images/registro_rh.png') }}" style="width:70px;height:70px;" alt="REGISTRO"></center> <br>
                                <strong>REGISTRO</strong>
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <div class="single_categories cat_style1 bg_lightnavy">
                            <a href="#">
                                <center><img src="{{ asset('frontend/images/capacitacion_rh.png') }}" style="width:70px;height:70px;" alt="CAPACITACIÓN"></center> <br>
                                <strong>CAPACITACIÓN</strong>
                            </a>
                        </div>
                    </div>
                    <div class="items">
                        <div class="single_categories cat_style1 bg_blue">
                            <a href="#">
                                <center><img src="{{ asset('frontend/images/informacion_rh.png') }}" style="width:70px;height:70px;" alt="INFORMACION"></center> <br>
                                <strong>INFORMACION</strong>
                            </a>
                        </div>
                    </div>

                    <div class="items">
                        <div class="single_categories cat_style1 bg_orange">
                            <a href="#">
                                <center><img src="{{ asset('frontend/images/licencias_rh.png') }}" style="width:70px;height:70px;" alt="LICENCIAS"></center> <br>
                                <strong>LICENCIAS</strong>
                            </a>
                        </div>
                    </div>
                                   
                </div>
            </div>
        </div>
    </div>
</div> 

@include('frontend.includes.formulario_front')

<div class="section overflow_hide background_bg overlay_bg_80" data-img-src="{{ asset('frontend/images/banner_header.png') }}">
    <div class="container">
    	<div class="row justify-content-center">
        	<div class="col-md-8 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
            	<div class="heading_s1 heading_light text-center">
                    <h2>Lo que nuestros estudiantes dicen</h2>
                </div>
            </div>
        </div>

        @if(count($memories) >= 1)
            <div class="row">
                <div class="col-12 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                    <div class="testimonial_slider testimonial_section testimonial_style3 carousel_slider owl-carousel owl-theme nav_style3 text_white" data-dots="false" data-nav="true" data-margin="10" data-center="true" data-loop="true" data-autoplay="true" data-items='1'>
                        @foreach ($memories as $me)
                            <div class="testimonial_box">
                                <div class="testimonial_desc">
                                    <p>{{ $me->message }}</p>
                                </div>
                                <div class="author_info">
                                    <div class="author_img">
                                        <img src="{{ asset('frontend/images/user.png') }}" alt="RRHH" />
                                    </div>
                                    <div class="author_name">
                                        <h6>{{ $me->author }}</h6>
                                        <span>{{ $me->grade }}</span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @else
            <div class="alert alert-primary text-center" role="alert">
                Sistema de Administración de Personal Recursos Humanos
            </div>
        @endif
    </div>
</div>

<div class="section pb_70">
	<div class="container">
    	<div class="row justify-content-center">
        	<div class="col-lg-6 col-md-8 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
            	<div class="heading_s1 text-center">
                	<h2>Publicaciones RRHH</h2>
                </div>
                <p class="text-center">Sistema de Administración de Personal Recursos Humanos</p>
            </div>
        </div>
        <div class="row">

            @foreach($blogs as $key => $bg)
                <div class="col-lg-4 col-md-6">
                    <div class="courses_box radius_all_10 box_shadow1 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                        <div class="courses_img"> 
                            <a href="#"><img src="{{ asset('frontend/images/GENERAL.png') }}" alt="course_img1"/></a>
                        </div>
                        <div class="courses_info">
                            <h5 class="courses_title"><a href="#">{{ $bg->title }}</a></h5>
                            <div class="courses_teacher"> 
                                <a href="#"><img src="{{ asset('frontend/images/user.png') }}" alt="user1"><span>INSO</span></a> 
                            </div>
                            <div class="courses_footer">
                                <div class="courses_price"><span>VER MAS</span> </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div> 

@endsection


@section('extrajs')

@endsection('extrajs')
    