<link rel="stylesheet" href="{{ asset('admin/css/backend-plugin.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/css/backend.css?v=1.0.0') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/line-awesome/dist/line-awesome/css/line-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/remixicon/fonts/remixicon.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/@icon/dripicons/dripicons.css') }}">

<link rel='stylesheet' href="{{ asset('admin/vendor/fullcalendar/core/main.css') }}" />
<link rel='stylesheet' href="{{ asset('admin/vendor/fullcalendar/daygrid/main.css') }}" />
<link rel='stylesheet' href="{{ asset('admin/vendor/fullcalendar/timegrid/main.css')}}" />
<link rel='stylesheet' href="{{ asset('admin/vendor/fullcalendar/list/main.css') }}" />
<link rel="stylesheet" href="{{ asset('admin/vendor/mapbox/mapbox-gl.css') }}">  
