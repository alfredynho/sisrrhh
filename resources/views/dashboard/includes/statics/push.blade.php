    @if (Session::has('create'))
        <script>
            iziToast.success({
                icon: 'ti-check-box',
                title: 'Creado',
                message: '{{ Session::get('create') }}'
            });
        </script>

    @elseif(Session::has('update'))
        <script>
            iziToast.warning({
                icon: 'ti-receipt',
                title: 'Modificado',
                message: '{{ Session::get('update') }}'
            });
        </script>

    @elseif(Session::has('delete'))
        <script>
            iziToast.error({
                icon : 'ti-trash',
                title: 'Eliminado',
                message: '{{ Session::get('delete') }}'
            });
        </script>

    @elseif(Session::has('chatbot'))
        <script>
            iziToast.info({
                icon: 'ti-themify-favicon',
                title: 'Modificado',
                message: '{{ Session::get('chatbot') }}'
            });
        </script>

    @elseif(count($errors)>0)
        @foreach ($errors->all() as $e)
            <script>
                iziToast.error({
                    title: 'Errores',
                    message: '{{ $e }}'
                });
            </script>
        @endforeach
    @endif
