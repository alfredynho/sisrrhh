<div class="navbar-wrapper  ">
	<div class="navbar-content scroll-div " >

		<div class="">
			@include('dashboard.includes.user')
		</div>

		<ul class="nav pcoded-inner-navbar ">
			<li class="nav-item pcoded-menu-caption">
				<label>Menu de Opciones</label>
			</li>

			<li class="nav-item"><a href="{{ route('dashboard') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a></li>

			<li class="nav-item"><a href="{{ route('dashboard.unidad.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-server"></i></span><span class="pcoded-mtext">Unidades</span></a></li>
			<li class="nav-item"><a href="{{ route('dashboard.profesion.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-flag"></i></span><span class="pcoded-mtext">Profesiones</span></a></li>
			<li class="nav-item"><a href="{{ route('dashboard.cargo.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-feather"></i></span><span class="pcoded-mtext">Cargos</span></a></li>
			<li class="nav-item"><a href="{{ route('dashboard.personal.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-flag"></i></span><span class="pcoded-mtext">Funcionario</span></a></li>
			<li class="nav-item"><a href="{{ route('dashboard.ausencias.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file"></i></span><span class="pcoded-mtext">Ausencias</span></a></li>
			<li class="nav-item"><a href="{{ route('dashboard.estudios.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-edit-1"></i></span><span class="pcoded-mtext">Estudios</span></a></li>
			<li class="nav-item"><a href="" class="nav-link "><span class="pcoded-micon"><i class="feather icon-disc"></i></span><span class="pcoded-mtext">Vacaciones</span><span class="pcoded-badge badge badge-success">EN PROCESO...</span></a></li>
			<li class="nav-item"><a href="" class="nav-link "><span class="pcoded-micon"><i class="feather icon-watch"></i></span><span class="pcoded-mtext">Horas Extra</span><span class="pcoded-badge badge badge-success">EN PROCESO...</span></a></li>
			<li class="nav-item"><a href="" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-plus"></i></span><span class="pcoded-mtext">Maternidad</span><span class="pcoded-badge badge badge-success">EN PROCESO...</span></a></li>
			<li class="nav-item"><a href="" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file"></i></span><span class="pcoded-mtext">Lactancia</span><span class="pcoded-badge badge badge-success">EN PROCESO...</span></a></li>
			{{-- <li class="nav-item"><a href="#" class="nav-link "><span class="pcoded-micon"><i class="feather icon-flag"></i></span><span class="pcoded-mtext">Exp. Laboral</span></a></li> --}}
			<li class="nav-item"><a href="#" class="nav-link "><span class="pcoded-micon"><i class="feather icon-flag"></i></span><span class="pcoded-mtext">Parentesco</span><span class="pcoded-badge badge badge-success">EN PROCESO...</span></a></li>
			<li class="nav-item"><a href="#" class="nav-link "><span class="pcoded-micon"><i class="feather icon-edit-1"></i></span><span class="pcoded-mtext">Capacitacion</span><span class="pcoded-badge badge badge-success">EN PROCESO...</span></a></li>

			<li class="nav-item"><a href="" class="nav-link "><span class="pcoded-micon"><i class="feather icon-users"></i></span><span class="pcoded-mtext">Usuarios</span><span class="pcoded-badge badge badge-success">EN PROCESO...</span></a></li>
		</ul>

	</div>
</div>
