<ul id="mm-sidebar-toggle" class="side-menu">

	<li class="">
		<a href="{{ route('dashboard') }}" class="svg-icon">
			<i class="dripicons dripicons-home"></i>
			<span class="">Dashboard</span>
		</a>
	</li>

	<li class="">
		<a href="{{ route('dashboard.unidad.index') }}" class="svg-icon">
			<i class="dripicons dripicons-wallet"></i>
			<span class="">Unidad</span>
		</a>
	</li>

	<li class="">
		<a href="{{ route('dashboard.profesion.index') }}" class="svg-icon">
			<i class="dripicons dripicons-briefcase"></i>
			<span class="">Profesiones</span>
		</a>
	</li>

	<li class="">
		<a href="{{ route('dashboard.cargo.index') }}" class="svg-icon">
			<i class="dripicons dripicons-bookmarks"></i>
			<span class="">Cargo</span>
		</a>
	</li>

	<li class="">
		<a href="#ui" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false">
			<i class="dripicons dripicons-user"></i>
			<span class="ml-2">Personal</span>
			<i class="las la-angle-right mm-arrow-right arrow-active"></i>
			<i class="las la-angle-down mm-arrow-right arrow-hover"></i>
		</a>
		<ul id="ui" class="submenu collapse" data-parent="#mm-sidebar-toggle">
			<li class="">
				<a href="{{ route('dashboard.ausencias.index') }}" class="svg-icon">
					<i class="dripicons dripicons-clipboard"></i>
					<span class="">Ausencias</span>
				</a>
			</li>
			<li class="">
				<a href="{{ route('dashboard.personal.index') }}" class="svg-icon">
					<i class="">
						<svg class="svg-icon" id="mm-ui-1-1" xmlns="http://www.w3.org/2000/svg" fill="none"
							viewBox="0 0 24 24" stroke="currentColor">
							<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
								d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z" />
						</svg>
					</i><span class="">Listado de Personal</span>
				</a>
			</li>


			<li class="">
				<a href="{{ route('dashboard.estudios.index') }}" class="svg-icon">
					<i class="">
						<svg class="svg-icon" id="mm-ui-1-2" xmlns="http://www.w3.org/2000/svg" fill="none"
							viewBox="0 0 24 24" stroke="currentColor">
							<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
								d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
						</svg>
					</i><span class="">Estudios</span>
				</a>
			</li>

			<li class="">
				<a href="{{ route('dashboard.estudios.index') }}" class="svg-icon">
					<i class="">
						<i class="dripicons dripicons-document-edit"></i>
					</i><span class="">Vacaciones</span>
				</a>
			</li>

			<li class="">
				<a href="{{ route('dashboard.estudios.index') }}" class="svg-icon">
					<i class="">
						<i class="dripicons dripicons-contract-2"></i>
					</i><span class="">Parentesco</span>
				</a>
			</li>

		</ul>
	</li>

	<li class="">
		<a href="#charts" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false">
			<i>
				<svg class="svg-icon" id="mm-chart-1" width="20" xmlns="http://www.w3.org/2000/svg"
					fill="none" viewBox="0 0 24 24" stroke="currentColor">
					<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
						d="M8 13v-1m4 1v-3m4 3V8M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z" />
				</svg>
			</i>
			<span class="ml-2">Reportes</span>
			<i class="las la-angle-right mm-arrow-right arrow-active"></i>
			<i class="las la-angle-down mm-arrow-right arrow-hover"></i>
		</a>
		<ul id="charts" class="submenu collapse" data-parent="#mm-sidebar-toggle">
			<li class="">
				<a href="../backend/chart-apex.html" class="svg-icon">
					<i class="">
						<i class="dripicons dripicons-blog"></i>
					</i>
					<span class="">Excel</span>
				</a>
			</li>
			<li class="">
				<a href="../backend/chart-high.html" class="svg-icon">
					<i class="">
						<i class="dripicons dripicons-browser"></i>
					</i>
					<span class="">PDF</span>
				</a>
			</li>
		</ul>
	</li>


	<li class="">
		<a href="#pages" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false">
			<i>
				<i class="dripicons dripicons-user-group"></i>
			</i>
			<span class="ml-2">Usuarios</span>
			<i class="las la-angle-right mm-arrow-right arrow-active"></i>
			<i class="las la-angle-down mm-arrow-right arrow-hover"></i>
		</a>
		<ul id="pages" class="submenu collapse" data-parent="#mm-sidebar-toggle">
			<li class="">
				<a href="#timeline" class="collapsed svg-icon" data-toggle="collapse" aria-expanded="false">
					<i class="">
						<i class="dripicons dripicons-document"></i>
					</i><span class="">Listado Usuarios</span>
					<i class="las la-angle-right mm-arrow-right arrow-active"></i>
					<i class="las la-angle-down mm-arrow-right arrow-hover"></i>
				</a>
				<ul id="timeline" class="submenu collapse" data-parent="#pages">
					<li class="">
						<a href="../backend/timeline-1.html" class="svg-icon">
							<i class="">
								<i class="dripicons dripicons-enter"></i>
							</i><span class="">Agregar Usuario</span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>

	<li class="">
		<a href="{{ route('dashboard.cargo.index') }}" class="svg-icon">
			<i class="dripicons dripicons-calendar"></i>
			<span class="">Calendario</span>
		</a>
	</li>

	<li class="">
		<a href="{{ route('dashboard.cargo.index') }}" class="svg-icon">
			<i class="dripicons dripicons-medical"></i>
			<span class="">Baja Medica</span>
		</a>
	</li>
</ul>