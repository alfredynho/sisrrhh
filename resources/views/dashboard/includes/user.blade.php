    <div class="main-menu-header">
        <img class="img-radius" src="{{ asset('admin/images/account.png') }}" alt="RRHH">
        <div class="user-details">
            <div id="more-details">
            <strong>
                @if (Auth::check())
                    {{ Auth::user()->email }}
                @else
                    user@mail.com
                @endif
            </strong></div>
        </div>
    </div>
    <div class="collapse" id="nav-user-link">
        <ul class="list-inline">
            <li class="list-inline-item"><a href="#" data-toggle="tooltip" title="Ver Perfil"><i class="feather icon-user"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="feather icon-mail" data-toggle="tooltip" title="Notificaciones"></i><small class="badge badge-pill badge-primary">5</small></a></li>

            <li class="list-inline-item"><a href="#" data-toggle="tooltip" title="Cerrar Sesión" class="text-danger"><i class="feather icon-power"></i></a></li>
        </ul>
    </div>
