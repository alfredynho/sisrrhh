@extends('layouts.admin')

@section('extracss')

@endsection('extracss')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Personal</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Personal</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card user-profile-list">
                <div class="card-body">
                    <div class="dt-responsive table-responsive">
                    <div class="row align-items-center m-l-0">
                        <div class="col-sm-6">

                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="{{ route('dashboard.personal.create') }}" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i> Nuevo Registro</a>
                        </div>
                    </div>

                        <table class="table nowrap dt">
                            <thead>
                                <tr class="text-center">
                                    <th>Nro</th>
                                    <th>CEDULA</th>
                                    <th>NOMBRES</th>
                                    <th>CARGO</th>
                                    <th>AFP</th>
                                    <th>SEGURO</th>
                                    <th>CELULAR</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($personal as $cont=>$bg)
                                    <tr class="text-center">
                                        <td>{{ $cont+1 }}</td>
                                        <td>{{ $bg->cedula }}</td>
                                        <td>{{ $bg->nombres }} {{ $bg->paterno }} {{ $bg->materno }}</td>
                                        <td>{{ $bg->nombre_cargo }}</td>
                                        <td>
                                            @if($bg->afp == 1 )
                                                <span class="badge badge-light-success">AFP PREVISIÓN</span>
                                            @else
                                                <span class="badge badge-light-success">AFP FUTURO DE BOLIVIA</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($bg->seguro == 1 )
                                                <span class="badge badge-light-success">SI</span>
                                            @else
                                                <span class="badge badge-light-danger">NO</span>
                                            @endif
                                        </td>
                                        <td>{{ $bg->celular }}
                                            <div class="overlay-edit">
                                                <a href="" data-target="#modalUpdate-{{ $bg->id }}" data-toggle="modal" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                                <a href="" data-target="#modalDelete-{{ $bg->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>
                                                <a href="{{ route('dashboard.personal.reporte_funcionario',[$bg->id]) }}" target="_blank" class="btn btn-icon btn-info"><i class="feather icon-eye"></i></a>
                                                <a href="{{ route('dashboard.personal.detalle',[$bg->id]) }}" class="btn btn-icon btn-primary"><i class="feather icon-eye"></i></a>
                                            </div>
                                        </td>
                                    </tr>


                                    {{-- {{ route('detail_new',[$bg->slug]) }} --}}
                                    {{--  @include('dashboard.pages.blog.edit') --}}
                                    
                                    @include('dashboard.pages.personal.delete')

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
            {{--  @include('dashboard.pages.blog.create')  --}}
    </div>
@endsection

@section('extrajs')
    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>

    <script>
    $(document).ready(function() {
        tinymce.init({
            selector: 'textarea',
            language:'es_ES',
        plugins: [
                "preview",
                "searchreplace wordcount visualblocks visualchars code fullscreen"
            ],
        a_plugin_option: true,
        a_configuration_option: 400
        });
    });
    </script>
@endsection('extrajs')
