@extends('layouts.admin')

@section('extracss')

@endsection('extracss')
    <link href="{{ asset('admin/plugins/select2.min.css')}}" rel="stylesheet">

@section('content')

    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">{{ $message }}</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">{{ $message }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Registro de Personal</h5>
                </div>
                <div class="card-body">

                    @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    {!! Form::open(['route' => 'dashboard.personal.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Cedula</label>
                                    <input type="number" name="cedula" id="cedula" maxlength="50" class="form-control" value="{{old('cedula')}}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Nombres</label>
                                    <input type="text" name="nombres" id="nombres" maxlength="50" class="form-control" value="{{old('nombres')}}">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Paterno</label>
                                    <input type="text" name="paterno" id="paterno" maxlength="50" class="form-control" value="{{old('paterno')}}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Materno</label>
                                    <input type="text" name="materno" id="materno" maxlength="50" class="form-control" value="{{old('materno')}}">
                                </div>
                            </div>
                        
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Fecha de nacimiento</label>
                                    <input type="text" name="f_nacimiento" id="f_nacimiento" maxlength="50" class="fechaIngreso form-control" value="{{old('f_nacimiento')}}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Caja</label>
                                    <select class="form-control" name="caja">
                                            <option value="Caja Petrolera de Salud">Caja Petrolera de Salud</option>                                            
                                            <option value="Caja Nacional de Salud">Caja Nacional de Salud</option>                                            
                                            <option value="Caja de Salud de la Banca Privada">Caja de Salud de la Banca Privada</option>                                            
                                            <option value="Caja de Salid de Caminos y RA">Caja de Salid de Caminos y RA</option>                                            
                                            <option value="Caja de Salud Cordes">Caja de Salud Cordes</option>                                            
                                            <option value="Caja Bancaria Estatal de Salud">Caja Bancaria Estatal de Salud</option>                                            
                                            <option value="Cossmil">Cossmil</option>                                            
                                            <option value="Seguro Integral de Salud SINEC">Seguro Integral de Salud SINEC</option>                                            
                                            <option value="Seguro Social Universitario">Seguro Social Universitario</option>                                            
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Cargo</label>
                                    <select class="form-control select2" name="id_cargo">
                                        @foreach ($cargos as $cargo)
                                            <option value="{{ $cargo->id }}">{{ $cargo->nombre }}</option>                                            
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Profesión</label>
                                    <select class="form-control select2" name="id_profesion">
                                        @foreach ($profesion as $pro)
                                            <option value="{{ $pro->id }}">{{ $pro->nombre }}</option>                                            
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Correo</label>
                                    <input type="email" name="correo" id="correo" maxlength="50" class="form-control" value="{{old('correo')}}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Fecha de incorporación</label>
                                    <input type="text" name="f_ingreso" id="f_ingreso" maxlength="50" class="fechaIngreso form-control" value="{{old('f_ingreso')}}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Estado Civil</label>
                                    <select class="form-control" name="estado_civil">
                                        <option value="1">Soltero (a)</option>
                                        <option value="0">Casado (a)</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Celular</label>
                                    <input type="number" name="celular" id="celular" maxlength="50" class="form-control" value="{{old('celular')}}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Sexo</label>
                                    <select class="form-control" name="genero">
                                        <option value="1">Masculino</option>
                                        <option value="0">Femenino</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">AFP</label>
                                    <select class="form-control" name="afp">
                                        <option value="1">AFP Previsión</option>
                                        <option value="0">AFP Futuro de Bolivia</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">¿Tiene Seguro?</label>
                                    <select class="form-control" name="seguro">
                                        <option value="1">Si tiene</option>
                                        <option value="0">No tiene</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Dirección</label>
                                    <textarea class="form-control editor" name="direccion"></textarea>
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <a href="{{ URL::previous() }}" type="button" class="btn btn-warning">Regresar</a>

                    {{Form::Close()}}

                </div>
            </div>
        </div>

    </div>

@endsection('content')

@section('extrajs')
    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2.min.js') }}"></script>

    <script>
    $(document).ready(function() {
        $('.select2').select2();

        tinymce.init({
            selector: '.editor',
            language:'es_ES',
            plugins: "table",
            toolbar: "table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol!",
            max_height: 500,
            min_height: 350
        });
    });
    </script>
@endsection('extrajs')
