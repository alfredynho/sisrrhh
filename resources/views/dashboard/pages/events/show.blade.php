@extends('layouts.admin')


@section('content')

    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Blog</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Blog</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Registro de Blog</h5>
                </div>
                <div class="card-body">

                    @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    {!! Form::model($blog,['route'=>['dashboard.blog.update',$blog->id],'method'=>'PUT','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Titulo</label>
                                    <input type="text" name="title" id="title_c" maxlength="50" class="form-control" value="{{ $blog->title }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="category">Categoria</label>
                                    <select id="category" name="category" class="form-control" required>
                                        <option value="BLOG" {{ ($blog->category=='BLOG') ? 'selected' : '' }}>BLOG</option>
                                        <option value="ANUNCIO" {{ ($blog->category=='ANUNCIO') ? 'selected' : '' }}>ANUNCIO</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Publicado?</label>
                                    <select class="form-control" name="is_published">
                                        <option value="1" {{ ($blog->is_published=='1') ? 'selected' : '' }}>Si</option>
                                        <option value="0" {{ ($blog->is_published=='0') ? 'selected' : '' }}>No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Destacado?</label>

                                    @if( $blog->destacado == 1)
                                        <select class="form-control" name="destacado">
                                            <option value="1" {{ ($blog->destacado=='1') ? 'selected' : '' }}>Si</option>
                                            <option value="0" {{ ($blog->destacado=='0') ? 'selected' : '' }}>No</option>
                                        </select>
                                    @elseif(BlogDestacado() >= 1)
                                        <select class="form-control" name="destacado">
                                            <option value="0" {{ ($blog->destacado=='0') ? 'selected' : '' }}>No</option>
                                        </select>
                                    @else
                                        <select class="form-control" name="destacado">
                                            <option value="1" {{ ($blog->destacado=='1') ? 'selected' : '' }}>Si</option>
                                        </select>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Descripción</label>
                                    <textarea class="form-control editor" name="description"> {{ $blog->description }}</textarea>
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <a href="{{ URL::previous() }}" type="button" class="btn btn-warning">Regresar</a>

                    {{Form::Close()}}

                </div>
            </div>
        </div>

    </div>

@endsection

@section('extrajs')
    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>

    <script>
    $(document).ready(function() {
        tinymce.init({
            selector: '.editor',
            language:'es_ES',
            plugins: "table",
            toolbar: "table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol!",
            max_height: 500,
            min_height: 350
        });
    });
    </script>
@endsection('extrajs')
