<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Nuevo Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="card-body" id="app">

            {!! Form::open(['route' => 'dashboard.gallery.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                <label for="title_c">Título imagen</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id='remainingC'></div>
                    </div>
                    <input type="text" name="title" id="title_c" maxlength="150" class="form-control" value="{{old('title')}}">
                </div>
                <small id="passwordHelpBlock" class="form-text text-muted mt-15 mb-15">
                    El limite para el titulo es de 150 caracteres incluyes espacios
                </small>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="status">Estado</label>
                        <select id="status" name="status" class="form-control" required>
                            <option selected>Seleccione...</option>
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <div class="custom-file">
                            <label>Subir Imagen</label>
                            <input type="file" name="image" id="image"/>
                        </div>
                    </div>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{Form::Close()}}
        </div>

    </div>
    </div>
</div>
