@extends('layouts.admin')

@section('content')
<div class="page-title">
    <div class="row">
      <div class="col-sm-6">
          <h4 class="mb-0"> Módulo Galeria Imagenes</h4>
      </div>

      <div class="col-sm-6">
        <ol class="breadcrumb pt-0 pr-0 float-left float-sm-right">
          <li class="breadcrumb-item"><a href="#" class="default-color">Inicio</a></li>
          <li class="breadcrumb-item active">Panel</li>
        </ol>
      </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12 mb-30">
        <div class="card card-statistics h-100">
        <div class="card-body">
            <div class="d-block d-md-flex justify-content-between">
            <div class="d-block">
                <h5 class="card-title pb-0 border-0">Registros</h5>
            </div>

            <div class="d-block d-md-flex clearfix sm-mt-20">
                <div class="clearfix">
                    <div class="box">
                        <button href="#" class="btn btn-success" data-toggle="modal" data-target="#modalCreate" title="Nuevo Registro">Nuevo Registro</button>
                        {{--  <select class="fancyselect sm-mb-20 mr-20">
                            <option value="1">Some option</option>
                            <option value="2">Another option</option>
                            <option value="3">A option</option>
                            <option value="4">Potato</option>
                        </select>  --}}
                    </div>
                </div>

            </div>
            </div>
            <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered p-0">
                <thead>
                <tr class="text-dark text-center">
                    <th>Nro</th>
                    <th>NOMBRE</th>
                    <th>ESTADO</th>
                    <th>FECHA CREACIÓN</th>
                    <th>OPCIONES</th>
                </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($gallery as $cont=>$ga)
                    <tr>
                        <td>{{ $cont+1 }}</td>
                        <td>{{ $ga->title }}</td>
                        <td>
                            @if($ga->is_published == 1 )
                                <label class="badge badge-success">ACTIVO</label>
                            @else
                                <label class="badge badge-danger">INACTIVO</label>
                            @endif
                        </td>
                        <td>{{ $ga->created_at->format('d-m-Y') }}</td>
                        <td>

                            <a href="" data-target="#modalUpdate-{{ $ga->id }}" data-toggle="modal" class="btn btn-warning btn-sm" title="Modificar Registro">
                                <i class="fa fa-pencil pr-1"></i>
                            </a> &nbsp;

                            <a href="" data-target="#modalDelete-{{ $ga->id }}" data-toggle="modal" class="btn btn-danger btn-sm" title="Eliminar Registro">
                                <i class="fa fa-trash pr-1"></i>
                            </a> &nbsp;

                        </td>
                    </tr>

                    @include('dashboard.pages.gallery.edit')
                    @include('dashboard.pages.gallery.delete')

                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
        </div>
    </div>
    @include('dashboard.pages.gallery.create')
</div>

@endsection

@section('extrajs')
    <script>

        $(document).ready(function() {
        var len = 0;
        var maxchar = 150;

        $( '#title_c' ).keyup(function(){
            len = this.value.length
            if(len > maxchar){
                return false;
            }
            else if (len > 0) {
                $( "#remainingC" ).html( ( maxchar - len ) );
            }
            else {
                $( "#remainingC" ).html( ( maxchar ) );
            }
        })

        $( '#title_e' ).keyup(function(){
            len = this.value.length
            if(len > maxchar){
                return false;
            }
            else if (len > 0) {
                $( "#remainingE" ).html( ( maxchar - len ) );
            }
            else {
                $( "#remainingE" ).html( ( maxchar ) );
            }
        })

        var bla = $('#title_e').val();
            $( "#remainingE" ).html( ( bla.length ) );
        });

    </script>
@endsection('extrajs')
