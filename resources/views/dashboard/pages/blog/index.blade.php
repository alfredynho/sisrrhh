@extends('layouts.admin')

@section('extracss')

@endsection('extracss')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Anuncio</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Anuncio</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card user-profile-list">
                <div class="card-body">
                    <div class="dt-responsive table-responsive">
                    <div class="row align-items-center m-l-0">
                        <div class="col-sm-6">

                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="{{ route('dashboard.blog.create') }}" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i> Nuevo Registro</a>
                        </div>
                    </div>

                        <table class="table nowrap dt">
                            <thead>
                                <tr class="text-center">
                                    <th>Nro</th>
                                    <th>TÍTULO</th>
                                    <th>FECHA CREACIÓN</th>
                                    <th>ACTIVO?</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($anuncio as $cont=>$bg)
                                    <tr class="text-center">
                                        <td>{{ $cont+1 }}</td>
                                        <td>{{ $bg->title }}</td>
                                        <td>{{ $bg->created_at->format('d-m-Y') }}</td>
                                        <td>
                                            @if($bg->is_published == 1 )
                                                <span class="badge badge-light-success">ACTIVO</span>
                                            @else
                                                <span class="badge badge-light-danger">INACTIVO</span>
                                            @endif

                                            <div class="overlay-edit">
                                                <a href="{{ route('dashboard.blog.show', [$bg->id]) }}" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                                <a href="" data-target="#modalDelete-{{ $bg->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>

                                                {{-- <a href="" data-target="#modalUpdate-{{ $bg->id }}" data-toggle="modal" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                                <a href="" data-target="#modalDelete-{{ $bg->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a> --}}
                                            </div>
                                        </td>
                                    </tr>

                                    {{--  @include('dashboard.pages.blog.edit') --}}
                                    
                                    @include('dashboard.pages.blog.delete')

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
            {{--  @include('dashboard.pages.blog.create')  --}}
    </div>
@endsection

@section('extrajs')
    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>

    <script>
    $(document).ready(function() {
        tinymce.init({
            selector: 'textarea',
            language:'es_ES',
        plugins: [
                "preview",
                "searchreplace wordcount visualblocks visualchars code fullscreen"
            ],
        a_plugin_option: true,
        a_configuration_option: 400
        });
    });
    </script>
@endsection('extrajs')
