@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Messenger</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Usuarios Messenger</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

    </div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card user-profile-list">
					<div class="card-body">
						<div class="dt-responsive table-responsive">
                        <div class="row align-items-center m-l-0">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6 text-right">
                                {{-- <button href="#" class="btn btn-success btn-sm btn-round has-ripple" data-toggle="modal" data-target="#modalCreate"><i class="feather icon-plus"></i> Nuevo Registro</button> --}}
                            </div>
                        </div>
                        <div class="table-responsive">

							<table class="table nowrap dt">
								<thead>
									<tr class="text-center">
                                        <th>NRO</th>
                                        <th>NOMBRE</th>
                                        <th>APELLIDO</th>
                                        <th>FECHA CREACIÓN</th>
                                        <th>GÉNERO</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($messenger as $cont=>$ms)
                                        <tr class="text-center">
                                            <td>{{ $cont+1 }}</td>
                                            <td>{{ $ms->first_name }}</td>
                                            <td>{{ $ms->last_name }}</td>
                                            <td>{{ $ms->created_at->format('d-m-Y') }}</td>
                                            <td>
                                                @if($ms->gender == 'male' )
                                                    <span class="badge badge-light-success">  Masculino / {{ $ms->created_at->format('d-m-Y') }}</span>
                                                @else
                                                    <span class="badge badge-light-warning">Femenino / {{ $ms->created_at->format('d-m-Y') }}</span>
                                                @endif

                                                <div class="overlay-edit">
                                                    <a href="" data-target="#modalDelete-{{ $ms->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>
                                                </div>
                                            </td>
                                        </tr>

                                        @include('dashboard.pages.messenger.edit')
                                        @include('dashboard.pages.messenger.delete')

                                    @endforeach
								</tbody>
							</table>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection

@section('extrajs')

@endsection('extrajs')
