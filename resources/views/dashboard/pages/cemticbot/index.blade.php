@extends('layouts.admin')

@section('extracss')

@endsection('extracss')

@section('content')

    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Blog</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Asistente Virtual</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-3 col-lg-12">
            <div class="card task-board-left">
                <div class="card-body">
                    <div class="task-right">
                        <div class="task-right-header-users">
                            <span class="f-w-400" data-toggle="collapse"><strong>Usuarios Facebook Registrados</strong></span>
                        </div>

                        <div class="user-box assign-user taskboard-right-users">
                            @if(count($user_messenger)>= '1')
                                @foreach ($user_messenger as $usm)
                                    <div class="media mb-2">
                                        <div class="media-left media-middle mr-3">
                                            <a href="{{ route('admin-messenger') }}">
                                                <img class="img-fluid media-object img-radius" src="{{ asset('cce/img/cce_logo.png') }}" alt="usuarios Facebook">
                                                <div class="live-status bg-primary"></div>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h6>{{ $usm->first_name }} {{ $usm->last_name }}</h6>
                                            <p>
                                                @if($usm->gender == 'male')
                                                    Masculino / {{ $usm->created_at->format('d-m-Y') }}
                                                @else
                                                    Femenino / {{ $usm->created_at->format('d-m-Y') }}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="mb-200">
                                    <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                    <strong>Sin Registros! en Usuarios Messenger</strong>
                                    </div>
                                </div>
                            @endif
                            <div class="text-center">
                                <a href="{{ route('admin-messenger') }}" class="b-b-primary text-primary">Ver todos los registros</a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-9 col-lg-12 filter-bar">
            <nav class="navbar m-b-30 p-10">
                <ul class="nav">
                    <li class="nav-item f-text active">
                        <a class="nav-link text-secondary text-center" href="#!"><strong> Lista de Preguntas por Categoria</strong> <span class="sr-only">(current)</span></a>
                    </li>

                    <div class="col-sm-6">
                        <button href="#" class="btn btn-success btn-sm btn-round has-ripple" data-toggle="modal" data-target="#modalCreate"><i class="feather icon-plus"></i> Agregar Preguntas</button>
                    </div>
                </ul>
            </nav>

            <div class="row">
                @if(count($category)>='1')
                    @foreach ($category as $ct)
                        <div class="col-md-6 col-sm-12">
                            <div class="card card-border-c-blue">
                                <div class="card-header">
                                    <a href="{{ route('dashboard.dtquestion.index', [$ct->slug]) }}" class="text-secondary"><strong>{{ strtoupper($ct->name) }}</strong></a>
                                    <span class="label label-primary float-right">{{ $ct->created_at->format('d') }} {{ mesLiteral($ct->created_at->format('m')) }} de {{ $ct->created_at->format('Y') }}</span>
                                </div>
                                <div class="card-body card-task">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p class="task-detail">Cantidad de preguntas y respuestas para la sección</p>
                                            <p class="task-due"><strong> Cantidad : </strong><strong class="label label-primary"> {{ cantidadRegFaqs($ct->id) }}</strong></p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="task-list-table">
                                        <a href="{{ route('dashboard.dtquestion.index', [$ct->slug]) }}"><img class="img-fluid img-radius m-r-5" src="{{ asset('cce/img/cce_logo.png') }}" alt="Img UPEA CCE " /></a>
                                    </div>
                                    <div class="task-board">
                                         <a href="{{ route('dashboard.dtquestion.index', [$ct->slug]) }}" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i> Nuevo Registro</a>
                                    </div>
                                </div>
                            </div>
                                @include('dashboard.pages.edubot.edit')
                                @include('dashboard.pages.edubot.delete')
                        </div>
                    @endforeach
                @else
                    <div class="mb-200">
                        <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                        <strong>Sin Registros! en Categorias </strong> Nos encontramos preparando el mejor contenido para ti <strong> UPEA CCE</strong>
                        </div>
                    </div>
                @endif

                @include('dashboard.pages.edubot.create')
            </div>
        </div>
    </div>

@endsection

@section('extrajs')

@endsection('extrajs')
