@extends('layouts.admin')

@section('pushcss')

@endsection

@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Módulo Clientes</h5>
                </div>
                <ul class="breadcrumb">
                    @include('dashboard.includes.navadmin')
                    <li class="breadcrumb-item"><a href="#!">Clientes</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Modificar de Cliente</h5>
            </div>
            <div class="card-body">

                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::model($clients,['route'=>['dashboard.clients.update',$clients->id],'method'=>'PUT','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Nombre Curso</label>
                                <input type="text" name="name" id="name" maxlength="150" class="form-control" value="{{ $clients->name }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Publicado?</label>
                                <select class="form-control" name="is_published">
                                    <option value="1" {{ ($clients->is_published=='1') ? 'selected' : '' }}>Activo</option>
                                    <option value="0" {{ ($clients->is_published=='0') ? 'selected' : '' }}>Inactivo</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Destacado?</label>
                                <select class="form-control" id="destacado" name="destacado">
                                    <option value="1" {{ ($clients->destacado=='1') ? 'selected' : '' }}>Si</option>
                                    <option value="0" {{ ($clients->destacado=='0') ? 'selected' : '' }}>No</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Enlace Imagen</label>
                                <input type="text" name="image" id="image" class="form-control" value="{{ $clients->image }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Descripción</label>
                                <textarea class="form-control editor" name="description"> {{ $clients->description }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Contenido</label>
                                <textarea class="form-control editor" name="message">{{ $clients->message }}</textarea>
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-success">Modificar</button>
                    <a href="{{ URL::previous() }}" type="button" class="btn btn-warning">Regresar</a>

                {{Form::Close()}}

            </div>
        </div>
    </div>

</div>
@endsection

@section('extrajs')
    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>

    <script>
    $(document).ready(function() {
        tinymce.init({
            selector: '.editor',
            language:'es_ES',
            plugins: "table",
            toolbar: "table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol!",
            max_height: 500,
            min_height: 350
        });
    });
    </script>

@endsection
