<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Nuevo Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="card-body" id="app">

            {!! Form::open(['route' => 'dashboard.students.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                <label for="name_c">Nombre</label>
                <div class="input-group mb-2 mr-sm-2">
                    <input type="text" name="name" id="name_c" maxlength="150" class="form-control" value="{{old('nombre')}}">
                </div>

                <label for="position">Nivel</label>
                <div class="input-group mb-2 mr-sm-2">
                    <input type="text" name="position" id="position" maxlength="150" class="form-control" value="{{old('position')}}">
                </div>

                <div class="form-group">
                    <label for="message">Descripción</label>
                    <textarea class="form-control classic-editor" id="message" name="message" rows="3" value="{{old('message')}}" placeholder="Descripción de la publicación"></textarea>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="status">Estado</label>
                        <select id="status" name="status" class="form-control" required>
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>

                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>

            {{Form::Close()}}
        </div>

    </div>
    </div>
</div>
