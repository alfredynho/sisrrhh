<div class="modal fade" id="modalUpdate-{{ $aca->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Modificar Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        {!! Form::model($aca,['route'=>['dashboard.academics.update',$aca->id],'method'=>'PUT','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

        <div class="card-body">
            <label for="name">Nombre</label>
            <div class="input-group mb-2 mr-sm-2">
                <input type="text" name="name" id="name_e" maxlength="150" class="form-control" value="{{ $aca->name }}" >
            </div>

            <label for="cargo_c">Cargo</label>
            <div class="input-group mb-2 mr-sm-2">
                <input type="text" name="cargo" id="cargo_c" maxlength="150" class="form-control" value="{{ $aca->cargo }}">
            </div>

            <div class="form-group">
                <label for="biography">Biografia</label>
                <textarea class="form-control classic-editore" id="biography" name="biography" rows="3" maxlength="150" required> {{ $aca->biography }}</textarea>
            </div>

            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="status">Estado</label>
                    <select id="status" name="status" class="form-control" required>
                        <option value="1" {{ ($aca->is_published=='1') ? 'selected' : '' }}>Activo</option>
                        <option value="0" {{ ($aca->is_published=='0') ? 'selected' : '' }}>Inactivo</option>
                    </select>
                </div>

                <div class="form-group col-md-6">
                    <label for="grade">Grado</label>
                    <select id="grade" name="grade" class="form-control" required>
                        <option value="0" {{ ($aca->grade=='0') ? 'selected' : '' }}>Licenciatura</option>
                        <option value="1" {{ ($aca->grade=='1') ? 'selected' : '' }}>Magister</option>
                        <option value="2" {{ ($aca->grade=='2') ? 'selected' : '' }}>Doctorado</option>
                    </select>
                </div>

            </div>

            <div class="custom-file">
                <label>Subir Imagen</label>
                <input type="file" name="image" id="image"/>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Modificar</button>
            </div>
        </div>

        {{Form::Close()}}

    </div>
    </div>
</div>
