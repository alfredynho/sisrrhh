@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Categoria</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Categorias para preguntas</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

    </div>

		<div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="mb-40">
                            <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Anuncio </h4>
                                <p>El siguiente apartado se encuentra disponible para la administración de Categorias y Acciones(Actions) de preguntas frecuentes</p>
                            <hr>
                                <p>Una Acción(Action) es un evento que accionara algunas preguntas y respuestas del asistente virtual ("Esta acción debe copiarse en <strong>DialogFlow</strong>")</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

			<div class="col-lg-12">
				<div class="card user-profile-list">
					<div class="card-body">
						<div class="dt-responsive table-responsive">
                        <div class="row align-items-center m-l-0">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6 text-right">
                                <button href="#" class="btn btn-success btn-sm btn-round has-ripple" data-toggle="modal" data-target="#modalCreate"><i class="feather icon-plus"></i> Nuevo Registro</button>
                            </div>
                        </div>
                        <div class="table-responsive">

							<table class="table nowrap dt">
								<thead>
									<tr class="text-center">
                                        <th>NRO</th>
                                        <th>CATEGORIA</th>
                                        <th>ACCIÓN</th>
                                        <th>FECHA CREACIÓN</th>
                                        <th>ESTADO</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($category as $cont=>$ca)
                                        <tr class="text-center">
                                            <td>{{ $cont+1 }}</td>
                                            <td>{{ $ca->name }}</td>
                                            <td>{{ $ca->action }}</td>
                                            <td>{{ $ca->created_at->format('d-m-Y') }}</td>
                                            <td>
                                                @if($ca->status == 1 )
                                                    <span class="badge badge-light-success">ACTIVO</span>
                                                @else
                                                    <span class="badge badge-light-danger">INACTIVO</span>
                                                @endif

                                                <div class="overlay-edit">
                                                    <a href="" data-target="#modalUpdate-{{ $ca->id }}" data-toggle="modal" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                                    <a href="" data-target="#modalDelete-{{ $ca->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>
                                                </div>
                                            </td>
                                        </tr>

                                        @include('dashboard.pages.category.edit')
                                        @include('dashboard.pages.category.delete')

                                    @endforeach
								</tbody>
							</table>
						</div>
						</div>
					</div>
				</div>
			</div>
                @include('dashboard.pages.category.create')
		</div>
@endsection

@section('extrajs')

@endsection('extrajs')
