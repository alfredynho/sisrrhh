<div class="modal fade" id="modalUpdate-{{ $ca->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Modificar Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="card-body">

             {!! Form::model($ca,['route'=>['dashboard.categoria.update',$ca->id],'method'=>'PUT','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                <div class="form-group">
                    <label for="name">Título Categoria</label>
                    <input type="text" class="form-control" id="name" name="name" required value="{{ $ca->name }}">
                </div>

                <div class="form-group">
                    <label for="biography">Descripción</label>
                    <textarea class="form-control classic-editor" id="description" name="description" rows="3" value="{{old('description')}}" placeholder="Descripción">{{ $ca->description }}</textarea>
                </div>

                <div class="form-group">
                    <label for="image">imagen</label>
                    <input type="text" class="form-control" id="image" name="image" required value="{{ $ca->image }}">
                </div>


                <div class="form-row">

                    <div class="form-group col-md-12">
                        <label for="status">Estado</label>
                        <select id="status" name="status" class="form-control" required>
                            <option value="1" {{ ($ca->status=='1') ? 'selected' : '' }}>Activo</option>
                            <option value="0" {{ ($ca->status=='0') ? 'selected' : '' }}>Inactivo</option>
                        </select>
                    </div>

                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Modificar</button>
            </div>
        </div>
    {{Form::Close()}}

    </div>
    </div>
</div>
