<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Nuevo Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="card-body" id="app">

            {!! Form::open(['route' => 'dashboard.clients.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                <label for="name_c">Nombre Cliente</label>
                <div class="input-group mb-2 mr-sm-2">
                    <input type="text" name="name" id="name_c" maxlength="150" class="form-control" value="{{old('name')}}">
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="status">Estado</label>
                        <select id="status" name="status" class="form-control" required>
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                </div>

                <label for="url">Url</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id='remainingC'></div>
                    </div>

                    <textarea class="form-control" id="url" id="title_c" maxlength="150" name="url" rows="3"  required value="{{old('message')}}" placeholder="Mensaje" :maxlength="max" v-model="text"></textarea>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{Form::Close()}}
        </div>

    </div>
    </div>
</div>
