<div class="modal fade" id="modalUpdate-{{ $ts->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Modificar Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="card-body">

             {!! Form::model($ts,['route'=>['dashboard.memories.update',$ts->id],'method'=>'PUT','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                <div class="form-group">
                    <label for="author">Autor(a)</label>
                    <input type="text" class="form-control" id="author" name="author" required value="{{ $ts->author }}">
                </div>

                <div class="form-group">
                    <label for="message">Mensaje</label>
                    <textarea class="form-control" id="message" name="message" rows="3" > {{ $ts->message }}</textarea>
                </div>

                <div class="form-row">

                    <div class="form-group col-md-12">
                        <label for="status">Estado</label>
                        <select id="status" name="status" class="form-control" required>
                            <option value="1" {{ ($ts->status=='1') ? 'selected' : '' }}>Activo</option>
                            <option value="0" {{ ($ts->status=='0') ? 'selected' : '' }}>Inactivo</option>
                        </select>
                    </div>

                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Modificar</button>
            </div>
        </div>
    {{Form::Close()}}

    </div>
    </div>
</div>
