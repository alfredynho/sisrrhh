@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Memorias</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Frases Academica</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

    </div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card user-profile-list">
					<div class="card-body">
						<div class="dt-responsive table-responsive">
                        <div class="row align-items-center m-l-0">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6 text-right">
                                <button href="#" class="btn btn-success btn-sm btn-round has-ripple" data-toggle="modal" data-target="#modalCreate"><i class="feather icon-plus"></i> Nuevo Registro</button>
                            </div>
                        </div>
                        <div class="table-responsive">

							<table class="table nowrap dt">
								<thead>
									<tr class="text-center">
                                        <th>NRO</th>
                                        <th>AUTOR(A)</th>
                                        <th>FECHA CREACIÓN</th>
                                        <th>ESTADO</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($memories as $cont=>$ts)
                                        <tr class="text-center">
                                            <td>{{ $cont+1 }}</td>
                                            <td>{{ $ts->author }}</td>
                                            <td>{{ $ts->created_at->format('d-m-Y') }}</td>
                                            <td>
                                                @if($ts->status == 1 )
                                                    <span class="badge badge-light-success">ACTIVO</span>
                                                @else
                                                    <span class="badge badge-light-danger">INACTIVO</span>
                                                @endif

                                                <div class="overlay-edit">
                                                    <a href="" data-target="#modalUpdate-{{ $ts->id }}" data-toggle="modal" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                                    <a href="" data-target="#modalDelete-{{ $ts->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>
                                                </div>
                                            </td>
                                        </tr>

                                        @include('dashboard.pages.memories.edit')
                                        @include('dashboard.pages.memories.delete')

                                    @endforeach
								</tbody>
							</table>
						</div>
						</div>
					</div>
				</div>
			</div>
                @include('dashboard.pages.memories.create')
		</div>
@endsection

@section('extrajs')
    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>

    <script>
    $(document).ready(function() {
        tinymce.init({
            selector: 'textarea',
            language:'es_ES',
        plugins: [
                "preview",
                "searchreplace wordcount visualblocks visualchars code fullscreen"
            ],
        a_plugin_option: true,
        a_configuration_option: 400
        });
    });
    </script>
@endsection('extrajs')
