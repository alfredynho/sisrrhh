<div class="modal fade" id="modalUpdate-{{ $pg->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Modificar Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        {!! Form::model($pg,['route'=>['dashboard.programa.update',$pg->id],'method'=>'PUT','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}
            <div class="card-body">

                <label for="title">Título</label>
                <div class="input-group mb-2 mr-sm-2">
                    <input type="text" name="title" id="title_e" maxlength="50" class="form-control" value="{{ $pg->title }}" >
                </div>

                <div class="form-group">
                    <label>Descripción</label>
                    <textarea class="form-control classic-editore" id="description" name="description" rows="3" maxlength="150"> {{ $pg->description }}</textarea>
                </div>

                <div class="form-row">

                    <div class="form-group col-md-6">
                        <label for="category">Categoria</label>
                        <select id="category" name="category" class="form-control" required>
                            <option value="PASANTIA" {{ ($pg->category=='PASANTIA') ? 'selected' : '' }}>PASANTIA</option>
                            <option value="FERIA" {{ ($pg->category=='FERIA') ? 'selected' : '' }}>FERIA</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="status">Estado</label>
                        <select id="status" name="status" class="form-control" required>
                            <option value="1" {{ ($pg->is_published=='1') ? 'selected' : '' }}>Activo</option>
                            <option value="0" {{ ($pg->is_published=='0') ? 'selected' : '' }}>Inactivo</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="status">Destacado</label>
                        <select id="status" name="destacado" class="form-control" required>
                            <option value="1" {{ ($pg->destacado=='1') ? 'selected' : '' }}>Si</option>
                            <option value="0" {{ ($pg->destacado=='0') ? 'selected' : '' }}>No</option>
                        </select>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success">Modificar</button>
                </div>
            </div>
        {{Form::Close()}}

    </div>
    </div>
</div>
