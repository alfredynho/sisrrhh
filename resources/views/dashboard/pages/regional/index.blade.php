    @extends('layouts.admin')

    @section('content')
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Módulo Regionales</h5>
                        </div>
                        <ul class="breadcrumb">
                            @include('dashboard.includes.navadmin')
                            <li class="breadcrumb-item"><a href="#!">Regionales</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

        </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card user-profile-list">
                        <div class="card-body">
                            <div class="dt-responsive table-responsive">
                            <div class="row align-items-center m-l-0">
                                <div class="col-sm-6">

                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="{{ route('dashboard.regional.create') }}" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i> Nuevo Registro</a>
                                </div>
                            </div>

                                <table class="table nowrap dt">
                                    <thead>
                                        <tr class="text-center">
                                            <th>NRO</th>
                                            <th>NOMBRE</th>
                                            <th>FECHA CREACIÓN</th>
                                            <th>ENCARGADO</th>
                                            <th>ACTIVO?</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($regional as $cont=>$se)
                                            <tr class="text-center">
                                                <td>{{ $cont+1 }}</td>
                                                <td>
                                                    <div class="d-inline-block align-middle">
                                                        @if($se->image)
                                                            <img src="{{ Storage::url('Sedes/'.$se->image) }}" alt="regionales CEMTIC" class="img-radius align-top m-r-15" style="width:50px;height:50px;">
                                                        @else
                                                            <img src="{{ asset('admin/images/map_cemtic.png') }}" alt="regionales CEMTIC" class="img-radius align-top m-r-15" style="width:50px;height:50px;">
                                                        @endif
                                                        <div class="d-inline-block">
                                                            <br>
                                                            <p class="m-b-0"><strong>{{ Str::limit($se->name,20) }}</strong></p>
                                                        </div>
                                                    </div>
                                                </td>

                                                <td>{{ $se->created_at->format('d-m-Y') }} - <span class="pcoded-badge badge badge-warning">{{ LongTimeFilter($se->created_at) }}</td>
                                                <td>
                                                    <strong>{{ $se->in_charge }}</strong>
                                                </td>

                                                <td>
                                                    @if($se->is_published == 1 )
                                                        <span class="badge badge-light-success">ACTIVO</span>
                                                    @else
                                                        <span class="badge badge-light-danger">INACTIVO</span>
                                                    @endif

                                                    <div class="overlay-edit">
                                                        <a href="{{ route('dashboard.regional.show', [$se->id]) }}" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                                        <a href="" data-target="#modalDelete-{{ $se->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @include('dashboard.pages.regional.delete')

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    @endsection

    @section('extrajs')
        <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
        <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>

        <script>
        $(document).ready(function() {
            tinymce.init({
                selector: '.editor',
                language:'es_ES',
                plugins: "table",
                toolbar: "table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol!",
                max_height: 500,
                min_height: 350
            });
        });
        </script>
    @endsection('extrajs')
