@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Personal</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Personal</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card user-profile-list">
                <div class="card-body">
                    <div class="dt-responsive table-responsive">
                    <div class="row align-items-center m-l-0">
                        <div class="col-sm-6">

                        </div>
                        <div class="col-sm-6 text-right">
                            <button href="#" class="btn btn-success btn-sm btn-round has-ripple" data-toggle="modal" data-target="#modalCreate"><i class="feather icon-plus"></i> Nuevo Registro</button>
                        </div>
                    </div>

                    <table class="table nowrap dt">
                        <thead>
                        <tr class="text-dark text-center">
                            <th>Nro</th>
                            <th>NOMBRE</th>
                            <th>GRADO ACADEMICO</th>
                            <th>FECHA CREACIÓN</th>
                            <th>ESTADO</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach ($profetional as $cont=>$profesional)
                            <tr>
                                <td>{{ $cont+1 }}</td>
                                <td>
                                    <div class="d-inline-block align-middle">
                                        @if($profesional->image)
                                            <img src="{{ $profesional->image }}" alt="img profesionales" class="img-radius align-top m-r-15" style="width:50px;height:50px;">
                                        @else
                                            <img src="{{ asset('frontend/images/cemtic_user.png') }}" alt="img profesionales" class="img-radius align-top m-r-15" style="width:50px;height:50px;">
                                        @endif
                                        <div class="d-inline-block">
                                            <br>
                                            <p class="m-b-0"><strong>{{ Str::limit($profesional->name,20) }}</strong></p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{ strtoupper($profesional->grado) }}
                                </td>
                                <td>{{ $profesional->created_at->format('d-m-Y') }} - <span class="pcoded-badge badge badge-warning">{{ LongTimeFilter($profesional->created_at) }}</span></td>
                                <td>

                                    @if($profesional->is_published == 1 )
                                        <span class="badge badge-light-success">ACTIVO</span>
                                    @else
                                        <span class="badge badge-light-danger">INACTIVO</span>
                                    @endif

                                    <div class="overlay-edit">
                                        <a href="" data-target="#modalUpdate-{{ $profesional->id }}" data-toggle="modal" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                        <a href="" data-target="#modalDelete-{{ $profesional->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>
                                    </div>

                                </td>
                            </tr>

                            @include('dashboard.pages.profetionals.edit')

                            @include('dashboard.pages.profetionals.delete')

                            @endforeach
                        </tbody>
                    </table>

                    </div>
                </div>
            </div>
        </div>
            @include('dashboard.pages.profetionals.create')
    </div>
@endsection

@section('extrajs')
    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>

    <script>
    $(document).ready(function() {
        tinymce.init({
            selector: 'textarea',
            language:'es_ES',
        plugins: [
                "preview",
                "searchreplace wordcount visualblocks visualchars code fullscreen"
            ],
        a_plugin_option: true,
        a_configuration_option: 400
        });
    });


  function loadPreview(input, id) {
    id = id || '#preview_img';
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(id)
                    .attr('src', e.target.result)
                    .width(200)
                    .height(150);
        };

        reader.readAsDataURL(input.files[0]);
    }
 }
    </script>
@endsection('extrajs')
