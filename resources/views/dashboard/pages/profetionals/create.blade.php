<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Nuevo Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="card-body" id="app">

            {!! Form::open(['route' => 'dashboard.profetional.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                <label for="name">Nombre</label>
                <div class="input-group mb-2 mr-sm-2">
                    <input type="text" name="name" id="name" maxlength="150" class="form-control" value="{{old('name')}}">
                </div>

                <label for="grado">Grado</label>
                <div class="input-group mb-2 mr-sm-2">
                    <input type="text" name="grado" id="grado" maxlength="150" class="form-control" value="{{old('grado')}}">
                </div>

                <div class="form-group">
                    <label for="biography">Biografia</label>
                    <textarea class="form-control classic-editor" id="biography" name="biography" rows="3" value="{{old('biography')}}" placeholder="Descripción de la publicación"></textarea>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="is_published">Estado</label>
                        <select id="is_published" name="is_published" class="form-control" required>
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="gender">Género</label>
                        <select id="gender" name="gender" class="form-control" required>
                            <option value="1">Masculino</option>
                            <option value="0">Femenino</option>
                        </select>
                    </div>
                </div>

                <label for="image">Url imagen</label>
                <div class="input-group mb-2 mr-sm-2">
                    <input type="text" name="image" id="image" class="form-control" value="{{old('image')}}">
                </div>

                {{-- <div class="form-row">

                <img id="preview_img" src="{{ asset('admin/images/picture.png')}}" class="" width="80" height="80"/>

                    <div class="form-group col-md-4">
                        <div class="custom-file">
                            <label>Subir Imagen</label>
                            <label for="image"></label>
                            <input type="file" name="image" id="image" onchange="loadPreview(this);" class="form-control">
                        </div>
                    </div>
                </div> --}}

            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
            {{Form::Close()}}
        </div>

    </div>
    </div>
</div>
