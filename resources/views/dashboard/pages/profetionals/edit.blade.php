<div class="modal fade" id="modalUpdate-{{ $profesional->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Modificar Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        {!! Form::model($profesional,['route'=>['dashboard.profetional.update',$profesional->id],'method'=>'PUT','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

        <div class="card-body">
            <label for="name">Nombre</label>
            <div class="input-group mb-2 mr-sm-2">
                <input type="text" name="name" id="name_e" maxlength="150" class="form-control" value="{{ $profesional->name }}" >
            </div>

            <label for="grado">Grado</label>
            <div class="input-group mb-2 mr-sm-2">
                <input type="text" name="grado" id="grado" maxlength="150" class="form-control" value="{{ $profesional->grado }}">
            </div>

            <div class="form-group">
                <label for="biography">Biografia</label>
                <textarea class="form-control classic-editore" id="biography" name="biography" rows="3" maxlength="150" required> {{ $profesional->biography }}</textarea>
            </div>

            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="is_published">Estado</label>
                    <select id="is_published" name="is_published" class="form-control" required>
                        <option value="1" {{ ($profesional->is_published=='1') ? 'selected' : '' }}>Activo</option>
                        <option value="0" {{ ($profesional->is_published=='0') ? 'selected' : '' }}>Inactivo</option>
                    </select>
                </div>
            </div>

            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="gender">Género</label>
                    <select id="gender" name="gender" class="form-control" required>
                        <option value="1" {{ ($profesional->gender=='1') ? 'selected' : '' }}>Masculino</option>
                        <option value="0" {{ ($profesional->gender=='0') ? 'selected' : '' }}>Femenino</option>
                    </select>
                </div>
            </div>

            <label for="name">Url imagen</label>
            <div class="input-group mb-2 mr-sm-2">
                <input type="text" name="image" id="image" class="form-control" value="{{ $profesional->image }}">
            </div>

            {{-- <div class="custom-file">
                @if($profesional->image)
                    <img id="preview-img{{ $profesional->id}}" src="{{ Storage::url('Profetional/'.$profesional->image) }}" alt="Imagen" width="80" height="80" />
                @else
                    <img id="preview_img" src="{{ asset('admin/images/picture.png')}}" class="" width="80" height="80"/>
                @endif

                <label>Subir Imagen</label>
            <input type="file" name="image" id="image" onchange="document.getElementById('preview-img{{ $profesional->id}}').src = window.URL.createObjectURL(this.files[0])"/>
            </div> --}}

            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Modificar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

        {{Form::Close()}}

    </div>
    </div>
</div>
