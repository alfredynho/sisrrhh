@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Redes Sociales</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Redes Sociales</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-40">
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Categoria {{ $dt_cqa->name }} </h4>
                            <p>En el siguiente apartado se encuentra disponible para la modificación de las redes Sociales del UPEA CCE</p>
                            <hr>
                            <div class="row align-items-center m-l-0">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6 text-right">
                                    <a href="{{ route('dashboard.edubot.index') }}" class="btn btn-warning btn-sm btn-round has-ripple"> Regresar a Categirias</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card user-profile-list">
					<div class="card-body">
						<div class="dt-responsive table-responsive">
                        <div class="row align-items-center m-l-0">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6 text-right">
                                <button href="#" class="btn btn-success btn-sm btn-round has-ripple" data-toggle="modal" data-target="#modalCreate"><i class="feather icon-plus"></i> Nuevo Registro</button>
                            </div>
                        </div>

							<table class="table nowrap dt">
								<thead>
									<tr class="text-center">
                                        <th>NRO</th>
                                        <th>PREGUNTA</th>
                                        <th>RESPUESTA</th>
                                        <th>FECHA CREACION</th>
                                        <th>ACTIVO?</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($questions as $cont=>$qs)
                                        <tr class="text-center">
                                            <td>{{ $cont+1 }}</td>
                                            <td>
                                                <p class="m-b-0"><strong>{{ $qs->question }}</strong></p>
                                            </td>
                                            <td>
                                                {{ $qs->answer }}
                                            </td>
                                            <td>{{ $qs->created_at->format('d-m-Y') }}</td>
                                            <td>
                                                @if($qs->is_published == 1 )
                                                    <span class="badge badge-light-success">ACTIVO</span>
                                                @else
                                                    <span class="badge badge-light-danger">INACTIVO</span>
                                                @endif

                                                <div class="overlay-edit">
                                                    <a href="" data-target="#modalUpdate-{{ $qs->id }}" data-toggle="modal" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                                </div>
                                            </td>
                                        </tr>

                                        {{--  @include('dashboard.pages.social.edit')  --}}

                                    @endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
            @include('dashboard.pages.detquestions.create')
		</div>
@endsection

@section('extrajs')

@endsection('extrajs')
