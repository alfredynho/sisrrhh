<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Nuevo Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        {!! Form::open(['route' => 'dashboard.edubot.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}
            <div class="card-body" id="app">
                    <div class="form-group">
                        <label>Pregunta</label>
                        <textarea class="form-control" id="question" name="question" rows="3" value="{{old('question')}}" placeholder="Pregunta"></textarea>
                    </div>

                    <div class="form-group">
                        <label>Respuesta</label>
                        <textarea class="form-control" id="answer" name="answer" rows="3" value="{{old('answer')}}" placeholder="Respuesta"></textarea>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-6">
                            <label for="action">Categoria</label>
                                @if(count($select_category)>= '1')
                                    <select id="action" name="action" class="form-control" required>
                                        @foreach ($select_category as $selcat)
                                            <option readonly="readonly" value="{{$selcat->id}}" {{ ( $selcat->id == $dt_cqa->id ) ? 'selected' : '' }}>{{ strtoupper($selcat->name) }}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <br>
                                    <a href="{{ route('dashboard.categoria.index') }}" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i> Agregar Categoria</a>
                                @endif
                        </div>

                        <div class="form-group col-md-6">
                            <label for="status">Estado</label>
                            <select id="status" name="status" class="form-control" required>
                                <option value="1">ACTIVO</option>
                                <option value="0">INACTIVO</option>
                            </select>
                        </div>

                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </div>
            {{Form::Close()}}

    </div>
    </div>
</div>
