@extends('layouts.admin')

@section('content')

    <div class="page-title">
      <div class="row">
          <div class="col-sm-6">
              <h4 class="mb-0">Blogs </h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb pt-0 pr-0 float-left float-sm-right ">
              <li class="breadcrumb-item"><a href="#" class="default-color">Home</a></li>
              <li class="breadcrumb-item active">Blogs </li>
            </ol>
          </div>
        </div>
    </div>

    <!-- main body --> 
    <div id="vue-wrapper" class="row">   
      <div class="col-xl-12 mb-30">     
        <div class="card card-statistics h-100"> 
          <div class="card-body">
           <div class="d-block d-md-flex justify-content-between">
              <div class="d-block">
                <h5 class="card-title pb-0 border-0">Blogs</h5>
              </div>
              <div class="d-block d-md-flex clearfix sm-mt-20">
                 {{--  <div class="clearfix">
                   <div class="box">
                    <select class="fancyselect sm-mb-20 mr-20">
                      <option value="1">Some option</option>
                      <option value="2">Another option</option>
                      <option value="3">A option</option>
                      <option value="4">Potato</option>
                    </select>
                  </div>
                </div>  --}}

                <td><button data-toggle="modal" data-target=".bd-example-modal-lg" class="btn btn-outline-success btn-sm">View Order</button></td>

                 {{--  <div class="widget-search ml-0 clearfix">
                  <i class="fa fa-search"></i>
                  <input type="search" class="form-control" placeholder="Search....">
                </div>  --}}
               </div>
             </div>
             <div class="table-responsive mt-15">
              <table class="table center-aligned-table mb-0">
                <thead>
                  <tr class="text-dark text-center">
                    <th>NRO</th>
                    <th>TÍTULO</th>
                    <th>AUTOR</th>
                    <th>ESTADO</th>
                    <th>FECHA REGISTRO</th>
                    <th>OPCIONES</th>
                  </tr>
                </thead>
                <tbody class="text-center">
                    @forelse ($blog as $cont=>$bg)
                    <tr>
                        <td>{{ $cont+1 }}</td>
                        <td>{{ $bg->title }}</td>
                        <td>{{ $bg->category }}</td>
                        <td>
                            @if($bg->status == 1 )
                                <label class="badge badge-success">Activo</label>
                            @else
                                <label class="badge badge-danger">Inactivo</label>
                            @endif
                        </td>
                        <td>{{ $bg->created_at->format('d/m/Y') }}</td>
                        <td>

                            <button type="button" @click="abrirModal('categoria','actualizar',categoria)" class="btn btn-warning btn-sm" >
                            <i class="fa fa-pencil pr-1"></i>
                            </button> &nbsp;

                            <button type="button" @click="abrirModal('categoria','actualizar',categoria)" class="btn btn-danger btn-sm" >
                            <i class="fa fa-trash pr-1"></i>
                            </button> &nbsp;


                            {{--  <button href="#" class="btn btn-outline-warning" data-toggle="modal" data-target="#exampleModal">View Order</button>
                            <button href="#" class="btn btn-outline-danger">Cancel</button>  --}}
                        </td>
                    </tr>
                    @empty
                        <div class="alert alert-primary text-center" role="alert">
                            Actualmente no se tiene registros para mostrar!
                        </div>                    
                    @endforelse
                </tbody>
              </table>

              <div class="modal fade bd-example-modal-lg" id="add_post_model" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <div class="modal-title"><div class="mb-30">
                          <h6>Agregar nuevo Post</h6>
                        </div>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
            
                      <div class="card-body">
                          <form action method="post">
                              <div class="form-row">
                                  <div class="form-group col-md-6">
                                      <label for="titulo">Titulo</label>
                                      <input type="text" class="form-control" type="text" id="title" v-model="title ">
                                  </div>
                                  {{-- <div class="form-group col-md-6">
                                      <label for="inputPassword4">Password</label>
                                      <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
                                  </div> --}}
                              </div>
                              {{-- <div class="form-group">
                                  <label for="inputAddress">Address</label>
                                  <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                              </div>
                              <div class="form-group">
                                  <label for="inputAddress2">Address 2</label>
                                  <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
                              </div>
                              <div class="form-row">
                                  <div class="form-group col-md-6">
                                      <label for="inputCity">City</label>
                                      <input type="text" class="form-control" id="inputCity">
                                  </div>
                                  <div class="form-group col-md-4">
                                      <label for="inputState">State</label>
                                      <select id="inputState" class="form-control">
                                          <option selected>Choose...</option>
                                          <option>...</option>
                                      </select>
                                  </div>
                                  <div class="form-group col-md-2">
                                      <label for="inputZip">Zip</label>
                                      <input type="text" class="form-control" id="inputZip">
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="form-check">
                                      <input class="form-check-input" type="checkbox" id="gridCheck">
                                      <label class="form-check-label" for="gridCheck">
                                          Check me out
                                      </label>
                                  </div>
                              </div> --}}
                          </form>
                      </div>
            
                      <div class="modal-footer">
                        {{-- <td><button @click="deleteItem(item)" class="btn btn-outline-danger btn-sm">Eliminar</button></td> --}}
                        <button type="button" @click="createPost()" class="btn btn-secondary">Guardar</button>
                        <button class="btn btn-primary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
                
            </div>
          </div>
        </div>   
      </div>
  </div> 

@endsection

@section('extrajs')

@endsection('extrajs')