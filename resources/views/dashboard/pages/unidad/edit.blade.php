<div class="modal fade" id="modalUpdate-{{ $soc->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Modificar Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        {!! Form::model($soc,['route'=>['dashboard.unidad.update',$soc->id],'method'=>'PUT','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

        <div class="card-body">

            <label for="codigo">Nombre Codigo</label>
            <div class="input-group mb-2 mr-sm-2">
                <input type="text" name="codigo" id="codigo" maxlength="150" class="form-control" value="{{ $soc->codigo }}" >
            </div>

            <label for="nombre">Nombre</label>
            <div class="input-group mb-2 mr-sm-2">
                <input type="text" name="nombre" id="nombre" maxlength="150" class="form-control" value="{{ $soc->nombre }}" >
            </div>

            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="estado">Estado</label>
                    <select id="estado" name="estado" class="form-control" required>
                        <option value="1" {{ ($soc->estado=='1') ? 'selected' : '' }}>Activo</option>
                        <option value="0" {{ ($soc->estado=='0') ? 'selected' : '' }}>Inactivo</option>
                    </select>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Modificar</button>
            </div>
        </div>

        {{Form::Close()}}

    </div>
    </div>
</div>
