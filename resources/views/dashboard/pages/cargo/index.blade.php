@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Cargos</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Cargos</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card user-profile-list">
					<div class="card-body">
						<div class="dt-responsive table-responsive">

                            <div class="row align-items-center m-l-0">
                                <div class="col-sm-6">
    
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button href="#" class="btn btn-success btn-sm btn-round has-ripple" data-toggle="modal" data-target="#modalCreate"><i class="feather icon-plus"></i> Nuevo Registro</button>
                                    <a href="{{ route('dashboard.cargo.reporte') }}">Imprime el archivo</a>
                                </div>
                            </div>

							<table class="table nowrap dt">
								<thead>
									<tr class="text-center">
                                        <th>NRO</th>
                                        <th>NOMBRE</th>
                                        <th>NOMBRE DE UNIDAD</th>
                                        <th>ACTIVO?</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($cargos as $cont=>$soc)
                                        <tr class="text-center">
                                            <td>{{ $cont+1 }}</td>
                                            <td>
                                                {{ Str::limit($soc->nombre,70,'...') }}
                                            </td>
                                            <td>{{ $soc->nombre_unidad }}</td>
                                            <td>
                                                @if($soc->estado == 1 )
                                                    <span class="badge badge-light-success">ACTIVO</span>
                                                @else
                                                    <span class="badge badge-light-danger">INACTIVO</span>
                                                @endif

                                                <div class="overlay-edit">
                                                    <a href="" data-target="#modalUpdate-{{ $soc->id }}" data-toggle="modal" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                                    <a href="" data-target="#modalDelete-{{ $soc->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>
                                                    <a href="" data-target="#modalDelete-{{ $soc->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>
                                                </div>
                                            </td>
                                        </tr>

                                        @include('dashboard.pages.cargo.edit')

                                        @include('dashboard.pages.cargo.delete')

                                    @endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
                @include('dashboard.pages.cargo.create')
		</div>
@endsection

@section('extrajs')

@endsection('extrajs')
