<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="modalCreate">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Nuevo Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="card-body" id="app">

            {!! Form::open(['route' => 'dashboard.profesion.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                <label for="codigo">Codigo</label>
                <div class="input-group mb-2 mr-sm-2">
                    <input type="text" name="codigo" id="codigo" maxlength="150" class="form-control" value="{{old('codigo')}}">
                </div>

                <label for="nombre">Nombre</label>
                <div class="input-group mb-2 mr-sm-2">
                    <input type="text" name="nombre" id="nombre" maxlength="150" class="form-control" value="{{old('nombre')}}">
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="estado">Estado</label>
                        <select id="estado" name="estado" class="form-control" required>
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>
                    </div>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{Form::Close()}}
        </div>

    </div>
    </div>
</div>
