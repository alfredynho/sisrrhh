<!DOCTYPE html>
<html>
<head>
    <title>Reporte Funcionarios INSO {{ $personal->nombres }} {{ $personal->paterno }}</title>
    <style type="text/css">
    body{
        font-size: 16px;
        font-family: "Arial";
    }
    table{
        border-collapse: collapse;
    }
    td{
        padding: 6px 5px;
        font-size: 15px;
    }
    .h1{
        font-size: 21px;
        font-weight: bold;
    }
    .h2{
        font-size: 18px;
        font-weight: bold;
    }
    .tabla1{
        margin-bottom: 20px;
    }
    .tabla2 {
        margin-bottom: 20px;
    }
    .tabla3{
        margin-top: 15px;
    }
    .tabla3 td{
        border: 1px solid #000;
    }
    .tabla3 .cancelado{
        border-left: 0;
        border-right: 0;
        border-bottom: 0;
        border-top: 1px dotted #000;
        width: 200px;
    }
    .emisor{
        color: red;
    }
    .linea{
        border-bottom: 1px dotted #000;
    }
    .border{
        border: 1px solid #000;
    }
    .fondo{
        background-color: #dfdfdf;
    }
    .fisico{
        color: #fff;
    }
    .fisico td{
        color: #fff;
    }
    .fisico .border{
        border: 1px solid #fff;
    }
    .fisico .tabla3 td{
        border: 1px solid #fff;
    }
    .fisico .linea{
        border-bottom: 1px dotted #fff;
    }
    .fisico .emisor{
        color: #fff;
    }
    .fisico .tabla3 .cancelado{
        border-top: 1px dotted #fff;
    }
    .fisico .text{
        color: #000;
    }
    .fisico .fondo{
        background-color: #fff;
    }

</style>
</head>
<body>
    <div class="">
        <table width="100%" class="tabla1">
            <tr>
                <td width="73%" align="center"><img id="logo" src="../public/admin/images/inso.png" style="position:fixed;" alt="LOGO INSO" width="200" height="200"></td>
                <td width="27%" rowspan="3" align="center" style="padding-right:0">
                    <table width="100%">
                        <tr>
                            <td height="60" align="center" class="border"><span class="h2">FECHA DE INGRESO : <br>{{ $personal->fecha_ingreso }}</span></td>
                        </tr>
                        <tr>
                            <td height="55" align="center" class="border"><span class="h1">BOLETA FUNCIONARIO</span></td>
                        </tr>
                        <tr>
                            <td height="50" align="center" class="border">000-{{ $personal->id }}- Nº <span class="text">{{ $personal->cedula }}</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"><img src="../public/admin/images/banner.png" style="position:fixed;z-index:120; bottom:5px; right:5px;" alt="Banner INSO"  width="400" height="70"></td>
            </tr>
        </table>
        <table width="100%" class="tabla2">
            <tr>
                <td width="11%">Funcionario:</td>
                <td width="37%" class="linea"><span class="text">{{ $personal->nombres }} {{ $personal->paterno }} {{ $personal->materno }}</span></td>
                <td width="5%">&nbsp;</td>
                <td width="13%">&nbsp;</td>
                <td width="4%">&nbsp;</td>
            </tr>
            <tr>
                <td>Cargo:</td>
                <td class="linea"><span class="text">{{ $personal->nombre_cargo }}</span></td>
                <td>Cédula:</td>
                <td class="linea"><span class="text">{{ $personal->cedula }}</span></td>
            </tr>
        </table>

        Nº <span class="text">{{ $personal->cedula }}</span>

        <table width="100%" class="tabla3">
            <tr>
                <td align="center" class="fondo"><strong>CANT.</strong></td>
                <td align="center" class="fondo"><strong>DESCRIPCIÓN</strong></td>
                <td align="center" class="fondo"><strong>P. UNITARIO</strong></td>
                <td align="center" class="fondo"><strong>IMPORTE</strong></td>
            </tr>
            <tr>
                <td width="7%" align="center"><span class="text">UNO</span></td>
                <td width="59%"><span class="text"> DOS</span></td>
                <td width="16%" align="right"><span class="text"> TRES</span></td>
                <td width="18%" align="right"><span class="text"> CUATRO</span></td>
            </tr>
            <tr>
                <td style="border:0;">&nbsp;</td>
                <td style="border:0;">&nbsp;</td>
                <td align="right"><strong>TOTAL S/.</strong></td>
                <td align="right"><span class="text">EL TOTAL</span></td>
            </tr>
            <tr>
                <td style="border:0;">&nbsp;</td>
                <td align="center" style="border:0;">
                    <table width="200" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="center" class="cancelado">CANCELADO</td>
                        </tr>
                    </table>
                </td>
                <td style="border:0;">&nbsp;</td>
                <td align="center" style="border:0;" class="emisor"><strong>EMISOR</strong></td>
            </tr>
        </table>
    </div>
</body>
</html>