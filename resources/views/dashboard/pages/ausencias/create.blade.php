@extends('layouts.admin')

@section('extracss')

@endsection('extracss')
    <link href="{{ asset('admin/plugins/select2.min.css')}}" rel="stylesheet">
@section('content')

    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">{{ $message }}</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">{{ $message }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Registro de Ausencias</h5>
                </div>
                <div class="card-body">

                    @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    {!! Form::open(['route' => 'dashboard.ausencias.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Personal</label>
                                    <select class="form-control select2" name="personal_id">
                                        @foreach ($personal as $persona)
                                            <option value="{{ $persona->id }}">{{ $persona->nombres }} {{ $persona->paterno }} {{ $persona->materno }}</option>                                            
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Fecha de ausencia</label>
                                    <input type="text" name="fecha_ausencia" id="fecha_ausencia" maxlength="50" class="fechaIngreso form-control" value="{{old('fecha_ausencia')}}">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Justificado?</label>
                                    <select class="form-control" name="justificado">
                                        <option value="1">Si</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Observaciones</label>
                                    <textarea class="form-control editor" name="observaciones"></textarea>
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <a href="{{ URL::previous() }}" type="button" class="btn btn-warning">Regresar</a>

                    {{Form::Close()}}

                </div>
            </div>
        </div>

    </div>

@endsection('content')

@section('extrajs')
    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2.min.js') }}"></script>

    <script>
    $(document).ready(function() {
        $('.select2').select2();

        tinymce.init({
            selector: '.editor',
            language:'es_ES',
            plugins: "table",
            toolbar: "table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol!",
            max_height: 500,
            min_height: 350
        });
    });
    </script>
@endsection('extrajs')
