@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Galeria de Imagenes</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Galeria de Imagenes</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

    </div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card user-profile-list">
					<div class="card-body">
						<div class="dt-responsive table-responsive">
                        <div class="row align-items-center m-l-0">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6 text-right">
                                <a href="{{ route('dashboard.images.create') }}" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i> Nuevo Registro</a>
                            </div>
                        </div>
                        <div class="table-responsive">

							<table class="table nowrap dt">
								<thead>
									<tr class="text-center">
                                        <th>Nro</th>
                                        <th>NOMBRE</th>
                                        <th>FECHA CREACIÓN</th>
                                        <th>DESTACADO?</th>
                                        <th>ACTIVO?</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($images as $cont=>$ga)
                                        <tr class="text-center">
                                            <td>{{ $cont+1 }}</td>
                                            <td>
                                                <div class="d-inline-block align-middle">
                                                    <img src="{{ Storage::url('Gallery/'.$ga->image) }}" alt="Imagen Galeria" class="img-radius align-top m-r-15" style="width:50px;height:50px;">
                                                    <div class="d-inline-block">
                                                        <br>
                                                        <p class="m-b-0"><strong>{{ $ga->title }}</strong></p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                @if($ga->destacado == 1 )
                                                    <span class="badge badge-light-success">SI</span>
                                                @else
                                                    <span class="badge badge-light-danger">NO</span>
                                                @endif
                                            </td>
                                            <td>{{ $ga->created_at->format('d-m-Y') }}</td>
                                            <td>
                                                @if($ga->is_published == 1 )
                                                    <span class="badge badge-light-success">ACTIVO</span>
                                                @else
                                                    <span class="badge badge-light-danger">INACTIVO</span>
                                                @endif

                                                <div class="overlay-edit">
                                                    <a href="" data-target="#modalUpdate-{{ $ga->id }}" data-toggle="modal" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                                    <a href="" data-target="#modalDelete-{{ $ga->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>
                                                </div>
                                            </td>
                                        </tr>

                                        {{-- @include('dashboard.pages.gallery.edit')
                                        @include('dashboard.pages.gallery.delete') --}}

                                    @endforeach
								</tbody>
							</table>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection

@section('extrajs')
    <script src="{{ asset('admin/js/plugins/ckeditor.js')}}"></script>
    <script type="text/javascript">
        $(window).on('load', function() {
            ClassicEditor.create(document.querySelector('.classic-editor'))
                .catch(error => {
                    console.error(error);
                });

            ClassicEditor.create(document.querySelector('.classic-editore'))
                .catch(error => {
                    console.error(error);
                });
        });

    </script>

@endsection('extrajs')
