@extends('layouts.admin')


@section('content')

    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Galeria</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Galeria</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Registro de Galeria</h5>
                </div>
                <div class="card-body">

                    @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    {!! Form::open(['route' => 'dashboard.images.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Nombre</label>
                                    <input type="text" class="form-control" name="name" id="carrera">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Encargado</label>
                                    <input type="text" class="form-control" name="in_charge">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Categoria</label>
                                    @if(count($categorys) <= 1)
                                        <select class="form-control" name="category">
                                            @foreach ($categorys as $ct)
                                                <option value="1">Si</option>
                                            @endforeach
                                        </select>
                                    @else
                                        </br>
                                        <label class="form-label">Sin Registros</label>
                                        <button type="submit" class="btn btn-primary">Agregar Categoria</button>
                                    @endif

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Publicado?</label>
                                    <select class="form-control" name="is_published">
                                        <option value="1">Si</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Descripción</label>
                                    <textarea class="form-control editor" name="description"></textarea>
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a href="{{ URL::previous() }}" type="button" class="btn btn-warning">Regresar</a>

                    {{Form::Close()}}

                </div>
            </div>
        </div>

    </div>

@endsection

@section('extrajs')
    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>

    <script>
    $(document).ready(function() {
        tinymce.init({
            selector: '.editor',
            language:'es_ES',
            plugins: "table",
            toolbar: "table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol!",
            max_height: 500,
            min_height: 350
        });
    });
    </script>

@endsection('extrajs')
