<div class="modal fade" id="modalDelete-{{ $pq->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Borrar Elemento</strong></h4>
                </div>
            </div>w
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        {{Form::open(array('action'=>array('Dashboard\PaquetesController@destroy',$pq->id),'method'=>'delete'))}}
            {{ Form::token() }}
            <h3 class="text-center">{{ $pq->title }}</h3>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Eliminar</button>
            </div>
        {{Form::Close()}}
    </div>
    </div>
</div>
