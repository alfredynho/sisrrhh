@extends('layouts.admin')

@section('pushcss')

@endsection

@section('content')

<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5 class="m-b-10">Módulo Curso</h5>
                </div>
                <ul class="breadcrumb">
                    @include('dashboard.includes.navadmin')
                    <li class="breadcrumb-item"><a href="#!">Curso</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>Modificar de Curso</h5>
            </div>
            <div class="card-body">

                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::model($course,['route'=>['dashboard.course.update',$course->id],'method'=>'PUT','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Nombre Curso</label>
                                <input type="text" name="title" id="title" maxlength="150" class="form-control" value="{{ $course->title }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Publicado?</label>
                                <select class="form-control" name="is_published">
                                    <option value="1" {{ ($course->is_published=='1') ? 'selected' : '' }}>Activo</option>
                                    <option value="0" {{ ($course->is_published=='0') ? 'selected' : '' }}>Inactivo</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Tipo</label>
                                <select class="form-control" id="type" name="type">
                                    <option value="CEMTIC-V" {{ ($course->type=='CEMTIC-V') ? 'selected' : '' }}>Virtual</option>
                                    <option value="CEMTIC-P" {{ ($course->type=='CEMTIC-P') ? 'selected' : '' }}>Presencial</option>
                                    <option value="CEMTIC-SP" {{ ($course->is_published=='CEMTIC-SP') ? 'selected' : '' }}>Semi-Presencial</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Docente</label>
                                @if(count($profetionals) >= 1)
                                    <select id="id_teacher" class="form-control" name="id_teacher">
                                        @foreach ($profetionals as $prof)
                                            <option value="{{ $prof->id }}" {{ ($prof->id==$course->id_teacher) ? 'selected' : '' }}>{{ $prof->grado }} - {{ $prof->name }}</option>                                        
                                        @endforeach
                                    </select>
                                @else
                                    <a href="{{ route('dashboard.profetional.index') }}" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i> Crear Docente</a>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Descripción</label>
                                <textarea class="form-control editor" name="description"> {{ $course->description }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Contenido</label>
                                <textarea class="form-control editor" name="content">{{ $course->content }}</textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Url imagen</label>
                                <input type="text" name="image" id="image" class="form-control" value="{{ $course->image }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Destacado?</label>
                                <select class="form-control" name="destacado">
                                    <option value="1" {{ ($course->destacado=='1') ? 'selected' : '' }}>Si</option>
                                    <option value="0" {{ ($course->destacado=='0') ? 'selected' : '' }}>No</option>
                                </select>
                            </div>
                        </div>


                        {{-- <img id="preview_img" src="{{ asset('admin/images/picture.png')}}" class="" width="80" height="80"/>

                        <div class="form-group col-md-4">
                            <div class="custom-file">
                                <label>Subir Imagen</label>
                                <label for="image"></label>
                                <input type="file" name="image" id="image" onchange="loadPreview(this);" class="form-control">
                            </div>
                        <br><br>
                        <br><br>
                        </div>  --}}

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Enlace Formulario</label>
                                <input type="text" name="form" id="form" class="form-control" value="{{ $course->form }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Es Nuevo?</label>
                                <select class="form-control" name="is_new">
                                    <option value="1" {{ ($course->is_new=='1') ? 'selected' : '' }}>Si</option>
                                    <option value="0" {{ ($course->is_new=='0') ? 'selected' : '' }}>No</option>
                                </select>
                            </div>
                        </div>                            

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Fecha Inicio</label>
                                <input type="text" name="date" id="date" class="form-control" value="{{ $course->date }}">
                            </div>
                        </div>    

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Nro Participantes</label>
                                <input type="text" name="numparticipants" id="numparticipants" class="form-control" value="{{ $course->numparticipants }}">
                            </div>
                        </div>  
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Duración</label>
                                <input type="text" name="duration" id="duration" class="form-control" value="{{ $course->duration }}">
                            </div>
                        </div>  

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Nivel</label>
                                <input type="text" name="level" id="level" class="form-control" value="{{ $course->level }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Costo</label>
                                <input type="text" name="price" id="price" class="form-control" value="{{ $course->price }}">
                            </div>
                        </div>  

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Nro Módulos</label>
                                <input type="text" name="modules" id="modules" class="form-control" value="{{ $course->modules }}">
                            </div>
                        </div>  

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Certificado?</label>
                                <select class="form-control" name="certification">
                                    <option value="1" {{ ($course->certification=='1') ? 'selected' : '' }}>Si</option>
                                    <option value="0" {{ ($course->certification=='0') ? 'selected' : '' }}>No</option>
                                </select>
                            </div>
                        </div> 

                    </div>
                    <button type="submit" class="btn btn-success">Modificar</button>
                    <a href="{{ URL::previous() }}" type="button" class="btn btn-warning">Regresar</a>

                {{Form::Close()}}

            </div>
        </div>
    </div>

</div>
@endsection

@section('extrajs')
    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>

    <script>
    $(document).ready(function() {
        tinymce.init({
            selector: '.editor',
            language:'es_ES',
            plugins: "table",
            toolbar: "table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol!",
            max_height: 500,
            min_height: 350
        });
    });
    </script>

@endsection
