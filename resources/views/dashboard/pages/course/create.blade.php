@extends('layouts.admin')

@section('content')

    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Curso</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Curso</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Registro de Curso</h5>
                </div>
                <div class="card-body">

                    @if(count($errors)>0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open(['route' => 'dashboard.course.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Nombre Curso</label>
                                    <input type="text" name="title" id="title" maxlength="150" class="form-control" value="{{old('title')}}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Publicado?</label>
                                    <select class="form-control" name="is_published">
                                        <option value="1">Si</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Tipo</label>
                                    <select class="form-control" id="type" name="type">
                                        <option value="CEMTIC-V">Virtual</option>
                                        <option value="CEMTIC-P">Presencial</option>
                                        <option value="CEMTIC-SP">Semi-Presencial</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Docente</label>
                                    @if(count($profetionals) >= 1)
                                        <select id="id_teacher" class="form-control" name="id_teacher">
                                            @foreach ($profetionals as $prof)
                                                <option value="{{ $prof->id }}">{{ $prof->grado }} - {{ $prof->name }}</option>                                        
                                            @endforeach
                                        </select>
                                    @else
                                        <a href="{{ route('dashboard.profetional.index') }}" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i> Crear Docente</a>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Descripción</label>
                                    <textarea class="form-control editor" name="description"></textarea>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Contenido</label>
                                    <textarea class="form-control editor" name="content"></textarea>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Url imagen</label>
                                    <input type="text" name="image" id="image" class="form-control" value="{{old('image')}}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Destacado?</label>
                                    <select class="form-control" name="destacado">
                                        <option value="1">Si</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>

                            {{-- <img id="preview_img" src="{{ asset('admin/images/picture.png')}}" class="" width="80" height="80"/>

                            <div class="form-group col-md-4">
                                <div class="custom-file">
                                    <label>Subir Imagen</label>
                                    <label for="image"></label>
                                    <input type="file" name="image" id="image" onchange="loadPreview(this);" class="form-control">
                                </div>
                            <br><br>
                            <br><br>
                            </div>  --}}

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Enlace Formulario:</label>
                                    <input type="text" name="form" id="form" class="form-control" value="{{old('form')}}">
                                </div>
                            </div>    
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Es Nuevo?</label>
                                    <select class="form-control" name="is_new">
                                        <option value="1">Si</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>                            

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Fecha Inicio:</label>
                                    <input type="text" name="date" id="date" class="form-control" value="{{old('date')}}">
                                </div>
                            </div>   
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Nro de Participantes:</label>
                                    <input type="text" name="numparticipants" id="numparticipants" class="form-control" value="{{old('numparticipants')}}">
                                </div>
                            </div>   


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Duración:</label>
                                    <input type="text" name="duration" id="duration" class="form-control" value="{{old('duration')}}">
                                </div>
                            </div>   
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Nivel:</label>
                                    <input type="text" name="level" id="level" class="form-control" value="{{old('level')}}">
                                </div>
                            </div>  
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Costo:</label>
                                    <input type="text" name="price" id="price" class="form-control" value="{{old('price')}}">
                                </div>
                            </div>   
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Nro de Módulos</label>
                                    <input type="text" name="modules" id="modules" class="form-control" value="{{old('modules')}}">
                                </div>
                            </div>   

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Certificado?</label>
                                    <select class="form-control" name="certification">
                                        <option value="1">Si</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-success">Guardar</button>
                        <a href="{{ URL::previous() }}" type="button" class="btn btn-warning">Regresar</a>

                    {{Form::Close()}}

                </div>
            </div>
        </div>

    </div>

@endsection

@section('extrajs')
    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>

    <script>
    $(document).ready(function() {
        tinymce.init({
            selector: '.editor',
            language:'es_ES',
            plugins: "table",
            toolbar: "table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol!",
            max_height: 500,
            min_height: 350
        });
    });
    </script>

@endsection('extrajs')
