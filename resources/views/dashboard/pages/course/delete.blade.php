<div class="modal fade text-xs-left" id="modalDelete-{{ $cs->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header bg-danger white">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel10">Eliminar Registro</h4>
        </div>
        <div class="modal-body">

        {{Form::open(array('action'=>array('Dashboard\CourseController@destroy',$cs->id),'method'=>'delete'))}}
        {{ Form::token() }}

        <h5>Esta seguro de eliminar el Registro {{ $cs->title }}?</h5>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-outline-danger">Borrar</button>
        </div>
        {{Form::Close()}}
    </div>
    </div>
