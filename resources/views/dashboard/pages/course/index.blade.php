@extends('layouts.admin')

@section('extracss')

@endsection('extracss')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Cursos</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Cursos</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card user-profile-list">
					<div class="card-body">
						<div class="dt-responsive table-responsive">
                        <div class="row align-items-center m-l-0">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6 text-right">
                                <a href="{{ route('dashboard.profetional.index') }}" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i> Nuevo Docente</a>
                                <a href="{{ route('dashboard.course.create') }}" class="btn btn-success btn-sm btn-round has-ripple"><i class="feather icon-plus"></i> Nuevo Curso</a>
                                <a href="{{ route('export-csv-blog') }}" class="btn btn-info btn-sm btn-round has-ripple"><i class="feather icon-file-text"></i> Exportar CSV</a>
                            </div>

                        </div>

							<table class="table nowrap dt">
								<thead>
									<tr class="text-center">
                                        <th>Nro</th>
                                        <th>TÍTULO</th>
                                        <th>TIPO</th>
                                        <th>DOCENTE</th>
                                        <th>FECHA CREACIÓN</th>
                                        <th>ACTIVO?</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($courses as $cont=>$cs)
                                        <tr class="text-center">
                                            <td>{{ $cont+1 }}</td>
                                            <td>{{ $cs->title }}</td>
                                            <td>
                                                @if($cs->type == 'CEMTIC-V')
                                                    VIRTUAL
                                                @elseif($cs->type == 'CEMTIC-P')
                                                    PRESENCIAL
                                                @else
                                                    SEMIPRESENCIAL
                                                @endif
                                            </td>
                                            <td>{{ $cs->nombre_docente }}</td>
                                            <td>{{ $cs->created_at }}</td>
                                            <td>
                                                @if($cs->is_published == '1' )
                                                    <span class="badge badge-light-success">ACTIVO</span>
                                                @else
                                                    <span class="badge badge-light-danger">INACTIVO</span>
                                                @endif

                                                <div class="overlay-edit">
                                                    <a href="{{ route('dashboard.course.show', [$cs->slug]) }}" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                                    <a href="" data-target="#modalDelete-{{ $cs->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>
                                                </div>
                                            </td>
                                        </tr>

                                        @include('dashboard.pages.course.delete')

                                    @endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection

@section('extrajs')

@endsection('extrajs')
