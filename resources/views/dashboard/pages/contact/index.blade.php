@extends('layouts.admin')

@section('extracss')

@endsection('extracss')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Módulo Blog</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Blog</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Conteo de Registros</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2 col-xs-6 border-right">
                            <h3>{{ $blog->count() }}</h3>
                            <h6 class="text-success">Publicaciones Registradas</h6>
                        </div>
                        <div class="col-md-2 col-xs-6 border-right">
                            <h3>{{ $b_activos }}</h3>
                            <h6 class="text-primary">Activos</h6>
                        </div>
                        <div class="col-md-2 col-xs-6 border-right">
                            <h3>{{ $b_destacados }}</h3>
                            <h6 class="">Destacados</h6>
                        </div>
                        <div class="col-md-2 col-xs-6 border-right">
                            <h3>{{ $b_t_blog }}</h3>
                            <h6 class="text-success">Tipo Blog</h6>
                        </div>
                        <div class="col-md-2 col-xs-6">
                            <h3>{{ $b_t_noticia }}</h3>
                            <h6 class="text-success">Tipo Noticia</h6>
                        </div>

                        <div class="col-md-2 col-xs-6">
                            <h3>{{ $b_t_anuncio }}</h3>
                            <h6 class="text-success">Tipo Anuncio</h6>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card user-profile-list">
					<div class="card-body">
						<div class="dt-responsive table-responsive">
                        <div class="row align-items-center m-l-0">
                            <div class="col-sm-6">

                            </div>
                            <div class="col-sm-6 text-right">
                                <button href="#" class="btn btn-success btn-sm btn-round has-ripple" data-toggle="modal" data-target="#modalCreate"><i class="feather icon-plus"></i> Nuevo Registro</button>
                            </div>
                        </div>

							<table class="table nowrap dt">
								<thead>
									<tr class="text-center">
                                        <th>Nro</th>
                                        <th>TÍTULO</th>
                                        <th>CATEGORIA</th>
                                        <th>DESTACADO?</th>
                                        <th>FECHA CREACIÓN</th>
                                        <th>ACTIVO?</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($blog as $cont=>$bg)
                                        <tr class="text-center">
                                            <td>{{ $cont+1 }}</td>
                                            <td>{{ $bg->title }}</td>
                                            <td>{{ $bg->category }}</td>
                                            <td>
                                                @if($bg->destacado == 1 )
                                                    <span class="badge badge-light-success">SI</span>
                                                @else
                                                    <span class="badge badge-light-danger">NO</span>
                                                @endif

                                            </td>
                                            <td>{{ $bg->created_at->format('d-m-Y') }}</td>
                                            <td>
                                                    @if($bg->is_published == 1 )
                                                        <span class="badge badge-light-success">ACTIVO</span>
                                                    @else
                                                        <span class="badge badge-light-danger">INACTIVO</span>
                                                    @endif

                                                    <div class="overlay-edit">
                                                        <a href="" data-target="#modalUpdate-{{ $bg->id }}" data-toggle="modal" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                                        <a href="" data-target="#modalDelete-{{ $bg->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>
                                                    </div>
                                            </td>
                                        </tr>

                                        @include('dashboard.pages.blog.edit')
                                        @include('dashboard.pages.blog.delete')

                                    @endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
                @include('dashboard.pages.blog.create')
		</div>
@endsection

@section('extrajs')
    <script src="{{ asset('admin/js/plugins/ckeditor.js')}}"></script>
    <script type="text/javascript">
        $(window).on('load', function() {
            ClassicEditor.create(document.querySelector('.classic-editor1'))
                .catch(error => {
                    console.error(error);
                });

            ClassicEditor.create(document.querySelector('.classic-editore1'))
                .catch(error => {
                    console.error(error);
                });

        });
    </script>

    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>

    <script>
    $(document).ready(function() {
        tinymce.init({
            selector: 'textarea'
        });
    });
    </script>
@endsection('extrajs')
