<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Nuevo Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        {!! Form::open(['route' => 'dashboard.blog.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}
            <div class="card-body" id="app">

                    <label for="title">Título</label>
                    <div class="input-group mb-2 mr-sm-2">
                        <input type="text" name="title" id="title_c" maxlength="50" class="form-control" value="{{old('title')}}">
                    </div>

                    <div class="form-group">
                        <label>Descripción</label>
                        <textarea class="form-control classic-editor" id="description" name="description" rows="3" value="{{old('description')}}" placeholder="Descripción de la publicación"></textarea>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-6">
                            <label for="category">Categoria</label>
                            <select id="category" name="category" class="form-control" required>
                                <option value="BLOG">BLOG</option>
                                <option value="NOTICIA">NOTICIA</option>
                                <option value="ANUNCIO">ANUNCIO</option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="status">Estado</label>
                            <select id="status" name="status" class="form-control" required>
                                <option value="1">ACTIVO</option>
                                <option value="0">INACTIVO</option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="status">Destacado</label>
                            <select id="status" name="destacado" class="form-control" required>
                                <option value="1">SI</option>
                                <option value="0">NO</option>
                            </select>
                        </div>

                        <div class="custom-file">
                            <label>Subir Imagen</label>
                            <input type="file" name="image" id="image"/>
                        </div>

                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success">Guardar</button>
                </div>
            </div>
            {{Form::Close()}}

    </div>
    </div>
</div>
