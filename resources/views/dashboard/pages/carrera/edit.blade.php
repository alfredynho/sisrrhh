<div class="modal fade bd-example-modal-lg" id="modalUpdate-{{ $ca->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Modificar Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        {!! Form::model($ca,['route'=>['dashboard.carrera.update',$ca->id],'method'=>'PUT','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}
            <div class="card-body">

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="carrera" id='remainingC'>Nombre Carrera</label>
                        <input type="text" class="form-control" id="carrera_i" name="carrera" maxlength="60" placeholder="Ingrese nombre de la Carrera" value="{{ $ca->nombre }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="description"><strong>Descripción</strong></label>
                    <textarea class="form-control editor" id="description" name="description" rows="3" maxlength="150"> {{ $ca->description }}</textarea>
                </div>

                <div class="form-group col-md-12">
                    <label for="profile"><strong>Perfil Profesional</strong></label>
                    <textarea class="form-control" id="profile" name="profile" rows="3">{{ $ca->perfil_profesional }}</textarea>
                </div>

                <div class="form-group col-md-12">
                    <label for="aptitude"><strong>Aptitudes/Habilidades/Destrezas</strong></label>
                    <textarea class="form-control" id="aptitude" name="aptitude" rows="6" > {{ $ca->aptitudes_habilidades_destrezas }}</textarea>
                </div>

                <div class="form-group col-md-12">
                    <label for="objetive_carrera"><strong>Objetivo Carrera<strong></label>
                    <textarea class="form-control" id="objetive_carrera" name="objetive_carrera" rows="3" >{{ $ca->objetivo_carrera }}</textarea>
                </div>

                <div class="form-group col-md-12">
                    <label for="objetive_year"><strong>Objetivos por año</strong></label>
                    <textarea class="form-control" id="objetive_year" name="objetive_year" rows="3" >{{ $ca->objetivosxanio }}</textarea>
                </div>

                <div class="form-group col-md-12">
                    <label for="objetive_work"><strong>Objetivo Trabajo</strong></label>
                    <textarea class="form-control" id="objetive_work" name="objetive_work" rows="3" >{{ $ca->objetivo_trabajo }}</textarea>
                </div>

                <div class="form-group col-md-12">
                    <label for="pensum"><strong>Pensum</strong></label>
                    <textarea class="form-control editor" id="pensum" name="pensum" rows="3" >{{ $ca->pensum }}</textarea>
                </div>

                <div class="form-group col-md-12">
                    <label for="jefe_carrera"><strong>Jefe Carrera</strong></label>
                    <textarea class="form-control" id="jefe_carrera" name="jefe_carrera" rows="3" >{{ $ca->jefe_carrera }}</textarea>
                </div>

                <div class="form-group col-md-12">
                    <label for="fundacion"><strong>Fundación</strong></label>
                    <textarea class="form-control" id="fundacion" name="fundacion" rows="3" >{{ $ca->fundacion }}</textarea>
                </div>

                <div class="form-group col-md-12">
                    <label for="status"><strong>Estado</strong></label>
                    <select id="status" name="status" class="form-control" required>
                        <option value="1">Si</option>
                        <option value="0">No</option>
                    </select>
                </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success">Modificar</button>
                </div>
            </div>
        </div>
        {{Form::Close()}}

    </div>
    </div>
</div>
