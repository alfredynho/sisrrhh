@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Estudios</h5>
                    </div>
                    <ul class="breadcrumb">
                        @include('dashboard.includes.navadmin')
                        <li class="breadcrumb-item"><a href="#!">Estudios</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card user-profile-list">
					<div class="card-body">
						<div class="dt-responsive table-responsive">

                            <div class="row align-items-center m-l-0">
                                <div class="col-sm-6">
    
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button href="#" class="btn btn-success btn-sm btn-round has-ripple" data-toggle="modal" data-target="#modalCreate"><i class="feather icon-plus"></i> Nuevo Registro</button>
                                </div>
                            </div>

							<table class="table nowrap dt">
								<thead>
									<tr class="text-center">
                                        <th>NRO</th>
                                        <th>INSTITUCION</th>
                                        <th>PROFESION</th>
                                        <th>FECHA CREACION</th>
                                        <th>ACTIVO?</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach ($estudios as $cont=>$soc)
                                        <tr class="text-center">
                                            <td>{{ $cont+1 }}</td>
                                            <td>
                                                <p class="m-b-0"><strong>{{ $soc->institucion }}</strong></p>
                                            </td>
                                            <td>
                                                {{ Str::limit($soc->nombre_profesion,70,'...') }}
                                            </td>
                                            <td>{{ $soc->created_at }}</td>
                                            <td>
                                                <span class="badge badge-light-success">{{ $soc->grado }}</span>

                                                <div class="overlay-edit">
                                                    <a href="" data-target="#mwodalUpdate-{{ $soc->id }}" data-toggle="modal" class="btn btn-icon btn-success"><i class="feather icon-check-circle"></i></a>
                                                    <a href="" data-target="#msodalDelete-{{ $soc->id }}" data-toggle="modal" class="btn btn-icon btn-danger"><i class="feather icon-trash-2"></i></a>
                                                </div>
                                            </td>
                                        </tr>

                                        {{-- @include('dashboard.pages.estudios.edit')

                                        @include('dashboard.pages.estudios.delete') --}}

                                    @endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
                @include('dashboard.pages.estudios.create')
		</div>
@endsection

@section('extrajs')

    <script src="{{ asset('admin/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('admin/js/tinymce/langs/es_ES.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2.min.js') }}"></script>

    <script>
    $(document).ready(function() {
        $('.select2').select2();

        tinymce.init({
            selector: '.editor',
            language:'es_ES',
            plugins: "table",
            toolbar: "table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol!",
            max_height: 500,
            min_height: 350
        });
    });
    </script>

@endsection('extrajs')
