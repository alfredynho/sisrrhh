<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="modalCreate">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Nuevo Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="card-body" id="app">

            {!! Form::open(['route' => 'dashboard.estudios.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                <label for="institucion">Institución</label>
                <div class="input-group mb-2 mr-sm-2">
                    <input type="text" name="institucion" id="institucion" maxlength="150" class="form-control" value="{{old('institucion')}}">
                </div>

                <label for="mencion">Mención</label>
                <div class="input-group mb-2 mr-sm-2">
                    <input type="text" name="mencion" id="mencion" maxlength="150" class="form-control" value="{{old('mencion')}}">
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="grado">Grado</label>
                        <select id="grado" name="grado" class="form-control" required>
                            <option value="Bachillerato">Bachillerato</option>
                            <option value="Tecnico Medio">Tecnico Medio</option>
                            <option value="Tecnico Superior">Tecnico Superior</option>
                            <option value="Licenciatura">Licenciatura</option>
                            <option value="Magister">Magister</option>
                            <option value="Doctorado">Doctorado</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="profesion">Profesión</label>
                        <select id="profesion" name="profesion" class="form-control" required>
                            @foreach ($profesion as $pro)
                                <option value="{{ $pro->id }}">{{ $pro->nombre }}</option>                                            
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="id_personal">Personal</label>

                        <select class="form-control" name="id_personal">
                            @foreach ($personal as $persona)
                                <option value="{{ $persona->id }}">{{ $persona->nombres }} {{ $persona->paterno }} {{ $persona->materno }}</option>                                            
                            @endforeach
                        </select>

                    </div>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
            {{Form::Close()}}
        </div>

    </div>
    </div>
</div>
