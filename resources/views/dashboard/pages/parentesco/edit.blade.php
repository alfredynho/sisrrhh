<div class="modal fade" id="modalUpdate-{{ $soc->id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="modal-title" id="exampleModalLabel">
                <div class="mb-30">
                    <h4 class="alert-heading"><strong>Modificar Registro</strong></h4>
                </div>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        {!! Form::model($soc,['route'=>['dashboard.social.update',$soc->id],'method'=>'PUT','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

        <div class="card-body">
            <label for="name">Nombre Red Social</label>
            <div class="input-group mb-2 mr-sm-2">
                <input type="text" name="name" id="name" maxlength="150" class="form-control" value="{{ $soc->name }}" >
            </div>

            <div class="form-row">

                <div class="form-group col-md-6">
                    <label for="status">Estado</label>
                    <select id="status" name="status" class="form-control" required>
                        <option value="1" {{ ($soc->status=='1') ? 'selected' : '' }}>Activo</option>
                        <option value="0" {{ ($soc->status=='0') ? 'selected' : '' }}>Inactivo</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="url">url y/o contenido</label>
                <textarea class="form-control" id="url" name="url" rows="3"  required> {{ $soc->url }}</textarea>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-success">Modificar</button>
            </div>
        </div>

        {{Form::Close()}}

    </div>
    </div>
</div>
