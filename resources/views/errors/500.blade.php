@extends('layouts.frontend')

@section('title', __('Página no encontrada 500'))

@section('extracss')

@endsection

@section('content')

<section class="error-area">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="container">
                <div class="error-content">
                    <img src="{{ asset('frontend/images/SAPRH-black.png') }}" width="590" height="220" alt="ERROR PAGINA NO ENCONTRADA">
                    <h3>Sistema de Administración de Personal Recursos Humanos - Pagina no encontrada</h3><br><br>  
                    <a href="{{ route('landing') }}" class="btn btn-primary">Ir a Inicio <i class="flaticon-right-chevron"></i></a>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
