<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>INICIAR SESION | RRHH</title>
      
        @include('dashboard.includes.statics.css')
    </head>
  <body class=" ">

    @include('dashboard.includes.loader')

      <div class="wrapper">
      <section class="login-content">
         <div class="container h-100">
            <div class="row align-items-center justify-content-center h-100">
               <div class="col-md-5">
                  <div class="card">
                     <div class="card-body">
                        <div class="auth-logo">
                           <img src="{{ asset('frontend/images/inso.png') }}" class="img-fluid rounded-normal" alt="logo">
                        </div>
                        <p class="text-center">
                            Para mantenerse conectado con nosotros, inicie sesión con su información personal.
                        </p>
                        <form class="needs-validation" method="POST" action="{{ route('login') }}" autocomplete="off">
                           @csrf
                           <div class="row">
                              <div class="col-lg-12">
                                 <div class="form-group">
                                    <label>Correo Electronico</label>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" required autofocus name="email" autocomplete="off">
                                 </div>

                                @error('email')
                                     <span class="pcoded-badge badge badge-warning">{{ $message }}</span>
                                @enderror('email')
                              </div>

                              <div class="col-lg-12">
                                 <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" required autocomplete="current-password" autocomplete="off">
                                 </div>

                                @error('password')
                                    <span class="pcoded-badge badge badge-warning">{{ $message }}</span>
                                @enderror('password')

                              </div>
                           </div>
                           <div class="d-flex justify-content-between align-items-center">  
                              <button type="submit" class="btn btn-primary">{{ __('Iniciar Sesion') }}</button>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      </div>
    
      @include('dashboard.includes.statics.js')
 </body>
</html>