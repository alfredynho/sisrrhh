@extends('layouts.frontend')

@section('title', __('CEMTIC EMPRESA'))

@section('extracss')
    <link rel="stylesheet" href="{{ asset('frontend/pushbar.css') }}">

    <style>
    .pushbar {
        background:#FFFFFF;
        padding: 20px;
    }

    .pushbar .btn-close button{
        background: none;
        color:#333333;
        border:none;
        cursor:pointer;
        font-size:20px;
    }

    .pushbar-notification .contenedor{
        padding-bottom: 0;
    }

    .news-box .news-content .btn-cemtic,
    .news-content .btn-cemtic,
    .welcome-area2 .btn-cemtic,
    .pushbar-notification .contenedor .btn-cemtic
    {
        color:white;
        border-radius:15px;
        background: #007400;
    }
    </style>
@endsection

@section('content')

    @include('frontend.includes.header-title')


    <section class="shop-area ptb-100">
        <div class="container">

            <div class="row">
                @if(count($paquetes) >= 1)
                    @foreach ($paquetes as $paquete)
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="single-product-box">
                                <div class="product-image">
                                    <a href="{{ route('detail_paquete', [$paquete->slug]) }}">
                                        <img src="{{ asset('cemtic/img/institucional/cemtic_compartir_redes_sociales_2.webp')}}" alt="Paquetes cemtic" title="Paquetes cemtic">
                                        <img src="{{ asset('cemtic/img/institucional/cemtic_compartir_redes_sociales_2.webp')}}" alt="Paquetes cemtic" title="Paquetes cemtic">
                                    </a>

                                    <a href="{{ route('detail_paquete', [$paquete->slug]) }}" class="add-to-cart-btn">Ver paquete <i class="flaticon-shopping-bag"></i></a>
                                    @if($paquete->promotion == 1)
                                        <div class="sale-btn">Promo</div>
                                    @endif
                                </div>

                                <div class="product-content">
                                    <h3><a href="{{ route('detail_paquete', [$paquete->slug]) }}">{{ $paquete->title }}</a></h3>
                                </div>
                            </div>
                        </div>

                    @endforeach
                @else
                    <div class="alert alert-primary">
                        <strong> <i class="fa fa-exclamation-triangle"></i> Paquetes - </strong> sin contenido disponible nos encontramos trabajando el mejor contenido para ti
                    </div>
                @endif
                <div class="col-lg-12 col-md-12">
                    <div class="pagination-area">
                        {{ $paquetes->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if (Session::has('push-cemtic-paquete'))
        <!-- Pushboar -->
          <div data-pushbar-id="pushbar-notification" class="pushbar from_bottom pushbar-notification">
            <div class="btn-close">
               	<button data-pushbar-close><i class="fa fa-times-circle"></i></button>
            </div>
                <div class="contenedor">
                    <div class="main-title">
                        <div class="title-main-page">
                        <div class="container text-center">
                            <h3>{{ Session::get('push-cemtic-paquete') }} - tu mensaje fue enviado Exitosamente <strong class="font-weight-extra-bold"> <br> CEMTIC EMPRESA</strong></h3><br>
                                <button style="border-radius:15px;" type="button" class="btn btn-success btn-cemtic btn-lg" data-pushbar-close>Cerrar <span><i class="fa fa-stethoscope"></i></span></butto>
                            </div>
                        </div>
                    </div>
                    <br>
                    </div>
                </div>
          </div>
        <!-- end pushbar -->
    @endif

@endsection


@section('extrajs')
    <script src="{{ asset('frontend/pushbar.js') }}"></script>

    <script type="text/javascript">
        var pushbar = new Pushbar({
            blur:true,
            overlay:true
        });

        var interval = setTimeout(function(){
            pushbar.open('pushbar-notification');
            },100);
    </script>
@endsection
