@extends('layouts.frontend')

@section('title', __('CEMTIC, Servicios'))

@section('extracss')
@endsection

@section('content')

    @include('frontend.includes.header-title')

    <section id="shop-page" class="pt-120 pb-120 gray-bg">
        <div class="container">

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="shop-grid" role="tabpanel" aria-labelledby="shop-grid-tab">
                    <div class="row justify-content-center">

                        @foreach ($servicios as $sr)                            
                            <div class="col-lg-3 col-md-6 col-sm-8">
                                <div class="single-publication mt-30 text-center">
                                    <div class="image">
                                        <img src="{{ asset('frontend/images/publicaciones_cemtic.png') }}" alt="Publication">
                                        <div class="add-cart">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-eye"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="content pt-20">
                                        <h5 class="book-title"><a href="#">{{ $sr->title }} </a></h5>
                                        <p class="writer-name"><span></span>CEMTIC</p>
                                        <div class="price-btn d-flex align-items-center justify-content-between">
                                            <div class="price pt-20">
                                                <span>Disponible</span>
                                            </div>
                                            <div class="button pt-10">
                                                <a href="#" class="main-btn"><i class="fa fa-cart-plus"></i>Ver mas</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <nav class="courses-pagination mt-50">
                        <ul class="pagination justify-content-center">
                            <li class="page-item">
                                <a href="#" aria-label="Previous">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </li>
                            <li class="page-item"><a class="active" href="#">1</a></li>
                            <li class="page-item"><a href="#">2</a></li>
                            <li class="page-item"><a href="#">3</a></li>
                            <li class="page-item">
                                <a href="#" aria-label="Next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('extrajs')
@endsection
