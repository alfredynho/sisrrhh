@extends('layouts.frontend')

@section('title', __('CEMTIC EMPRESA'))

@section('extrameta')
    <meta property="og:url" content="{{ url()->full() }}"/>
    <meta property="og:type"               content="CEMTIC EMPRESA" />
    <meta property="og:title"              content="{{ $dt_profesional->name }}" />
    <meta property="og:description"        content="{{ getSocial('CEMTIC') }}" />
    <meta property="og:image"              content="https://res.cloudinary.com/due8e2c3a/image/upload/v1585776266/CEMTIC/cursos_cemtic.png" />
@endsection

@section('extracss')

@endsection

@section('content')

    @include('frontend.includes.header-title')

    <section id="teachers-single" class="pt-70 pb-120 gray-bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-8">
                    <div class="teachers-left mt-50">
                        <div class="hero">
                            @if($dt_profesional->image)
                                <img src="{{ $dt_profesional->image }}" alt="{{ $dt_profesional->name }}" title="{{ $dt_profesional->name }}">
                            @else
                                <img src="{{ asset('frontend/images/docentes.png') }}" alt="DOCENTE CEMTIC">
                            @endif
                        </div>
                        <div class="name">
                            <h6>{{ $dt_profesional->name }}</h6>
                            <span>{{ $dt_profesional->grado }}</span>
                        </div>
                        
                        <div class="social">
                            <p> <strong> Compartir :</strong> </p>
                            <ul>
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->full() }}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="https://twitter.com/intent/tweet?text=CemticDocentes&url={{ url()->full() }}&hashtags=#Cemtic"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://api.whatsapp.com/send?text=Cemtic%20Docentes%20{{ url()->full() }}" target="_blank"><i class="fa fa-whatsapp"></i></a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="teachers-right mt-50">
                        <ul class="nav nav-justified" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="active" id="dashboard-tab" data-toggle="tab" href="#dashboard" role="tab" aria-controls="dashboard" aria-selected="true">Biografia</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="dashboard" role="tabpanel" aria-labelledby="dashboard-tab">
                                <div class="dashboard-cont">
                                    <div class="single-dashboard pt-40">
                                        <h5>{{ $dt_profesional->name }}</h5>
                                        <p class="text-justify">{!! $dt_profesional->biography !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('extrajs')
             
@endsection
