@extends('layouts.frontend')

@section('title', __('CEMTIC EMPRESA - Blog'))

@section('extrameta')
    <meta property="og:url" content="https://res.cloudinary.com/due8e2c3a/image/upload/v1585776266/CEMTIC/cursos_cemtic.png"/>
    <meta property="og:type"               content="CEMTIC EMPRESA" />
    <meta property="og:title"              content="{{ $convenio->title }}" />
    <meta property="og:description"        content="{{ $convenio->description }}" />
    <meta property="og:image"              content="{{ url()->full() }}" />
@endsection('extrameta')

@section('extracss')

@endsection

@section('content')

    @include('frontend.includes.header-title')

    <section class="blog-details-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="blog-details-desc">
                        <div class="article-image">
                            @if($convenio->image)
                                <img style="display: block; margin-left: auto; margin-right: auto;" src="{{ Storage::url('Convenios/'.$convenio->image) }}" alt="{{ $convenio->title}}" title="{{ $convenio->title }}" width="350" height="350">
                            @else
                                <img style="display: block; margin-left: auto; margin-right: auto;" src="{{ asset('cemtic/img/convenio_cemtic.png') }}" alt="{{ $convenio->title}}" title="{{ $convenio->title }}" width="350" height="350">
                            @endif

                        </div>

                        <div class="article-content">
                            <div class="entry-meta">
                                <ul>
                                    <li><span>{{ $convenio->created_at->format('d') }}  {{ mesLiteral($convenio->created_at->format('m')) }}  {{ $convenio->created_at->format('Y')}}</span></li>
                                    <li><span class="badge badge-warning">{{ $convenio->title }}</span></li>
                                </ul>
                            </div>

                            <h3>{{ $convenio->title }}</h3>

                            <p class="text-justify">{!! $convenio->description !!}</p>


                            <blockquote class="wp-block-quote">
                                <p class="text-justify">{{ getSocial('CEMTIC')}}</p>

                                <cite>CEMTIC EMPRESA</cite>
                            </blockquote>

                        </div>

                        <div class="article-footer">
                            <div class="article-tags">
                                <span><i class="fas fa-bookmark"></i></span>
                            </div>

                            <div class="article-share">
                                <ul class="social">
                                    <li><span>Compartir:</span></li>
                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->full() }}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="https://twitter.com/intent/tweet?text=CemticProfesionales&url={{ url()->full() }}&hashtags=#CEMTIC"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="https://api.whatsapp.com/send?text=CEMTIC%20Profesionales%20{{ url()->full() }}" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="comments-area">
                            <h3 class="comments-title">Comentarios:</h3>
                            <div id="disqus_thread"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-12">
                    <aside class="widget-area" id="secondary">

                        <section class="widget widget_fovia_posts_thumb">
                            <h3 class="widget-title">Publicaciones Recientes</h3>

                            @foreach ($convenios as $cv)
                                <article class="item">
                                    <a href="{{ route('detalle_convenio',[$cv->slug]) }}" class="thumb">
                                        <span class="fullimage cover bg1" role="img"></span>
                                    </a>
                                    <div class="info">
                                        <time datetime="2019-06-30">{{ $cv->created_at->format('d') }}  {{ mesLiteral($cv->created_at->format('m')) }}  {{ $cv->created_at->format('Y')}}</time>
                                        <h4 class="title usmall"><a href="{{ route('detalle_convenio',[$cv->slug]) }}">{{ $cv->title }}</a></h4>
                                    </div>

                                    <div class="clear"></div>
                                </article>
                            @endforeach

                        </section>

                        <section class="widget widget_archive">
                            <h3 class="widget-title">Facebook</h3>
                            <div class="fb-page" data-href="https://www.facebook.com/cemticempresa/" data-tabs="timeline" data-width="300" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/cemticempresa/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/cemticempresa/">CEMTIC</a></blockquote></div>
                        </section>

                        <section class="widget widget_meta">
                            <h3 class="widget-title">Categorias</h3>
                            <ul>
                                <li><a href="{{ route('anuncios')}}">ANUNCIOS</a></li>
                                <li><a href="{{ route('blogs')}}">BLOGS</a></li>
                            </ul>
                        </section>

                    </aside>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('extrajs')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.3&appId=383264375339567&autoLogAppEvents=1"></script>
    <script>

        (function() { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        s.src = 'https://cemtic-empresa-1.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
        })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                    @endsection
