@extends('layouts.frontend')

@section('title', __('INSO Salud Ocupacional'))

@section('extracss')
    <style>
        .custom-pagination-brand-blue>.pagination>li.active>.page-link{
            background: #255125;
            border-color: #255125;
            color: #FFFFFF;
        }

        .page-link {
            background: #255125;
            border-color: #255125;
            color: #FFFFFF;
        }
    </style>
@endsection

@section('content')

    @include('frontend.includes.header-title')
    <br><br>
    <h3 style="text-align: center;">Rendición de Cuentas INSO</h3>

    <section class="cart-area ptb-100">

        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <form>
                        <div class="cart-table table-responsive">
                            <table class="table table-bordered">

                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Opciones</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(count($rendiciones) >= 1)
                                        @foreach ($rendiciones as $rendi)
                                            <tr>
                                                <td class="product-thumbnail" align="center">
                                                    <a href="{{ route('descarga-documentos',[$rendi->file]) }}">
                                                        <img src="{{ asset('inso/img/institucional/rendicion-inso.webp ')}}" alt="{{ $rendi->title }}" title="{{ $rendi->title }}">
                                                    </a>
                                                </td>

                                                <td class="product-name">
                                                    <a href="{{ route('descarga-documentos',[$rendi->file]) }}">{{ $rendi->title }}</a>
                                                </td>

                                                <td class="product-quantity">
                                                    <a href="{{ route('descarga-documentos',[$rendi->file]) }}" class="remove"><i class="fas fa-download"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <div class="alert alert-primary">
                                            <strong> <i class="fa fa-exclamation-triangle"></i> Rendición - </strong> sin contenido disponible.
                                        </div>
                                    @endif
                                </tbody>
                            </table>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('extrajs')
@endsection
