@extends('layouts.frontend')

@section('title', __('CEMTIC CLIENTES'))

@section('extracss')
@endsection

@section('content')

    @include('frontend.includes.header-title')

    <section id="event-page" class="pt-90 pb-120 gray-bg">
        <div class="container">
           <div class="row">
                @foreach ($clients as $cli)
                    <div class="col-lg-4">
                        <div class="single-event-list-2 mt-30">
                            <div class="event-thum">
                                @if($cli->image)
                                    <img src="{{ $cli->image }}" alt="CEMTIC CLIENTE">
                                @else
                                    <img src="{{ asset('frontend/images/clientes_cemtic.png') }}" alt="CEMTIC CLIENTE">
                                @endif
                            </div>
                            <div class="event-cont">
                                    <a href="{{ route('detail_client',[$cli->slug]) }}"><h4>{{ $cli->name }}</h4></a>
                            </div>
                        </div>
                    </div>
               @endforeach
           </div>

        </div>
    </section>

@endsection


@section('extrajs')
@endsection
