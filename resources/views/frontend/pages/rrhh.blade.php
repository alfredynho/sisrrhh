@extends('layouts.frontend')

@section('title', __('CEMTIC, Servicios'))

@section('extracss')
@endsection

@section('content')

    @include('frontend.includes.header-title')

    <!-- START SECTION ABOUT -->
    <div class="section small_pb">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                    <div class="about_img"> <img src="assets/images/about_img2.jpg" alt="about_img2" /> </div>
                  </div>
                  <div class="col-lg-6 animation" data-animation="fadeInUp" data-animation-delay="0.3s">
                    <div class="about_section pl-lg-3">
                          <div class="heading_s1"> 
                            <span class="sub_heading">RRHH</span>
                            <h2>Recursos Humanos</h2>
                          </div>
                          <p class="text-justify">La administración de recursos humanos es sumamente importante en una empresa u organización porque administra el recurso humano, por lo tanto el recurso menos predecible y dinámico.

                            Una buena gestión de los recursos humanos genera, como un proceso en cadena, los siguientes beneficios y ventajas:
                            
                            mejora y aprovecha las capacidades y habilidades de los trabajadores
                            aumenta el rendimiento, la calidad y la producción tanto del trabajador como de la empresa.
                            la buena relación interpersonal entre los trabajadores crea motivación y buen clima.
                            la buena relación interpersonal entre los trabajadores y RRHH hace que todos se sientan escuchados y valorados​
                            la renovación de los puestos de trabajo o la creación de nuevos puestos de trabajos son implementados de forma armoniosa para todos.
                            los puestos de trabajos son ocupados por personas competentes para ése puesto de trabajo y compatible con el equipo de trabajo.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION ABOUT --> 
    
    <!-- START SECTION WHY CHOOSE --> 
    <div class="section small_pt pb_70">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                    <div class="icon_box icon_box_style1 box_shadow1 text-center">
                        <div class="icon ibc_orange">
                            <i class="fas fa-user-graduate"></i>
                        </div>
                        <div class="icon_box_content">
                            <h5>Admission Center</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.3s">
                    <div class="icon_box icon_box_style1 box_shadow1 text-center">
                        <div class="icon ibc_green">
                            <i class="fas fa-globe"></i>
                        </div>
                        <div class="icon_box_content">
                            <h5>Online Training</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                    <div class="icon_box icon_box_style1 box_shadow1 text-center">
                        <div class="icon ibc_pink">
                            <i class="fas fa-graduation-cap"></i>
                        </div>
                        <div class="icon_box_content">
                            <h5>Certification</h5>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION WHY CHOOSE -->
    
    <!-- START SECTION COUNTER -->
    <div class="section background_bg counter_wrap bg_blue fixed_bg" data-img-src="assets/images/pattern_bg1.png">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-6 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                    <div class="box_counter counter_white text-center"> <i class="flaticon-book"></i>
                        <h3 class="counter_text"><span class="counter" data-from="0" data-to="280" data-speed="1500" data-refresh-interval="5"></span>+</h3>
                        <p>Courses</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-6 animation" data-animation="fadeInUp" data-animation-delay="0.3s">
                    <div class="box_counter counter_white text-center"> <i class="flaticon-student"></i>
                        <h3 class="counter_text"><span class="counter" data-from="0" data-to="1350" data-speed="1500" data-refresh-interval="5"></span>+</h3>
                        <p>Students</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-6 animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                    <div class="box_counter counter_white text-center"> <i class="flaticon-teacher"></i>
                        <h3 class="counter_text"><span class="counter" data-from="0" data-to="200" data-speed="1500" data-refresh-interval="5"></span>+</h3>
                        <p>Qualified Staff</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-6 animation" data-animation="fadeInUp" data-animation-delay="0.5s">
                    <div class="box_counter counter_white text-center"> <i class="flaticon-trophy"></i>
                        <h3 class="counter_text"><span class="counter" data-from="0" data-to="150" data-speed="1500" data-refresh-interval="5"></span>+</h3>
                        <p>Awards win</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION COUNTER -->
    

@endsection


@section('extrajs')
@endsection
