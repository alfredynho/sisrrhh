@extends('layouts.frontend')

@section('title', __('CEMTIC CURSOS'))

@section('extracss')
@endsection

@section('content')

    @include('frontend.includes.header_pages')

<div class="section">
	<div class="container">
        <div class="row">
            @foreach ($courses as $course)
                <div class="col-lg-4 col-md-6">
                    <div class="courses_box radius_all_10 box_shadow1 animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                        <div class="courses_img"> 
                            <a href="{{ route('detail_course',[$course->slug]) }}"><img src="{{ $course->image }}" alt="{{ $course->title }}"/></a>
                        </div>
                        <div class="courses_info">
                            <div class="rating_stars"> 
                                <i class="ion-android-star"></i> 
                                <i class="ion-android-star"></i> 
                                <i class="ion-android-star"></i> 
                                <i class="ion-android-star"></i> 
                                <i class="ion-android-star-outline"></i> 
                                <span>4.0</span> 
                            </div>
                            <h5 class="courses_title"><a href="{{ route('detail_course',[$course->slug]) }}">{{ Str::limit($course->title,30) }}</a></h5>
                            <div class="courses_teacher"> 
                                <a href="{{ route('detail_course',[$course->slug]) }}"><img src="{{ asset('frontend/images/user.png') }}" alt="user6"><span>RRHH</span></a> 
                            </div>
                            <div class="courses_footer">
                                <ul class="courses_meta">
                                    <li><a href="#" ><i class="ti-user"></i><span>31</span></a></li>
                                    <li><a href="#"><i class="ti-time"></i><span>2 hr 30 min</span></a></li>
                                </ul>
                                <div class="courses_price"><span><span class="label label-warning">Ver mas</span></span> </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="row">
        	<div class="col-12 mt-2 mt-md-3">
            	<ul class="pagination pagination_style1 justify-content-center">
                    <li class="page-item disabled"><a class="page-link" href="#" tabindex="-1"><i class="linearicons-arrow-left"></i></a></li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#"><i class="linearicons-arrow-right"></i></a></li>
              	</ul>
            </div>
        </div>
    </div>
</div> 

@endsection


@section('extrajs')
@endsection('extrajs')
