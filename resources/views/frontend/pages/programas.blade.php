@extends('layouts.frontend')

@section('title', __('Programas - CCE UPEA'))

@section('extracss')

@endsection('extracss')

@section('content')

    <section class="page-header page-header-modern bg-color-light-scale-1 page-header-md" style="background-image: url({{ asset('cce/img/banner_header.png') }});">
        <div class="container">
            <div class="row">

                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8">Pasantias</h1>
                </div>

                <div class="col-md-12 align-self-center order-1">

                    <ul class="breadcrumb d-block text-center">
                        <li><a>CCE-UPEA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-4">
        <div class="row">
            <div class="col-lg-3">
                <aside class="sidebar">
                    <h5 class="font-weight-bold pt-4">Programas</h5>
                    <p>En esta sección podra encontrar pasantias para las distintas carreras de el CCE UPEA</p>
                </aside>
            </div>
            <div class="col-lg-9">
                <div class="blog-posts">
                    @forelse($program as $pg)
                        <article class="post post-medium">
                            <div class="row mb-3">
                                <div class="col-lg-5">
                                    <div class="post-image">
                                        <a href="blog-post.html">
                                            <img src="{{ asset('cce/img/cce_logo.png')}}" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="PROGRAMAS CCE UPEA" />
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="post-content">
                                        <h2 class="font-weight-semibold text-5 line-height-4 mb-2"><a href="{{ route('detail_programa', [$pg->slug]) }}">{{ $pg->title }}</a></h2>
                                        <p class="mb-0">{!! Str::limit($pg->description,500) !!}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="post-meta">
                                        <span><i class="far fa-calendar-alt"></i> {{ $pg->created_at->format('d-m-Y') }}</span>
                                        <span><i class="far fa-user"></i> By <a href="{{ route('detail_programa', [$pg->slug]) }}">CCE UPEA</a> </span>
                                        <span class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="{{ route('detail_programa', [$pg->slug]) }}" class="btn btn-xs btn-light text-1 text-uppercase">Ver mas</a></span>
                                    </div>
                                </div>
                            </div>
                        </article>
                    @empty
                        <h2>Sin registros para mostrar</h2>
                    @endforelse

                    <ul class="pagination float-right">
                        <li class="page-item"><a class="page-link" href="#"><i class="fas fa-angle-left"></i></a></li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>
                    </ul>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('extrajs')

@endsection
