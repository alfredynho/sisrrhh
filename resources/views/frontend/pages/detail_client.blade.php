@extends('layouts.frontend')

@section('title', __('CEMTIC CURSO'))

@section('extrameta')
    <meta property="og:url" content="https://res.cloudinary.com/due8e2c3a/image/upload/v1585776266/CEMTIC/cursos_cemtic.png"/>
    <meta property="og:type"               content="CEMTIC EMPRESA" />
    <meta property="og:title"              content="{{ $dt_client->title }}" />
    <meta property="og:description"        content="{{ $dt_client->description }}" />
    <meta property="og:image"              content="https://res.cloudinary.com/due8e2c3a/image/upload/v1587243117/CEMTIC/1.png" />
@endsection('extrameta')

@section('extracss')

@endsection

@section('content')

    @include('frontend.includes.header-title')

    <section id="event-single" class="pt-120 pb-120 gray-bg">
        <div class="container">
            <div class="events-area">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="events-left">
                            <h3>{{ $dt_client->name }}</h3>
                            @if($dt_client->image)
                                <center><img src="{{ $dt_client->image }}" alt="{{ $dt_client->name }}"></center>
                            @else
                                <img src="{{ asset('frontend/images/clientes_cemtic.png') }}" alt="{{ $dt_client->name }}">
                            @endif
                            <br>
                            <p class="text-justify"> 
                                <h3>Servicios prestados por Cemtic</h3>
                                {!! $dt_client->description !!}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="events-right">
                            <div class="alert alert-info" role="alert">
                                {!! $dt_client->message !!}                                    
                            </div>    

                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </section>


@endsection


@section('extrajs')
@endsection
