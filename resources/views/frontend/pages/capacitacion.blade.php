@extends('layouts.frontend')

@section('title', __('CEMTIC EMPRESA'))

@section('extracss')
@endsection

@section('content')

    <section class="breadcrumb-area text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Curso de Capacitación</h2>
                    <ul class="list-unstyled list-inline">
                        <li class="list-inline-item"><a href="">Inicio</a></li>
                        <li class="list-inline-item"><i class="fa fa-long-arrow-right"></i>CEMTIC</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="blog-four">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-7">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="blog-box">
                                <div class="blog-image">
                                    <a href=""><img src="{{ asset('cemtic/img/capacitacion_cemtic.png')}}" alt="" class="img-fluid"></a>
                                </div>
                                <div class="blog-heading d-flex">
                                    <div class="blog-date text-center">
                                        <p>25<br> Mar</p>
                                    </div>
                                    <div class="heading-box">
                                        <h6><a href="">Curso de Capacitación en seguridad Industrial</a></h6>
                                        <ul class="list-unstyled list-inline">
                                            <li class="list-inline-item"><i class="fa fa-user-o"></i><a href="">Alfredo Callizaya</a></li>
                                            <li class="list-inline-item"><i class="fa fa-commenting-o"></i><a href="">(<span>23</span>)</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="blog-content">
                                    <p>Por el desarrollo del país, un trabajador sano y productivo.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="blog-box">
                                <div class="blog-image">
                                    <a href=""><img src="{{ asset('frontend/img/capacitacion_cemtic.png')}}" alt="" class="img-fluid"></a>
                                </div>
                                <div class="blog-heading d-flex">
                                    <div class="blog-date text-center">
                                        <p>25<br> Mar</p>
                                    </div>
                                    <div class="heading-box">
                                        <h6><a href="">Curso de Capacitación en seguridad Industrial</a></h6>
                                        <ul class="list-unstyled list-inline">
                                            <li class="list-inline-item"><i class="fa fa-user-o"></i><a href="">Alfredo Callizaya</a></li>
                                            <li class="list-inline-item"><i class="fa fa-commenting-o"></i><a href="">(<span>23</span>)</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="blog-content">
                                    <p>Por el desarrollo del país, un trabajador sano y productivo.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="blog-box">
                                <div class="blog-image">
                                    <a href=""><img src="{{ asset('cemtic/img/capacitacion_cemtic.png')}}" alt="" class="img-fluid"></a>
                                </div>
                                <div class="blog-heading d-flex">
                                    <div class="blog-date text-center">
                                        <p>25<br> Mar</p>
                                    </div>
                                    <div class="heading-box">
                                        <h6><a href="">Curso de Capacitación en seguridad Industrial</a></h6>
                                        <ul class="list-unstyled list-inline">
                                            <li class="list-inline-item"><i class="fa fa-user-o"></i><a href="">Alfredo Callizaya</a></li>
                                            <li class="list-inline-item"><i class="fa fa-commenting-o"></i><a href="">(<span>23</span>)</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="blog-content">
                                    <p>Por el desarrollo del país, un trabajador sano y productivo.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="blog-box">
                                <div class="blog-image">
                                    <a href=""><img src="{{ asset('cemtic/img/capacitacion_cemtic.png')}}" alt="" class="img-fluid"></a>
                                </div>
                                <div class="blog-heading d-flex">
                                    <div class="blog-date text-center">
                                        <p>25<br> Mar</p>
                                    </div>
                                    <div class="heading-box">
                                        <h6><a href="">Curso de Capacitación en seguridad Industrial</a></h6>
                                        <ul class="list-unstyled list-inline">
                                            <li class="list-inline-item"><i class="fa fa-user-o"></i><a href="">Alfredo Callizaya</a></li>
                                            <li class="list-inline-item"><i class="fa fa-commenting-o"></i><a href="">(<span>23</span>)</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="blog-content">
                                    <p>Por el desarrollo del país, un trabajador sano y productivo.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="pagination-box text-center">
                                <ul class="list-unstyled list-inline">
                                    <li class="list-inline-item"><a href="">1</a></li>
                                    <li class="list-inline-item"><a href="">2</a></li>
                                    <li class="active list-inline-item"><a href="">3</a></li>
                                    <li class="list-inline-item"><a href="">4</a></li>
                                    <li class="list-inline-item"><a href="">...</a></li>
                                    <li class="list-inline-item"><a href=""><i class="fa fa-angle-right"></i></a></li>
                                    <li class="list-inline-item"><a href=""><i class="fa fa-angle-double-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="widget-search">
                                <h5>Buscar Publicación</h5>
                                <form action="#">
                                    <input type="text" name="search" placeholder="buscar" required="">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <div class="widget-post">
                                <h5>Curso Pasados</h5>
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-angle-right"></i><a href="">Curso de capacitación 1</a></li>
                                    <li><i class="fa fa-angle-right"></i><a href="">Curso de capacitación 2</a></li>
                                    <li><i class="fa fa-angle-right"></i><a href="">Curso de capacitación 3</a></li>
                                    <li><i class="fa fa-angle-right"></i><a href="">Curso de capacitación 4</a></li>
                                    <li><i class="fa fa-angle-right"></i><a href="">Curso de capacitación 5</a></li>

                                </ul>
                            </div>

                            <div class="widget-tag">
                                <h5>Tags Polulares</h5>
                                <div class="tag-box">
                                    <a href="">SALUD</a>
                                    <a href="">CEMTIC</a>
                                    <a href="">TRABAJO</a>
                                    <a href="">EDUCACIÓN</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@section('extrajs')
@endsection
