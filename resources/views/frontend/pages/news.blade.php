@extends('layouts.frontend')

@section('title', __('Noticias - CCE UPEA'))

@section('extracss')

@endsection('extracss')

@section('content')

    <section class="page-header page-header-modern page-header-background page-header-background-sm overlay overlay-color-primary overlay-show overlay-op-8 mb-5" style="background-image: url({{ asset('cce/img/banner_header.png') }});">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1>Noticias Ciencias de la Educación</h1>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-light d-block text-center">
                        <li><a href="#">UPEA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div role="main" class="main">
        <section class="section border-0 m-0 pb-3">
            <div class="container container-lg">
                <div class="row pb-1">
                    @if(count($noticias) >= '1')
                        @foreach ($noticias as $bg)
                        <div class="col-sm-6 col-lg-4 mb-4 pb-2">
                            <a href="{{ route('detail_anuncio', [$bg->slug]) }}">
                                <article>
                                    <div class="thumb-info thumb-info-no-borders thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom border-radius-0">
                                        <div class="thumb-info-wrapper thumb-info-wrapper-opacity-6">
                                            <img src="{{ asset('cce/img/cce_logo.png')}}" class="img-fluid" alt="NOTICIAS CCE UPEA">
                                            <div class="thumb-info-title bg-transparent p-4">
                                                <div class="thumb-info-type bg-color-primary px-2 mb-1">{{ $bg->created_at->format('Y-m-d') }}</div>
                                                <div class="thumb-info-inner mt-1">
                                                    <h2 class="text-color-light line-height-2 text-4 font-weight-bold mb-0">
                                                        {!! Str::limit($bg->title, 60,'...') !!}
                                                    </h2>
                                                </div>
                                                <div class="thumb-info-show-more-content">
                                                    <p class="mb-0 text-1 line-height-9 mb-1 mt-2 text-light opacity-5">
                                                        {!! Str::limit($bg->description, 150, ' ...') !!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </a>
                        </div>
                        @endforeach
                    @else
                        <div class="col-lg-12 col-md-12">
                            <div class="mb-200">
                                <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Sin Registros! en Noticias </strong> Nos encontramos preparando el mejor contenido para ti <strong> CCE UPEA</strong>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </section>

        <div class="container container-lg">
            <div class="row py-5">
                <div class="col-md-6 col-lg-4">
                    <h3 class="font-weight-bold text-3 mb-0">Actividades</h3>
                    <ul class="simple-post-list">
                        @if(count($noticias) >= '1')
                            @foreach ($noticias as $act)
                                <li>
                                    <article>
                                        <div class="post-image">
                                            <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                                <a href="{{ route('detail_anuncio', [$act->slug]) }}">
                                                    <img src="{{ asset('cce/img/cce_logo.png')}}" class="border-radius-0" width="50" height="50" alt="{{ $act->title }}">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="post-info">
                                            <h4 class="font-weight-normal text-3 line-height-4 mb-0"><a href="{{ route('detail_anuncio', [$act->slug]) }}" class="text-dark">{!! Str::limit($act->title, 60,'...') !!}</a></h4>
                                            <div class="post-meta">
                                                {{ $act->created_at->format('Y-d-m') }}
                                            </div>
                                        </div>
                                    </article>
                                </li>
                            @endforeach
                        @else
                            <div class="col-lg-12 col-md-12">
                                <div class="mb-200">
                                    <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                    <strong>Sin Registros! en Noticias </strong> Nos encontramos preparando el mejor contenido para ti <strong> CCE UPEA</strong>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </ul>

                </div>
                <div class="col-md-6 col-lg-4">
                    <h3 class="font-weight-bold text-3 mb-0 mt-4 mt-md-0">Articulos </h3>
                    <ul class="simple-post-list">
                        @if(count($blog) >= '1')
                            @foreach ($blog as $art)
                            <li>
                                <div class="post-image">
                                    <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                        <a href="{{ route('detail_anuncio', [$art->slug]) }}">
                                            <img src="{{ asset('cce/img/cce_logo.png')}}" class="border-radius-0" width="50" height="50" alt="{{ $art->title }}">
                                        </a>
                                    </div>
                                </div>
                                <div class="post-info">
                                    <h4 class="font-weight-normal text-3 line-height-4 mb-0"><a href="{{ route('detail_anuncio', [$art->slug]) }}" class="text-dark">{!! Str::limit($art->title, 60,'...') !!}</a></h4>
                                    <div class="post-meta">
                                        {{ $art->created_at->format('Y-m-d') }}
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        @else
                            <div class="col-lg-12 col-md-12">
                                <div class="mb-200">
                                    <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                    <strong>Sin Registros! en Blog </strong> Nos encontramos preparando el mejor contenido para ti <strong> CCE UPEA</strong>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </ul>

                </div>
                <div class="col-lg-4">
                    <h3 class="font-weight-bold text-3 mt-4 mt-md-0">Noticias Destacadas</h3>
                    <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'margin': 10, 'loop': true, 'nav': false, 'dots': false, 'autoplay': true, 'autoplayTimeout': 5000}">
                    @if(count($noticias_destacado) >= '1')
                        @foreach ($noticias_destacado as $nt)
                            <div>
                                <a href="{{ route('detail_anuncio', [$nt->slug]) }}">
                                    <article>
                                        <div class="thumb-info thumb-info-no-borders thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom border-radius-0">
                                            <div class="thumb-info-wrapper thumb-info-wrapper-opacity-6">
                                                <img src="{{ asset('cce/img/cce_logo.png')}}" class="img-fluid" alt="CCE UPEA">
                                                <div class="thumb-info-title bg-transparent p-4">
                                                    <div class="thumb-info-type bg-color-primary px-2 mb-1">{{ $nt->created_at->format('Y-m-d') }}</div>
                                                    <div class="thumb-info-inner mt-1">
                                                        <h2 class="text-color-light line-height-2 text-4 font-weight-bold mb-0">{!! Str::limit($nt->title, 60,'...') !!}</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <div class="col-lg-12 col-md-12">
                            <div class="mb-200">
                                <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Sin Registros! en Noticias Destacadas </strong> Nos encontramos preparando el mejor contenido para ti <strong> CCE UPEA</strong>
                                </div>
                            </div>
                        </div>
                    @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('extrajs')

@endsection
