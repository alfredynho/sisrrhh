@extends('layouts.frontend')

@section('title', __('CEMTIC EMPRESA'))

@section('extracss')

@endsection

@section('content')

    @include('frontend.includes.header-title')

    <section class="product-details-area ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="product-details-image">
                        <img src="{{ asset('frontend/img/institucional/frontend_compartir_redes_sociales_2.webp') }}" alt="{{ $paquete->title}}" title="{{ $paquete->title }}">
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="product-details-desc">
                        <h3>{{ $paquete->title }}</h3>

                        <p class="text-justify">{{ getSocial('PAQUETES')}}</p>

                    </div>
                </div>

                <div class="col-lg-12 col-md-12">
                    <div class="tab products-details-tab">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <ul class="tabs">
                                    <li><a href="#">
                                        <div class="dot"></div> Descripción
                                    </a></li>

                                    <li><a href="#">
                                        <div class="dot"></div> Examenes
                                    </a></li>

                                    <li><a href="#">
                                        <div class="dot"></div> Solicitar
                                    </a></li>
                                </ul>
                            </div>

                            <div class="col-lg-12 col-md-12">
                                <div class="tab_content">
                                    <div class="tabs_item">
                                        <div class="products-details-tab-content">
                                            <p class="text-justify">{!! $paquete->description !!}</p>
                                        </div>
                                    </div>

                                    <div class="tabs_item">
                                        <div class="products-details-tab-content">
                                            <p class="text-justify">{!! $paquete->services !!}</p>
                                        </div>
                                    </div>

                                    <div class="tabs_item">
                                        <div class="products-details-tab-content">
                                            <div class="product-review-form">

                                                <div>
                                                    @foreach ($errors->all() as $error)
                                                        <div class="mb-200">
                                                            <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                                                                <strong>{{ $error }}</strong>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>

                                                
                                                <div class="review-form">
                                                    <h3>Solicitar Paquete</h3>

                                                    {!! Form::open(['route' => 'dashboard.package.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                                                        <input id="formId" name="formId" type="hidden" value="0">

                                                        <input id="id_package" name="id_package" type="hidden" value="{{ $paquete->id }}">

                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6">
                                                                <div class="form-group">
                                                                    <input type="text" id="name" name="name" placeholder="Nombre Completo" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-6 col-md-6">
                                                                <div class="form-group">
                                                                    <input type="email" id="email" name="email" placeholder="email" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" id="business" name="business" placeholder="Razon Social" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="form-group">
                                                                    <input type="number" id="phone" name="phone" placeholder="Celular/Teléfono" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="form-group">
                                                                    <textarea name="message" id="message" cols="30" rows="7" placeholder="Mensaje" class="form-control"></textarea>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12">
                                                                <button type="submit" class="btn btn-primary">Enviar</button>
                                                            </div>
                                                        </div>

                                                    {{Form::Close()}}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="related-products">
            <div class="container">
                <div class="section-title">
                    <span>CEMTIC EMPRESA</span>
                    <h2>Paquetes de Servicios</h2>
                </div>

                <div class="row">
                    @if(count($paquetes) >= 1)
                        @foreach ($paquetes as $paquete)
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="single-product-box">
                                    <div class="product-image">
                                        <a href="{{ route('detail_paquete', [$paquete->slug]) }}">
                                            <img src="{{ asset('cemtic/img/institucional/cemtic_compartir_redes_sociales_2.webp')}}" alt="{{ $paquete->title }}" title="{{ $paquete->title }}">
                                            <img src="{{ asset('cemtic/img/institucional/cemtic_compartir_redes_sociales_2.webp')}}" alt="{{ $paquete->title }}" title="{{ $paquete->title }}">
                                        </a>

                                        <a href="{{ route('detail_paquete', [$paquete->slug]) }}" class="add-to-cart-btn">Ver paquete <i class="flaticon-shopping-basket"></i></a>
                                    </div>

                                    <div class="product-content">
                                        <h3><a href="{{ route('detail_paquete', [$paquete->slug]) }}">{{ $paquete->title }}</a></h3>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    @else
                        <div class="alert alert-primary">
                            <strong> <i class="fa fa-exclamation-triangle"></i> Blog - </strong> sin contenido disponible.
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </section>

@endsection


@section('extrajs')

@endsection
