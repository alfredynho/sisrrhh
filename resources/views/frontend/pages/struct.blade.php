@extends('layouts.frontend')

@section('title', __('Estructura Organica - CCE UPEA'))

@section('extracss')

@endsection

@section('content')

    <section class="page-header page-header-modern page-header-background page-header-background-sm overlay overlay-color-primary overlay-show overlay-op-8 mb-5" style="background-image: url({{ asset('cce/img/banner_header.png') }});">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1>Estructura Organica</h1>

                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-light d-block text-center">
                        <li><a href="#">UPEA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-4">

        @if(count($academics)>= '1')
            @foreach ($academics as $aca)
            <div class="row">
                <div class="col-md-7 order-2">
                    <div class="overflow-hidden">
                        <h2 class="text-color-dark font-weight-bold text-8 mb-0 pt-0 mt-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="300">{{ $aca->name }}</h2>
                    </div>
                    <div class="overflow-hidden mb-3">
                        <p class="font-weight-bold text-primary text-uppercase mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="500">{{ $aca->cargo }}</p>
                    </div>
                    <p class="lead appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="700">{!! $aca->biography !!}</p>
                    <hr class="solid my-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="900">
                </div>
                <div class="col-md-5 order-md-2 mb-4 mb-lg-0 appear-animation" data-appear-animation="fadeInRightShorter">
                    <div class="thumb-info-wrapper">
                        @if($aca->image)
                            <img src="{{ Storage::url('Academic/'.$aca->image) }}" alt="{{ $aca->name }}" class="img-fluid" height="150" width="200">
                        @else
                            <img src="{{ asset('cce/img/color_cce.png')}}" class="img-fluid" alt="AUTORIDADES CCE UPEA" class="img-fluid" height="150" width="200">
                        @endif
                    </div>
                </div>

            </div>
            <br><br>
            @endforeach
        @else
            <div class="col-lg-12 col-md-12">
                <div class="mb-200">
                    <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                    <strong>Sin Registros! en Estructura Academica </strong> Nos encontramos preparando el mejor contenido para ti <strong> CCE UPEA</strong>
                    </div>
                </div>
            </div>
        @endif

    </div>

@endsection


@section('extrajs')

@endsection
