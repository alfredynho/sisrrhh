@extends('layouts.frontend')

@section('title', __('Biblioteca - CCE UPEA'))

@section('extracss')

@endsection('extracss')

@section('content')

    <section class="page-header page-header-modern bg-color-light-scale-1 page-header-md" style="background-image: url({{ asset('cce/img/banner_header.png') }});">
        <div class="container">
            <div class="row">

                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1 class="text-dark font-weight-bold text-8">Pasantias</h1>
                </div>

                <div class="col-md-12 align-self-center order-1">

                    <ul class="breadcrumb d-block text-center">
                        <li><a>CCE-UPEA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


@endsection

@section('extrajs')

@endsection
