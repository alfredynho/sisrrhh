@extends('layouts.frontend')

@section('title', __('CEMTIC - Blog'))

@section('extrameta')
    <meta property="og:url" content="t.ly/EGKBj"/>
    <meta property="og:type"               content="CEMTIC EMPRESA" />
    <meta property="og:title"              content="{{ $post->title }}" />
    <meta property="og:description"        content="{{ $post->description }}" />
    <meta property="og:image"              content="{{ url()->full() }}" />
@endsection('extrameta')

@section('extracss')

@endsection

@section('content')

    @include('frontend.includes.header-title')

    <section id="blog-single" class="pt-90 pb-120 gray-bg">
        <div class="container">
           <div class="row">
              <div class="col-lg-8">
                  <div class="blog-details mt-30">
                      <div class="thum">
                        <img src="{{ asset('frontend/images/post_cemtic.png')}}" alt="Blog detalle CEMTIC">
                      </div>
                      <div class="cont">
                        <h3>{{ $post->title }}</h3><br><br>
                            <p class="text-justify"> {!! $post->description !!}</p>
                           <ul class="share">
                               <li class="title">Compartir :</li>
                               <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->full() }}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
                               <li><a href="https://twitter.com/intent/tweet?text=CemticBlog&url={{ url()->full() }}&hashtags=#Cemtic"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank"><i class="fa fa-twitter"></i></a></li>
                               <li><a href="https://api.whatsapp.com/send?text=CEMTIC%20BLOG%20{{ url()->full() }}" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                           </ul>
                      </div>
                  </div>
              </div>
               <div class="col-lg-4">
                   <div class="sidebar">
                       <div class="row">
                           <div class="col-lg-12 col-md-6">
                               <div class="sidebar-post mt-30">
                                   <h4>Publicaciones Recientes</h4>
                                   <ul>
                                       @if(count($blogs) >= 1)
                                            @foreach ($blogs as $bgs)                                           
                                                <li>
                                                    <a href="{{ route('detail_new',[$bgs->slug]) }}">
                                                        <div class="single-post">
                                                        <div class="thum">
                                                            <img src="{{ asset('frontend/images/detalle_blog.png')}}" alt="Blog CEMTIC">
                                                        </div>
                                                        <div class="cont">
                                                            <h6>{{ $bgs->title }}</h6>
                                                            <span>{{ $bgs->created_at->format('d-m-Y') }}</span>
                                                        </div>
                                                    </div> 
                                                    </a>
                                                </li>
                                            @endforeach
                                        @else
                                            <div class="alert alert-primary">
                                                <strong> <i class="fa fa-exclamation-triangle"></i> Blogs - </strong> sin contenido disponible, estamos creando los mejores cursos para ti - CEMTIC.
                                            </div>                
                                        @endif
                                   </ul>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
        </div>
    </section>
@endsection
