@extends('layouts.frontend')

@section('title', __('Anuncios - CCE UPEA'))

@section('extrameta')

    <meta property="og:url" content="http://www.nytimes.com/2015/02/19/arts/international/when-great-minds-dont-think-alike.html" />
    <meta property="og:type"               content="CCE UPEA" />
    <meta property="og:title"              content="{{ $post->title }}" />
    <meta property="og:description"        content="{{ $post->description }}" />
    <meta property="og:image"              content="https://www.myjess.xyz/upea/img/blog.png" />
@endsection('extrameta')

@section('extracss')

@endsection('extracss')

@section('content')

    <section class="page-header page-header-modern page-header-background page-header-background-sm overlay overlay-color-primary overlay-show overlay-op-8 mb-5" style="background-image: url({{ asset('cce/img/banner_header.png') }});">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1>Anuncio</h1>

                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-light d-block text-center">
                        <li><a href="#">UPEA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

<div class="container py-4">

    <div class="row">
        <div class="col-lg-3 order-lg-2">
            <aside class="sidebar">
                <div class="tabs tabs-dark mb-4 pb-2">
                    <ul class="nav nav-tabs">
                        <li class="nav-item active"><a class="nav-link show active text-1 font-weight-bold text-uppercase" data-toggle="tab">Facebook</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="fb-page" data-href="https://www.facebook.com/Ciencias-de-la-Educación-UPEA-319708405405055/" data-tabs="timeline" data-width="700" data-height="700" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Ciencias-de-la-Educación-UPEA-319708405405055/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Ciencias-de-la-Educación-UPEA-319708405405055/">Upea CCE</a></blockquote></div>
                    </div>
                </div>
                <h5 class="font-weight-bold pt-4 mb-2">Tags</h5>
                <div class="mb-3 pb-1">
                    <a href="#"><span class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Noticias</span></a>
                    <a href="#"><span class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Articulos</span></a>
                    <a href="#"><span class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Actividades</span></a>
                </div>
                <h5 class="font-weight-bold pt-4">Encuentranos en Facebook</h5>
                <div class="fb-page" data-href="https://www.facebook.com/Ciencias-de-la-Educación-UPEA-319708405405055/" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/OklerThemes/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/OklerThemes/">Okler Themes</a></blockquote></div>
            </aside>
        </div>
        <div class="col-lg-9 order-lg-1">
            <div class="blog-posts single-post">

                <article class="post post-large blog-single-post border-0 m-0 p-0">
                    <div align="center" class="post-image ml-0">
                        <img src="{{ asset('cce/img/slider_1.png')}}" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="ANUNCIOS CCE UPEA" />
                    </div>

                    <div class="post-date ml-0">
                        <span class="day">{{ $post->created_at->format('d') }}</span>
                        <span class="month">{{ mesLiteral($post->created_at->format('m')) }}</span>
                    </div>

                    <div class="post-content ml-0">

                        <h2 class="font-weight-bold"><a href="blog-post.html">{{ $post->title }}</a></h2>

                        <div class="post-meta">
                            <span><i class="far fa-user"></i> <a href="#">CCE UPEA</a> </span>
                        </div>

                        <p>{!! $post->description !!}</p>

                        <div class="post-block mt-5 post-share">
                            <h4 class="mb-3">Compartir Publicación</h4>
                            <ul class="social-icons">
                                <li class="social-icons-facebook"><a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->full() }}"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-icons-twitter"><a href="https://twitter.com/intent/tweet?text=CCE UPEA&url={{ url()->full() }}&hashtags=#UPEA" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"><i class="fab fa-twitter"></i></a></li>
                                <li class="social-icons-linkedin"><a href="https://api.whatsapp.com/send?text=CCE%20UPEA%20Anuncios%20{{ url()->full() }}" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                            </ul>
                        </div>

                        <div id="disqus_thread"></div>

                        </div>

                    </div>
                </article>

            </div>

        </div>
    </div>

</div>

@endsection

@section('extrajs')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.3&appId=383264375339567&autoLogAppEvents=1"></script>

    <script>

        (function() { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        s.src = 'https://ciencias-de-la-educacion-upea.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
        })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                                        
@endsection
