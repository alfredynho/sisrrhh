@extends('layouts.frontend')

@section('title', __('CEMTIC EMPRESA'))

@section('extracss')

    <link rel="stylesheet" href="{{ asset('frontend/leaflet.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/pushbar.css') }}">

    <style>
        #leaflet { height: 980px; }
        .custom .leaflet-popup-tip,
        .custom .leaflet-popup-content-wrapper {
            background:#02B095;
            color: #FFAF00;
        }
    </style>

    <style>
    .pushbar {
        background:#C0D6ED;
        padding: 20px;
    }

    .pushbar .btn-close button{
        background: none;
        color:#333333;
        border:none;
        cursor:pointer;
        font-size:20px;
    }

    .pushbar-notification .contenedor{
        padding-bottom: 0;
    }

    .news-box .news-content .btn-cemtic,
    .news-content .btn-cemtic,
    .welcome-area2 .btn-cemtic,
    .pushbar-notification .contenedor .btn-cemtic
    {
        color:white;
        border-radius:15px;
        background: #007400;
    }
    </style>

@endsection

@section('content')

    @include('frontend.includes.header-title')

    <div class="section pb_70">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="contact_wrap contact_style1">
                        <div class="contact_icon">
                            <img src="{{ asset('frontend/images/location.png') }}" width="70" heigth="70" alt="Ubicación Cemtic SRL">
                        </div>
                        <div class="contact_text">
                            <span>Dirección</span>
                            <p>{{ getSocial('DIRECCION')}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact_wrap contact_style1">
                        <div class="contact_icon">
                            <img src="{{ asset('frontend/images/phone.png') }}" width="70" heigth="70" alt="Telefonos Cemtic SRL">
                        </div>
                        <div class="contact_text">
                            <span>Teléfono</span>
                            <p>{{ getSocial('TELEFONOS')}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact_wrap contact_style1">
                        <div class="contact_icon">
                            <img src="{{ asset('frontend/images/email.png') }}" width="70" heigth="70" alt="Correo Electronico Cemtic SRL">
                        </div>
                        <div class="contact_text">
                            <span>Correo Electrónico</span>
                            <a href="mailto:{{ getSocial('EMAIL')}}">{{ getSocial('EMAIL')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--  <div class="map map-big">
        <div id="leaflet"></div>
    </div>

    <div class="section p-0">
        <div class="container-fluid p-0">
            <div class="row no-gutters">
                <div class="col-12 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                    <div class="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d193229.77301255226!2d-74.05531241936525!3d40.823236500441624!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2f613438663b5%3A0xce20073c8862af08!2sW+123rd+St%2C+New+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1533565007513" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>  --}}

    <div class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-9">
                    <div class="heading_s1 text-center">
                        <h2>Contactate con nosotros</h2>
                    </div>
                    <p class="text-center">Cemtic - Construimos una idea y la hacemos realidad</p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="field_form">
                        <form method="post" name="enq">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Nombre completo<span class="required">*</span></label>
                                    <input required="" placeholder="Nombre completo" id="first-name" class="form-control" name="name" type="text">
                                 </div>
                                <div class="form-group col-md-6">
                                    <label>Correo<span class="required">*</span></label>
                                    <input required="" placeholder="Correo" id="email" class="form-control" name="email" type="email">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Celular<span class="required">*</span></label>
                                    <input required="" placeholder="Celular" id="phone" class="form-control" name="phone" type="text">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Asunto</label>
                                    <input placeholder="Asunto" id="subject" class="form-control" name="subject" type="text">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Mensaje<span class="required">*</span></label>
                                    <textarea required="" placeholder="Mensaje" id="description" class="form-control" name="message" rows="4"></textarea>
                                </div>
                                <div class="col-md-12 text-center">
                                    <button type="submit" title="Enviar Mensaje" class="btn btn-default" id="submitButton" name="submit" value="Submit">Enviar Mensaje</button>
                                </div>
                                <div class="col-md-12">
                                    <div id="alert-msg" class="alert-msg text-center"></div>
                                </div>
                            </div>
                        </form>		
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (Session::has('push-cemtic'))
        <!-- Pushboar -->
          <div data-pushbar-id="pushbar-notification" class="pushbar from_bottom pushbar-notification">
            <div class="btn-close">
               	<button data-pushbar-close><i class="fa fa-times-circle"></i></button>
            </div>
                <div class="contenedor">
                    <div class="main-title">
                        <div class="title-main-page">
                        <div class="container text-center">
                            <h3>{{ Session::get('push-cemtic') }} - tu mensaje fue enviado Exitosamente <strong class="font-weight-extra-bold"> <br> CEMTIC - al servicio del País </strong></h3><br>
                                <button style="border-radius:15px;" type="button" class="btn btn-success btn-cemtic btn-lg" data-pushbar-close>Cerrar <span><i class="fa fa-stethoscope"></i></span></butto>
                            </div>
                        </div>
                    </div>
                    <br>
                    </div>
                </div>
          </div>
        <!-- end pushbar -->
    @endif

@endsection

@section('extrajs')

    <script src="{{ asset('frontend/leaflet.js') }}"></script>
    <script src="{{ asset('frontend/pushbar.js') }}"></script>
    <script src="https://www.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=BOllcdYjn6GgcvzO7Ut7Z14BsWj5Qz5Q"></script>

    <script type="text/javascript">

        var mapLayer = MQ.mapLayer();

        var osmLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>',
            thunLink = '<a href="http://thunderforest.com/">Thunderforest</a>';

        var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            osmAttrib = '&copy; ' + osmLink + ' Contributors',
            landUrl = 'http://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png',
            thunAttrib = '&copy; '+osmLink+' Contributors & '+thunLink;

        var osmMap = L.tileLayer(osmUrl, {attribution: osmAttrib}),
            landMap = L.tileLayer(landUrl, {attribution: thunAttrib});

        var baseLayers = {
            "OSM Mapnik": osmMap,
            "Landscape": landMap
        };

        var map = L.map('leaflet', {
            center: [-16.49483 ,-68.13514],
            zoom: 15,
            dragging: true,
            scrollWheelZoom:false
        });

        map.dragging.disable();
        map.scrollWheelZoom.disable();

        var greenIcon = L.icon({
            iconUrl: "{{ asset('cce/img/cce_logo_p.png') }}",

            iconSize:     [65, 80],
            shadowSize:   [50, 64],
            iconAnchor:   [22, 94],
            shadowAnchor: [4, 62],
            popupAnchor:  [-3, -76]
        });

        L.control.scale().addTo(map);

        var customPopup = "<b> Upea CCE <br> <img src='{{ asset('cce/img/cce_ubicacion.png') }}' style='border: #FFFFFF 2px solid;' alt='UPEA CCE' width='140px'/> <br> Av. Sucre S/N Villa Esperanza <br> (Bloque Antiguo) <br> 2do Piso</b>";

        var customOptions =
        {
            'maxWidth': '500',
            'className' : 'custom'
        }

        L.marker([-16.4991552,-68.1360078], {icon: greenIcon}).addTo(map).bindPopup(customPopup,customOptions);

        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '<a href="http://osm.org/copyright">OpenStreetMap</a>'
            }).addTo(map);

        L.control.layers({
            'Mapa': mapLayer,
            'Hibrido': MQ.hybridLayer(),
            'Satelital': MQ.satelliteLayer(),
            'Negro': MQ.darkLayer(),
            'Blanco': MQ.lightLayer()
        }).addTo(map);

        var pushbar = new Pushbar({
            blur:true,
            overlay:true
        });

        var interval = setTimeout(function(){
            pushbar.open('pushbar-notification');
            },100);
    </script>

@endsection
