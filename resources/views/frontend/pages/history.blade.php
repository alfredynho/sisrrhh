@extends('layouts.frontend')

@section('title', __('Historia - CCE UPEA'))

@section('extracss')

@endsection('extracss')

@section('content')

<div role="main" class="main">
    <section class="page-header page-header-modern page-header-background page-header-background-sm overlay overlay-color-primary overlay-show overlay-op-8 mb-5" style="background-image: url({{ asset('cce/img/banner_header.png') }});">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1>Ciencias de la Educación</h1>

                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-light d-block text-center">
                        <li><a href="#">UPEA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container pt-2 pb-4">

        <div class="row pb-4 mb-2">
            <div class="col-md-6 mb-4 mb-md-0 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">

                <div class="owl-carousel owl-theme nav-inside nav-inside-edge nav-squared nav-with-transparency nav-dark mt-3" data-plugin-options="{'items': 1, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">
                    <div>
                        <div class="img-thumbnail border-0 border-radius-0 p-0 d-block">
                            <img src="{{ asset('cce/img/slider_1.png') }}" class="img-fluid border-radius-0" alt="HISTORIA UPEA CCE">
                        </div>
                    </div>
                    <div>
                        <div class="img-thumbnail border-0 border-radius-0 p-0 d-block">
                            <img src="{{ asset('cce/img/slider_1.png') }}" class="img-fluid border-radius-0" alt="HISTORIA UPEA CCE">
                        </div>
                    </div>
                    <div>
                        <div class="img-thumbnail border-0 border-radius-0 p-0 d-block">
                            <img src="{{ asset('cce/img/slider_1.png') }}" class="img-fluid border-radius-0" alt="HISTORIA UPEA CCE">
                        </div>
                    </div>
                </div>

                <hr class="solid my-5 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="1000">

            </div>
            <div class="col-md-6">
                <div class="overflow-hidden">
                    <h2 class="text-color-dark font-weight-normal text-4 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="600">Historia <strong class="font-weight-extra-bold">Ciencias de la Educación</strong></h2>
                </div>
                <p class="appear-animation" style="text-align: justify;" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">
                    La Carrera Ciencias de la Educación funciona al amparo de la Ley 2115 del 5 de noviembre 2000 y Ley 2556 de 12 de noviembre 2003, integrante de la Universidad Pública de El Alto (UPEA) y esta a su vez dentro del Sistema Universitario Nacional (reconocido en el XI congreso de Universidades del Sistema Boliviano). Funciona en los predios de la Universidad de El Alto ubicado en Villa Esperanza S/N de la zona de Rio Seco. Respecto a la expansión universitaria cuenta con 6 sedes universitaria ubicadas en las áreas rurales del departamento de La Paz y estas son las siguientes:
                    <br> 
                    <strong>SEDE DE ACHACACHI </strong><br>
                    <strong>SEDE CHUA COCANI </strong><br>
                    <strong>SEDE BATALLAS </strong><br>
                    <strong>SEDE GUAQUI </strong><br>
                    <strong>SEDE VIACHA </strong><br>
                    <strong>SEDE CHAGUAYA </strong><br>
                </p>
            </div>
        </div>

    </div>

    <section class="section bg-color-grey section-height-3 border-0 mt-5 mb-0">
        <div class="container">
            <div class="row">
                <div class="col">

                    <div class="row align-items-center pt-4 appear-animation" data-appear-animation="fadeInLeftShorter">
                        <div class="col-md-4 mb-4 mb-md-0">
                            <img class="img-fluid scale-2 pr-5 pr-md-0 my-4" src="{{ asset('cce/img/upea_biblio.png') }}" width="350" height="350" alt="CCE UPEA" />
                        </div>
                        <div class="col-md-8 pl-md-5">
                            <h2 class="font-weight-normal text-6 mb-3"><strong class="font-weight-extra-bold">Ciencias</strong> de la Educación</h2>
                            <p class="text-4" style="text-align: justify">Actualmente se proyecta la apertura de la sede en la localidad de Caranavi para la misma se cuenta con el apoyo de los comunarios y las autoridades de la comunidad.
                                    Las Ciencias de la Educación es un conjunto de disciplinas que estudian, describen, analizan y explican los fenómenos educativos en sus múltiples aspectos. La educación es un fenómeno complejo que tiene lugar en todos los ámbitos de la sociedad diferentes disciplinas de las Ciencias Sociales y Humanas como la Sociología, Derecho, Psicología, Ciencia Política, Historia, Economía, Filosofía realizan abordajes y estudios específicos por ello es posible de hablar de una Sociología de la Educación, una Historia de la Educación, una Antropología de la Educación, una Psicología Educacional, una Política Educacional, Economía de la Educación y una Filosofía de la Educación. Todas aquellas disciplinas que explican los fenómenos educativos, que pueden integrarse para realizar estudios nutren el campo de las Ciencias de la Educación.</p>
                        </div>
                    </div>

                    <hr class="solid my-5">

                    <div class="row align-items-center pt-5 pb-3 appear-animation" data-appear-animation="fadeInRightShorter">
                        <div class="col-md-8 pr-md-5 mb-5 mb-md-0">
                            <h2 class="font-weight-normal text-6 mb-3"><strong class="font-weight-extra-bold">Objetivos </strong>Generales</h2>
                            <p class="text-4" style="text-align:justify">Formar profesionales capaces de responder a la realidad educativa en el contexto plurinacional, haciendo uso de las metodologías y recursos científico-tecnológicos para conocer, interpretar, proponer soluciones y transformar la sociedad, construyendo nuestra conciencia crítica, creativa y reflexiva en función a los cambios sociales.</p>
                        </div>
                        <div class="col-md-4 px-5 px-md-3">
                            <img class="img-fluid scale-2 my-4" src="{{ asset('cce/img/biblio.png') }}" width="380" height="380" alt="CCE UPEA" />
                        </div>
                    </div>

                    <hr class="solid my-5">

                    <div class="row align-items-center pt-5 pb-3 appear-animation" data-appear-animation="fadeInRightShorter">
                        <div class="col-md-8 pr-md-5 mb-5 mb-md-0">
                            <h2 class="font-weight-normal text-6 mb-3"><strong class="font-weight-extra-bold">Objetivos </strong>Especificos</h2>
                            <p class="text-4" style="text-align:justify">Formar profesionales fortaleciendo los valores y principios éticos, académicos y autonomistas buscando la construcción de una conciencia social constructiva y solidaria. Promover la creación de nuestras teorías y prácticas pedagógicas, que contribuyan al mejoramiento de la calidad de la educación, con la participación plena de la sociedad. Desarrollar el proceso de cambio y transformación dialéctico, para que los profesionales desarrollen la mentalidad de cambio e innovaciones permanentes en los contenidos curriculares y prácticas académicas. Formar líderes sociales comprometidos con los ideales del pueblo trabajador y los sectores sociales marginados y discriminados para lograr la libertad, la democracia participativa y la justicia social. Propiciar una formación humanística y ecológica valorando la preservación de nuestros recursos naturales y la Biodiversidad, en la perspectiva de lograr una armonía entre el hombre y la naturaleza. Desarrollar en los docentes y estudiantes de la carrera hábitos, actitudes y estrategias dirigidas a la productividad, la producción de bienes educativos y servicios de calidad.</p>
                        </div>
                        <div class="col-md-4 px-5 px-md-3">
                            <img class="img-fluid scale-2 my-4" src="{{ asset('cce/img/pasillos.png') }}" width="380" height="380" alt="CCE UPEA" />
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>

</div>

@endsection('content')


@section('extrajs')

@endsection('extrajs')
