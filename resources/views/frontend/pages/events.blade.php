@extends('layouts.frontend')

@section('title', __('CEMTIC EMPRESA'))

@section('extracss')
    <meta property="og:url" content="https://res.cloudinary.com/due8e2c3a/image/upload/v1585776266/CEMTIC/cursos_cemtic.png"/>
    <meta property="og:type"               content="CEMTIC EMPRESA" />
    <meta property="og:title"              content="{{ getSocial('CEMTIC') }}" />
    <meta property="og:description"        content="{{ getSocial('CEMTIC_DESCRIPCION') }}" />
    <meta property="og:image"              content="{{ url()->full() }}" />
@endsection

@section('content')

    @include('frontend.includes.header-title')

    <section id="event-page" class="pt-90 pb-120 gray-bg">
        <div class="container">
           <div class="row">
               <div class="col-lg-6">
                   <div class="single-event-list mt-30">
                       <div class="event-thum">
                           <img src="{{ asset('frontend/images/cemtic_eventos.png') }}" alt="CEMTIC EVENTOS">
                       </div>
                       <div class="event-cont">
                           <span><i class="fa fa-calendar"></i> 2 December 2018</span>
                            <a href="events-single.html"><h4>Tech Summit</h4></a>
                            <span><i class="fa fa-clock-o"></i> 10:00 Am - 3:00 Pm</span>
                            <span><i class="fa fa-map-marker"></i> Rc Auditorim</span>
                            <p>Nam nec tellus a odio tincidunt auctor a ornare odionon mauris itae erat conuat</p>
                       </div>
                   </div>
               </div>

           </div>

        </div>
    </section>

@endsection

@section('extrajs')

@endsection
