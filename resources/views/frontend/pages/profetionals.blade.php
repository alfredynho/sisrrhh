@extends('layouts.frontend')

@section('title', __('PERSONAL'))

@section('extrameta')
    <meta property="og:url" content="{{ url()->full() }}"/>
    <meta property="og:type"               content="PERSONAL" />
    <meta property="og:title"              content="PERSONAL" />
    <meta property="og:description"        content="PERSONAL" />
    <meta property="og:image"              content="https://res.cloudinary.com/due8e2c3a/image/upload/v1585776266/CEMTIC/cursos_cemtic.png" />
@endsection('extrameta')

@section('extracss')

@endsection('extracss')

@section('content')

    @include('frontend.includes.header-title')

    <div class="section pb_70">
        <div class="container">
            <div class="row justify-content-center">

                @foreach ($profesionales as $prof)
                    <div class="col-lg-3 col-sm-6 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                        <div class="team_box team_style2 radius_all_10">
                            <div class="team_img">
                                <img src="{{ asset('frontend/images/personal.png') }}" alt="Personal">
                                <ul class="social_icons social_white">
                                    <li><a href="#"><i class="ion-eye"></i></a></li>
                                </ul>
                            </div>
                            <div class="team_content">
                                <div class="team_title">
                                    <h5>{{ strtoupper(Str::limit($prof->name,30)) }}</h5>
                                    <span>{{ strtoupper($prof->grado) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    {{--  <section id="event-page" class="pt-90 pb-120 gray-bg">
        <div class="container">
           <div class="row">
            @foreach ($profesionales as $prof)
                <div class="col-lg-6">
                    <div class="single-event-list mt-30">
                        <div class="event-thum">
                            @if($prof->image)
                                <img src="{{ $prof->image }}" alt="{{ $prof->name }}" title="{{ $prof->name }}" >
                            @else
                                <img src="{{ asset('frontend/images/profesionales_cemtic.png') }}" alt="{{ $prof->name }}" title="{{ $prof->name }}">
                            @endif
                        </div>
                        <div class="event-cont">
                            <a href="{{ route('detail_profesional',[$prof->slug]) }}"><h4>{{ $prof->name }}</h4></a>
                            <p>{!! Str::limit($prof->biography,179) !!}</p><br>
                            <a href="{{ route('detail_profesional',[$prof->slug]) }}">
                                <button type="button" class="btn btn-primary">Ver mas</button>
                            </a>                              
                        </div>
                    </div>
                </div>
            @endforeach
           </div>
        </div>
    </section>  --}}

@endsection


@section('extrajs')

@endsection('extrajs')
