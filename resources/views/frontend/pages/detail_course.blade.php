@extends('layouts.frontend')

@section('title', __('RRHH'))

@section('extrameta')
    <meta property="og:url" content="{{ asset('frontend/images/eventos.png') }}"/>
    <meta property="og:type"               content="RRHH" />
    <meta property="og:title"              content="{{ $dt_course->title }}" />
    <meta property="og:description"        content="{{ $dt_course->description }}" />
    <meta property="og:image"              content="{{ asset('frontend/images/eventos.png') }}" />
@endsection('extrameta')

@section('extracss')

@endsection

@section('content')

    @include('frontend.includes.header_pages')

<div class="section">
	<div class="container">
    	<div class="row">
        	<div class="col-xl-9 col-lg-8">
            	<div class="single_course">
                    <div class="courses_title">
                        <h4>{{ $dt_course->title }}</h4>
                    </div>
                    <div class="countent_detail_meta">
                        <ul>
                            <li>
                                <div class="instructor">
                                    @if($dt_course->imagen_docente)
                                        <img src="{{ $dt_course->imagen_docente }}" alt="{{ $dt_course->nombre_docente }}">
                                    @else
                                        <img src="{{ asset('frontend/images/user.png') }}" alt="{{ $dt_course->nombre_docente }}">
                                    @endif
                                    <div class="instructor_info">
                                        <label>Docente</label>
                                        <a href="{{ route('detail_profesional',[$dt_course->slug_profetional]) }}">{{ $dt_course->nombre_docente }}</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="course_cat">
                                    <label>Estado</label>
                                    @if ($dt_course->is_published)
                                        Disponible
                                    @else
                                        Cerrado
                                    @endif
                                </div>
                            </li>
                            <li>
                                <div class="course_rating">
                                    <label>Puntuación</label>
                                    <div class="rating_stars">
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i>
                                        <i class="ion-android-star"></i> 
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="course_student">
                                    <label>Estudiantes</label>
                                    <span> Nro. {{ $dt_course->numparticipants }}</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="course_img">
						<a href="#">
                            <img src="{{ $dt_course->image }}" alt="CURSO RRHH">
                        </a>
                    </div>
                    <div class="course_info">
                        <div class="course_desc"> 
                        	<div class="heading_s1">
                        		<h4>Descripción:</h4>
                            </div>
                        	<p class="text-justify">{!! $dt_course->description !!}</p>
                        </div>
                        <div class="course_curriculum">
                        	<div class="heading_s1">
                        		<h4>Contenido:</h4>
                            </div>
                        	<div id="accordion" class="accordion accordion_style1">
                                <div class="card">
                                  <div class="card-header" id="heading-1-One">
                                    <h6 class="mb-0"> <a data-toggle="collapse" href="#collapse-1-One" aria-expanded="true" aria-controls="collapse-1-One">¿Que aprenderemos?</a></h6>
                                  </div>
                                  <div id="collapse-1-One" class="collapse show" aria-labelledby="heading-1-One" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>{!! $dt_course->content !!}</p>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="instructors_wrap">
                            <div class="heading_s1">
                                <h4>Sobre el Docente:</h4>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="author_wrap">
                                        <div class="author_img">
                                            @if($dt_course->imagen_docente)
                                                <img src="{{ $dt_course->imagen_docente }}" alt="{{ $dt_course->nombre_docente }}">
                                            @else
                                                <img src="{{ asset('frontend/images/user.png') }}" width="100" heigth="100" alt="{{ $dt_course->nombre_docente }}">
                                            @endif
                                        </div>
                                        <div class="author_info">
                                            <h6 class="author_name"><a href="{{ route('detail_profesional',[$dt_course->slug_profetional]) }}" class="d-inline-block"></a></h6>
                                            <span class="text_default mb-2 d-block"><strong>{{ $dt_course->nombre_docente }}</strong></span>
                                        </div>
                                    </div>
                                    <div class="autor_desc">
                                        <p>{!! $dt_course->biography_profetional !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="blog_post_footer">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-md-8 mb-3 mb-md-0">
                                    <div class="artical_tags">
                                        <a href="https://api.whatsapp.com/send?text=Cemtic%Cursos%20{{ url()->full() }}" target="_blank">Comparte el curso con tus amigos (as)</a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <ul class="social_icons text-md-right">
                                        <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->full() }}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" class="sc_facebook"><i class="ion-social-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/intent/tweet?text=RRHHCursos&url={{ url()->full() }}&hashtags=#RRHH"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" class="sc_twitter"><i class="ion-social-twitter"></i></a></li>
                                        <li><a href="https://api.whatsapp.com/send?text=RRHH%Cursos%20{{ url()->full() }}" target="_blank" class="sc_whatsapp"><i class="fab fa-whatsapp"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
            </div>
        	<div class="col-xl-3 col-lg-4 mt-3 mt-lg-0">
            	<div class="sidebar">
                	<div class="widget widget_course_description">
                        <ul>
                        	<li><i class="linearicons-user"></i><span>Estudiantes:</span> <span class="item_value">{{ $dt_course->numparticipants }}</span></li>
                        	<li><i class="linearicons-clock"></i><span>Duración:</span> <span class="item_value"> {{ $dt_course->duration }}</span></li>
                            <li><i class="linearicons-signal"></i><span>Nivel:</span> <span class="item_value">{{ $dt_course->level }}</span></li>
                            <li><i class="linearicons-transform"></i><span>Modulos</span> <span class="item_value">{{ $dt_course->modules }}</span></li>
                            <li><i class="linearicons-license2"></i><span>Certificado:</span> 
                                <span class="item_value">
                                    @if ($dt_course->certification == 1)
                                        Si
                                    @else
                                        No
                                    @endif
                                </span></li>
                            <li><i class="linearicons-bag-dollar "></i><span>Precio:</span> <span class="item_value">{{ $dt_course->price }}</span></li>
                        </ul>
                        <a href="#" class="btn btn-default btn-block" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Inscribirme</a>
                    </div>
                    <div class="widget">
                    	<h5 class="widget_title">Otros Cursos</h5>
                        <ul class="widget_recent_post">
                            @foreach ($courses as $course)
                                <li>
                                    <div class="post_footer">
                                        <div class="post_img">
                                            <a href="{{ route('detail_course',[$course->slug]) }}"><img src="{{ $course->image }}" width="70" heigth="70" alt="letest_post1"></a>
                                        </div>
                                        <div class="post_content">
                                            <h6><a href="{{ route('detail_course',[$course->slug]) }}">{{ $course->title }}</a></h6>
                                            <p class="small m-0"> {{ mesLiteral($course->created_at->format('m')) }}- {{ $course->created_at->format('Y') }}</p>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                    	</ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    @include('frontend.includes.modal_inscription')


@endsection


@section('extrajs')

@endsection('extrajs')
