@extends('layouts.frontend')

@section('title', __('Galeria Fotos - CEMTIC'))

@section('extracss')

@endsection('extracss')

@section('content')

    @include('frontend.includes.header-title')

    <section class="pt-90 pb-120 gray-bg">
        <div class="container">
            <div class="row gallery">
                <div class="col-md-4 col-xs-6 thumb">
                    <a href="{{ asset('frontend/images/galeria/phoca_thumb_l_img1.jpg') }}">
                        <figure><img class="img-fluid img-thumbnail" src="{{ asset('frontend/images/galeria/phoca_thumb_l_img1.jpg') }}" alt="Gallery Image"></figure>
                    </a>
                </div>

                <div class="col-md-4 col-xs-6 thumb">
                    <a href="{{ asset('frontend/images/galeria/phoca_thumb_l_img2.jpg') }}">
                        <figure><img class="img-fluid img-thumbnail" src="{{ asset('frontend/images/galeria/phoca_thumb_l_img2.jpg') }}" alt="Gallery Image"></figure>
                    </a>
                </div>

                <div class="col-md-4 col-xs-6 thumb">
                    <a href="{{ asset('frontend/images/galeria/phoca_thumb_l_img3.jpg') }}">
                        <figure><img class="img-fluid img-thumbnail" src="{{ asset('frontend/images/galeria/phoca_thumb_l_img3.jpg') }}" alt="Gallery Image"></figure>
                    </a>
                </div>

                <div class="col-md-4 col-xs-6 thumb">
                    <a href="{{ asset('frontend/images/galeria/phoca_thumb_l_img4.jpg') }}">
                        <figure><img class="img-fluid img-thumbnail" src="{{ asset('frontend/images/galeria/phoca_thumb_l_img4.jpg') }}" alt="Gallery Image"></figure>
                    </a>
                </div>

                <div class="col-md-4 col-xs-6 thumb">
                    <a href="{{ asset('frontend/images/galeria/phoca_thumb_l_img12.jpg') }}">
                        <figure><img class="img-fluid img-thumbnail" src="{{ asset('frontend/images/galeria/phoca_thumb_l_img12.jpg') }}" alt="Gallery Image"></figure>
                    </a>
                </div>

                <div class="col-md-4 col-xs-6 thumb">
                    <a href="{{ asset('frontend/images/galeria/phoca_thumb_l_img6.jpg') }}">
                        <figure><img class="img-fluid img-thumbnail" src="{{ asset('frontend/images/galeria/phoca_thumb_l_img6.jpg') }}" alt="Gallery Image"></figure>
                    </a>
                </div>

                <div class="col-md-4 col-xs-6 thumb">
                    <a href="{{ asset('frontend/images/galeria/phoca_thumb_l_img7.jpg') }}">
                        <figure><img class="img-fluid img-thumbnail" src="{{ asset('frontend/images/galeria/phoca_thumb_l_img7.jpg') }}" alt="Gallery Image"></figure>
                    </a>
                </div>

                <div class="col-md-4 col-xs-6 thumb">
                    <a href="{{ asset('frontend/images/galeria/phoca_thumb_l_img8.jpg') }}">
                        <figure><img class="img-fluid img-thumbnail" src="{{ asset('frontend/images/galeria/phoca_thumb_l_img8.jpg') }}" alt="Gallery Image"></figure>
                    </a>
                </div>

                <div class="col-md-4 col-xs-6 thumb">
                    <a href="{{ asset('frontend/images/galeria/phoca_thumb_l_img9.jpg') }}">
                        <figure><img class="img-fluid img-thumbnail" src="{{ asset('frontend/images/galeria/phoca_thumb_l_img9.jpg') }}" alt="Gallery Image"></figure>
                    </a>
                </div>
            </div>
        </div>
    </section>


@endsection


@section('extrajs')

@endsection('extrajs')
