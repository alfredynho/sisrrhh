@extends('layouts.frontend')

@section('title', __('CEMTIC EMPRESA'))

@section('extracss')
    <meta property="og:url" content="https://res.cloudinary.com/due8e2c3a/image/upload/v1585776266/CEMTIC/cursos_cemtic.png"/>
    <meta property="og:type"               content="CEMTIC EMPRESA" />
    <meta property="og:title"              content="{{ getSocial('CEMTIC') }}" />
    <meta property="og:description"        content="{{ getSocial('CEMTIC_DESCRIPCION') }}" />
    <meta property="og:image"              content="{{ url()->full() }}" />
@endsection

@section('content')

    @include('frontend.includes.header-title')

    <section id="event-page" class="pt-90 pb-120 gray-bg">
        <div class="container">
           <div class="row">
               @foreach ($faqs as $fq)                   
                    <div class="col-lg-4">
                        <div class="single-event-list-2 mt-30">
                            <div class="event-thum">
                                <img src="{{ $fq->image }}" alt="{{ $fq->name }}">
                            </div>
                            <div class="event-cont">
                                <a href="{{ route('questionsfaqs', [$fq->slug]) }}"><h4>{{ $fq->name }}</h4></a>
                                <p class="text-justify">{{ Str::limit($fq->description,80) }}</p>
                            </div>
                                <center><a href="{{ route('questionsfaqs', [$fq->slug]) }}" class="main-btn mt-55">Ver mas</a></center>
                        </div>
                    </div>
               @endforeach
            </div>
        </div>
    </section>

@endsection

@section('extrajs')
@endsection
