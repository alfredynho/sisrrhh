@extends('layouts.frontend')

@section('title', __('Autoridades - CCE UPEA'))

@section('extracss')

@endsection('extracss')

@section('content')

    <section class="page-header page-header-modern page-header-background page-header-background-sm overlay overlay-color-primary overlay-show overlay-op-8 mb-5" style="background-image: url({{ asset('cce/img/banner_header.png') }});">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1>Autoridades UPEA</h1>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-light d-block text-center">
                        <li><a href="#">{{ now()->year  }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-4">

        <ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center" data-sort-id="team" data-option-key="filter">
            <li class="nav-item active" data-option-value="*"><a class="nav-link text-1 text-uppercase active" href="#"></a></li>
        </ul>

        <div class="sort-destination-loader sort-destination-loader-showing mt-4 pt-2">
            <div class="row team-list sort-destination" data-sort-id="team">

                @if(count($authorities) >= 1)

                    @foreach ($authorities as $au)
                    <div class="col-12 col-sm-6 col-lg-3 isotope-item leadership">
                        <span class="thumb-info thumb-info-hide-wrapper-bg mb-4">
                            <span class="thumb-info-wrapper">
                                <a href="{{ route('detail_autoridades', [$au->slug]) }}">

                                    @if($au->image)
                                        <img src="{{ Storage::url('Authorities/'.$au->image) }}" alt="{{ $au->name }}" height="250" width="200">
                                    @else
                                        <img src="{{ asset('cce/img/cce_logo.png')}}" alt="{{ $au->name }}" height="250" width="200">
                                    @endif
                                    <span class="thumb-info-title">
                                        <span class="thumb-info-inner">{{ $au->name }}</span>
                                        <span class="thumb-info-type">{{ $au->cargo }}</span>
                                    </span>
                                </a>
                            </span>
                            <span class="thumb-info-caption">
                                <span class="thumb-info-social-icons mb-4">
                                    <a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
                                    <a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
                                </span>
                            </span>
                        </span>
                    </div>
                    @endforeach

                @else
                    <div class="alert alert-quaternary">
                        <strong> <i class="fas fa-exclamation-triangle"></i> Noticias - </strong> sin contenido disponible.
                    </div>
                @endif


            </div>

        </div>

    </div>
@endsection

@section('extrajs')

@endsection
