@extends('layouts.frontend')

@section('title', __('Faqs - CEMTIC'))

@section('extracss')

@endsection('extracss')

@section('content')
    @include('frontend.includes.header-title')

    <section class="pt-105 pb-120 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="mb-30">Categoria {{ $categoria->name }}</h2>
                </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="faq-accordion" role="tabpanel" aria-labelledby="faq-accordion-tab">
                            <div class="faq-accordion-cont">
                                @if(count($questions) >= '1')
                                    @foreach ($questions as $cont=>$qs)
                                        <div class="accordion" id="accordionExample{{ $cont+1 }}">
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <a href="#" data-toggle="collapse" data-target="#collapseOne{{ $cont+1 }}" aria-expanded="{{ ( $cont+1 == '1' ) ? 'true' : 'false' }}" aria-controls="collapseOne">
                                                        <ul>
                                                            <li><i class="fa fa-question-circle" aria-hidden="true"></i></i></li>
                                                            <li><span class="head">{{ $qs->question }}</span></li>
                                                            <li></li>
                                                        </ul>
                                                    </a>
                                                </div>

                                                <div id="collapseOne{{ $cont+1 }}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample{{ $cont+1 }}">
                                                    <div class="card-body">
                                                        <p>{{ $qs->answer }}</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    @endforeach

                                @else
                                    <div class="col-lg-12 col-md-12">
                                        <div class="mb-200">
                                            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                            <strong>Sin Registros! en preguntas de categoria </strong> Nos encontramos preparando el mejor contenido para ti <strong> CEMTIC EMPRESA</strong>
                                            </div>
                                        </div>
                                    </div>        
                                @endif

                            </div> 
                        </div>

                    </div>
              </div>
            </div>
        </div>
    </section>
@endsection

@section('extrajs')

@endsection
