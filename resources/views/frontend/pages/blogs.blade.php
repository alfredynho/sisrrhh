@extends('layouts.frontend')

@section('title', __('CEMTIC EMPRESA'))

@section('extracss')

@endsection

@section('content')

    @include('frontend.includes.header-title')

    <section id="blog-page" class="pt-90 pb-120 gray-bg">
        <div class="container">
           <div class="row">
               <div class="col-lg-8">

                @foreach ($blog as $bg)
                   <div class="single-blog mt-30">
                       <div class="blog-thum">
                           <img src="{{ asset('frontend/images/blog_cemtic.png') }}" alt="{{ $bg->title }}" alt="{{ $bg->title }}">
                       </div>
                       <div class="blog-cont">
                           <a href="{{ route('detail_new',[$bg->slug]) }}"><h3>{{ $bg->title }}</h3></a>
                           <ul>
                               <li><a href="{{ route('detail_new',[$bg->slug]) }}"><i class="fa fa-calendar"></i>{{ $bg->created_at->format('d-m-Y') }}</a></li>
                               <li><a href="{{ route('detail_new',[$bg->slug]) }}"><i class="fa fa-user"></i>CEMTIC EMPRESA</a></li>
                           </ul>
                           <p class="tex-justify">{!! Str::limit($bg->description,200) !!}</p>
                       </div>
                   </div>
                @endforeach

                   {{-- <nav class="courses-pagination mt-50">
                        <ul class="pagination justify-content-lg-end justify-content-center">
                            <li class="page-item">
                                <a href="#" aria-label="Previous">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </li>
                            <li class="page-item"><a class="active" href="#">1</a></li>
                            <li class="page-item"><a href="#">2</a></li>
                            <li class="page-item"><a href="#">3</a></li>
                            <li class="page-item">
                                <a href="#" aria-label="Next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>  --}}
               </div>
               <div class="col-lg-4">
                   <div class="sidebar">
                       <div class="row">

                           <div class="col-lg-12 col-md-6">
                               <div class="sidebar-post mt-30">
                                   <h4>Publicaciones Recientes</h4>
                                   <ul>
                                       @foreach ($blogs_list as $bgl)                                           
                                            <li>
                                                <a href="{{ route('detail_new',[$bgl->slug]) }}">
                                                    <div class="single-post">
                                                    <div class="thum">
                                                        <img src="{{ asset('frontend/images/detalle_blog.png') }}" alt="{{ $bg->title }}" alt="{{ $bgl->title }}">
                                                    </div>
                                                    <div class="cont">
                                                        <h6>{{ $bgl->title }}</h6>
                                                        <span>{{ $bgl->created_at->format('d-m-Y') }}</span>
                                                    </div>
                                                </div>
                                                </a>
                                            </li>
                                       @endforeach
                                   </ul>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
        </div>
    </section>
    
@endsection


@section('extrajs')
@endsection
