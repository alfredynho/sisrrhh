@extends('layouts.frontend')

@section('title', __('Admisión - CCE UPEA'))

@section('extracss')
    <style>
    .pushbar {
        background:#FFFFFF;
        padding: 20px;
    }

    .pushbar .btn-close button{
        background: none;
        color:#538433;
        border:none;
        cursor:pointer;
        font-size:20px;
    }

    .pushbar-notification .contenedor{
        padding-bottom: 0;
    }

    </style>

    <link rel="stylesheet" href="{{ asset('cce/css/pushbar.css') }}">

@endsection('extracss')

@section('content')

    <section class="page-header page-header-modern page-header-background page-header-background-sm overlay overlay-color-primary overlay-show overlay-op-8 mb-5" style="background-image: url({{ asset('cce/img/banner_header.png') }});">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1>Admisiones</h1>

                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-light d-block text-center">
                        <li><a href="#">UPEA</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container pb-1">

        <div class="row pt-4">
            <div class="col">
                <div class="overflow-hidden mb-3">
                    <h2 class="font-weight-bold text-8 mb-0 appear-animation" data-appear-animation="maskUp">
                        EXAMEN DE ADMISIÓN – GESTIÓN 2019
                    </h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-10">
                <div class="overflow-hidden">
                    <p class="lead mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200">
                        Carrera Ciencias de la educacion: <br><br>

                       <strong> - CONTADURÍA GENERAL <br>

                        - ESPECIALIDAD 1 <br>

                        - ESPECIALIDAD 2 <br>

                        - ESPECIALIDAD 2  <br>

                        - ESPECIALIDAD 2
                         </strong>
                    </p>
                </div>
            </div>
            <div class="col-lg-2 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="500">
                <a href="/carrera" class="btn btn-modern btn-dark mt-1">Ver Carreras</a>
            </div>
        </div>

    <section class="section bg-color-grey section-height-3 border-0 mt-5 mb-0">
        <div class="container">

            <div class="row">
                <div class="col">

                    <div class="row align-items-center pt-4 appear-animation" data-appear-animation="fadeInLeftShorter">
                        <div class="col-md-4 mb-4 mb-md-0">
                            <img class="img-fluid scale-2 pr-5 pr-md-0 my-4" src="{{ asset('cce/img/cce_logo.png') }}" alt="Detalle Admisión UPEA CCE" />
                        </div>
                        <div class="col-md-8 pl-md-5">
                            <p class="text-4">
                                Cumpliendo con el siguiente procedimiento: <br>

                                <strong>1. COSTO DE INSCRIPCIÓN  </strong><br>

                                Depósito bancario de Bs. 460.- (sesenta bolivianos) en el Banco Unión a la Cta. Nro. 1-4707771 con nombres y apellidos del postulante, escritos correctamente.

                            </p>
                            <p class="text-4" ><strong> 2. INSCRIPCIÓN </strong> <br>
                            La inscripción debe realizar en secretaría del “Ciencias de la educacion UPEA”</p>
                        </div>
                    </div>

                    <hr class="solid my-5">

                    <div class="row align-items-center pt-5 pb-3 appear-animation" data-appear-animation="fadeInRightShorter">
                        <div class="col-md-8 pr-md-5 mb-5 mb-md-0">
                            <p class="text-4">3.

                            <strong> REQUISITOS EN ORIGINAL Y FOTOCOPIA SIMPLE  </strong>

                            Fotocopia de Cédula de Identidad (vigente) <br>

                            Fotocopia de Certificado de Nacimiento <br>

                            Fotocopia de Título de Bachiller <br>

                            Depósito Bancario Original y fotocopia <br>
                            </p>

                            <p>

                            <strong> 4. PRESENTACIÓN DE DOCUMENTOS </strong> <br>

                            Toda la documentación debe ser presentada en secretaría de carrera, a partir del 7 al 18 de enero de 2019 en el siguiente horario: Hrs. 08:30 a.m. a 13:00 p.m.

                            </p>

                            <p>

                            <strong> 5 FECHA EXAMEN </strong><br>

                                5. FECHA DE EXAMEN

                                Se llevara a cabo el DÍA SÁBADO 26 DE ENERO DE 2019, a partir de la 08:00 a.m.

                                Lugar, Coliseo héroes de octubre, Av. Juan Pablo II, lado TAM (por confirmar)

                            </p>
                        </div>
                        <div class="col-md-4 px-5 px-md-3">
                            <img class="img-fluid scale-2 my-4" src="{{ asset('cce/img/cce_upea.png') }}" alt="Detalle Admisión UPEA CCE"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <div class="row featured-boxes-full featured-boxes-full-scale">
            <div class="col-lg-3 featured-box-full featured-box-full-primary">
                <i class="far fa-life-ring"></i>
                <h4>Customer Support</h4>
                <p class="font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet. Quisque rutrum pellentesque imperdiet.</p>
            </div>
            <div class="col-lg-3 featured-box-full featured-box-full-primary">
                <i class="fas fa-film"></i>
                <h4>Sliders</h4>
                <p class="font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet. Quisque rutrum pellentesque imperdiet.</p>
            </div>
            <div class="col-lg-3 featured-box-full featured-box-full-primary">
                <i class="far fa-star"></i>
                <h4>Winner</h4>
                <p class="font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet. </p>
            </div>
            <div class="col-lg-3 featured-box-full featured-box-full-primary">
                <i class="far fa-edit"></i>
                <h4>Customizable</h4>
                <p class="font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet. Quisque rutrum pellentesque imperdiet.</p>
            </div>
        </div>
    </div>


@endsection

@section('extrajs')
    <script src="{{ asset('cce/js/pushbar.js') }}"></script>

    <script type="text/javascript">
        var pushbar = new Pushbar({
            blur:true,
            overlay:true
        });

        var interval = setTimeout(function(){
            pushbar.open('pushbar-notification');
        },30000);
    </script>

@endsection
