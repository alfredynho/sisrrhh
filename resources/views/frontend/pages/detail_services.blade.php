@extends('layouts.frontend')

@section('title', __('CEMTIC SERVICIO'))

@section('extrameta')
    <meta property="og:url" content="https://res.cloudinary.com/due8e2c3a/image/upload/v1585776266/CEMTIC/cursos_cemtic.png"/>
    <meta property="og:type"               content="CEMTIC EMPRESA" />
    <meta property="og:title"              content="{{ $post->title }}" />
    <meta property="og:description"        content="{{ $post->description }}" />
    <meta property="og:image"              content="https://res.cloudinary.com/due8e2c3a/image/upload/v1587243117/CEMTIC/1.png" />
@endsection('extrameta')

@section('extracss')

@endsection

@section('content')

    @include('frontend.includes.header-title')

    <section id="shop-single" class="pt-120 pb-120 gray-bg">
        <div class="container">
            <div class="shop-details">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="shop-reviews mt-60">
                            <ul class="nav" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">{{ $post->title }}</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
                                    <div class="description-cont pt-40">
                                        <p>{!! $post->services !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="related-item pt-110">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="title pb-10">
                            <h3>Mas Servicios</h3>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">

                    @foreach ($servicios as $pq)
                        <div class="col-lg-3 col-md-6 col-sm-8">
                            <div class="single-publication mt-30 text-center">
                                <div class="image">
                                    <img src="{{ asset('frontend/images/publicaciones_cemtic.png') }}" alt="Servicio CEMTIC EMPRESA">
                                    <div class="add-cart">
                                        <ul>
                                            <li><a href="{{ route('detail_service',[$pq->slug]) }}"><i class="fa fa-eye"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="content pt-10">
                                    <h5 class="book-title"><a href="{{ route('detail_service',[$pq->slug]) }}">{{ $pq->title }} </a></h5>
                                    <p class="writer-name"><span></span> CEMTIC</p>
                                    <div class="price-btn d-flex align-items-center justify-content-between">
                                        <div class="price pt-20">
                                            <span>Disponible</span>
                                        </div>
                                        <div class="button pt-10">
                                            <a href="{{ route('detail_service',[$pq->slug]) }}" class="main-btn"><i class="fa fa-cart-plus"></i> Ver mas</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>

@endsection


@section('extrajs')
@endsection
