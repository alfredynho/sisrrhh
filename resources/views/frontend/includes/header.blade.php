    <div class="top-header light_skin bg_dark d-none d-md-block">
        @include('frontend.includes.socials')
    </div>
    <div class="container">
        <nav class="navbar navbar-expand-lg"> 
        	<a class="navbar-brand" href="{{ route('landing') }}"> 
            	<img class="logo_light" src="{{ asset('frontend/images/SAPRH-black.png') }}" alt="RRHH"> 
                <img class="logo_dark" src="{{ asset('frontend/images/SAPRH-black.png') }}" alt="RRHH"> 
            </a>
          	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false"> <span class="ion-android-menu"></span> </button>
			<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                <ul class="navbar-nav">

                    <li><a class="dropdown-item nav-link nav_item active" href="{{ route('landing') }}">INICIO</a></li>

					<li class="dropdown"><a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">RRHH</a>
                        <div class="dropdown-menu">
							<ul>
                            	<li><a class="dropdown-item nav-link nav_item" href="{{ route('rrhh') }}">NOSOTROS</a></li>
                                {{--  <li><a class="dropdown-item nav-link nav_item" href="{{ route('calendar') }}">CALENDARIO</a></li>  --}}
                                <li class="dropdown"> <a class="dropdown-item menu-link dropdown-toggler" data-toggle="dropdown" href="#">PERSONAL</a>
									<div class="dropdown-menu">
                                        <ul>
                                            <li><a class="dropdown-item nav-link nav_item" href="{{ route('docentes') }}">VER PERSONAL</a></li>
                                        </ul>
									</div>
                                </li>
							</ul>
                        </div>
                    </li>
					<li class="dropdown"><a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">PROGRAMAS</a>
                        <div class="dropdown-menu">
							<ul>
                                <li><a class="dropdown-item nav-link nav_item" href="#">TALLERES</a></li>
                                <li><a class="dropdown-item nav-link nav_item" href="{{ route('courses') }}">CURSOS</a></li>
                          </ul>
                        </div>
                      </li>

                    <li class="dropdown"><a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">FORMULARIOS</a>
                        <div class="dropdown-menu">
                        	<ul>
                            	<li><a class="dropdown-item nav-link nav_item" href="#">LICENCIA POR HORA</a></li>
                            	<li><a class="dropdown-item nav-link nav_item" href="#">VACACIONES</a></li>
                            	<li><a class="dropdown-item nav-link nav_item" href="#">CAPACITACION</a></li>
                        	</ul>
                        </div>
                    </li>

                    <li><a class="dropdown-item nav-link nav_item active" href="{{ route('landing') }}">REGLAMENTOS</a></li>

                </ul>
          	</div>
        </nav>
    </div>