<div class="section background_bg counter_wrap bg_blue fixed_bg" data-img-src="{{ asset('frontend/images/estadisticas.png') }}">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-6 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                <div class="box_counter counter_white text-center"> <img src="{{ asset('frontend/images/participantes_cemtic.png') }}" alt="Participantes Cemtic">
                    <h3 class="counter_text"><span class="counter" data-from="0" data-to="280" data-speed="1500" data-refresh-interval="5"></span>+</h3>
                    <p>Personas Capacitadas</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-6 animation" data-animation="fadeInUp" data-animation-delay="0.3s">
                <div class="box_counter counter_white text-center"> <img src="{{ asset('frontend/images/cursos.png') }}" alt="Cursos cemtic">
                    <h3 class="counter_text"><span class="counter" data-from="0" data-to="1350" data-speed="1500" data-refresh-interval="5"></span>+</h3>
                    <p>Cursos/Programas</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-6 animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                <div class="box_counter counter_white text-center"> <img src="{{ asset('frontend/images/docentes_cemtic.png') }}" alt="Personal">
                    <h3 class="counter_text"><span class="counter" data-from="0" data-to="200" data-speed="1500" data-refresh-interval="5"></span>+</h3>
                    <p>Talleres Realizados</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-6 animation" data-animation="fadeInUp" data-animation-delay="0.5s">
                <div class="box_counter counter_white text-center"> <img src="{{ asset('frontend/images/clientes.png') }}" alt="Clientes Cemtic ">
                    <h3 class="counter_text"><span class="counter" data-from="0" data-to="150" data-speed="1500" data-refresh-interval="5"></span>+</h3>
                    <p>Clientes</p>
                </div>
            </div>
        </div>
    </div>
</div>