<div class="section background_bg bg_blue" data-img-src="{{ asset('frontend/images/fondo.png')}}">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-xl-6 col-lg-6 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                <div class="heading_s1 heading_apply_form heading_light">
					<h2>Registrarte ahora!</h2>
                </div>
                <p class="text-white leads">Aprende con los mejores profesionales, <strong>RRHH</strong> una plataforma para la actualización academica</p>
                <a href="{{ route('contactos') }}" class="btn btn-tran-light">Contactos</a>
                <a href="{{ route('courses') }}" class="btn btn-tran-border">Cursos</a>
            </div>
            <div class="col-xl-5 col-lg-6">
                <div class="bg-white apply_form radius_all_10 box_shadow1 animation" data-animation="fadeInUp" data-animation-delay="0.3s">
					<div class="heading_s1">
						<h3>Inscribirme a un curso</h3>
					</div>
                	<form>
                        <div class="form-group">
                            <input required="required" placeholder="Nombres *" class="form-control" name="firstname" type="text">
                        </div>

                        <div class="form-group">
                            <input required="required" placeholder="Apellidos *" class="form-control" name="lastname" type="text">
                        </div>

                        <div class="form-group">
                            <input required="required" placeholder="Correo *" class="form-control" name="email" type="email">
                        </div>
                        <div class="form-group">
                            <input required="required" placeholder="Celular *" class="form-control" name="phone" type="tel">
                        </div>

                        <div class="form-group">
                            <button type="submit" title="Inscribirme" class="btn btn-default btn-block" name="submit" value="Submit">Inscribirme</button>
                        </div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>