<div class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                <div class="heading_s1 text-center">
                    <h2>Lo que nuestros Estudiantes Opinan</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 animation" data-animation="fadeInUp" data-animation-delay="0.3s">
                <div class="testimonial_slider testimonial_style1 carousel_slider owl-carousel owl-theme nav_style1" data-margin="15" data-loop="true" data-autoplay="true" data-responsive='{"0":{"items": "1"}, "767":{"items": "2"}, "991":{"items": "3"}}'>
                    @foreach($memories as $memorie)
                        <div class="testimonial_box">
                            <div class="author_info">
                                <div class="author_img">
                                    <img src="{{ asset('frontend/images/estudiantes.jpg') }}" alt="Estudiantes de Cemtic" /> 
                                </div>
                                <div class="author_name">
                                    <h6>{{ $memorie->author }}</h6>
                                    <span>{{ $memorie->grade }}</span> 
                                </div>
                            </div>
                            <div class="testimonial_desc">
                                <p>{{ $memorie->message }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>