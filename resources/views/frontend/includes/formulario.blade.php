<section class="{{ flurryEffect() }} appointment-area ptb-100 jarallax" data-jarallax='{"speed": 0.3}'>
    <div class="container">
        <div class="appointment-content">
            <span class="sub-title">Formulario de Contactos</span>

            {!! Form::open(['route' => 'dashboard.contactos.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                <input id="formId" name="formId" type="hidden" value="11">
                <input id="status" name="status" type="hidden" value="1">

                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <div class="icon">
                                <i class="flaticon-user"></i>
                            </div>
                            <label>Nombre Completo</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <div class="icon">
                                <i class="flaticon-envelope"></i>
                            </div>
                            <label>Correo Electronico</label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <div class="icon">
                                <i class="flaticon-phone-call"></i>
                            </div>
                            <label>Celular</label>

                            <input type="text" class="form-control" id="phone" name="phone">

                        </div>
                    </div>


                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <div class="icon">
                                <i class="flaticon-support"></i>
                            </div>
                            <label>Asunto </label>

                            <input type="text" class="form-control" id="subject" name="subject">

                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <div class="icon">
                                <i class="flaticon-light-bulb"></i>
                            </div>
                            <label>Mensaje</label>
                            <textarea name="message" class="form-control" id="message" cols="30" rows="3" required data-error="Escriba su mensaje"></textarea>

                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <div class="submit-btn">
                            <button class="btn btn-primary">Enviar Formulario <i class="flaticon-right-chevron"></i></button>
                        </div>
                    </div>
                </div>
            {{Form::Close()}}
        </div>
    </div>
</section>
