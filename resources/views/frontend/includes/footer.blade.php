    <div class="footer_top">
    	<div class="container">
    		<div class="row">
    			<div class="col-xl-4 col-md-8 col-sm-12">
    				<div class="widget">
    					<div class="footer_logo"> 
                        	<a href="{{ route('cemtic') }}"><img src="{{ asset('frontend/images/SAPRH.png') }}" width="150" height="55" alt="RRHH"/></a> 
                        </div>
    					<p>Sistema de Administración de Personal Recursos Humanos</p>
    				</div>

    			</div>
                <div class="col-xl-2 col-md-4 col-sm-5">
                    <div class="widget">
                        <h6 class="widget_title">Enlaces directos</h6>
                        <ul class="widget_links">
                            <li><a href="{{ route('courses') }}">Cursos</a></li>
                            <li><a href="#">Formularios</a>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Calendario</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-sm-7">
                    <div class="widget">
                        <h6 class="widget_title">Publicaciones Recientes</h6>
                        @if(count(getBlogs()) >= 1)
                        <ul class="widget_recent_post">
                            @foreach (getBlogs() as $bg)                                
                                <li>
                                    <div class="post_footer">
                                        <div class="post_img"> 
                                            <a href="#"><img src="{{ asset('frontend/images/content.png') }}" width="90" heigth="90" alt="RRHH"></a> 
                                        </div>
                                        <div class="post_content">
                                            <h6><a href="#">{{ $bg->title }}</a></h6>
                                            <p class="small m-0">{{ $bg->created_at->format('d-m-Y') }}</p>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        @else
                            <div class="alert alert-primary text-center" role="alert">
                                Sistema de Administración de Personal Recursos Humanos
                            </div>            
                        @endif
                    </div>
                </div>

    		</div>
    	</div>
    </div>
    <div class="container">
    	<div class="row">
    		<div class="col-12">
    			<div class="bottom_footer border-top-tran">
    				<div class="row">
    					<div class="col-md-6">
    						<p class="mb-md-0 text-center text-md-left">© {{ now()->year }} <span class="text_default">RRHH</span></p>
    					</div>
                        <div class="col-md-6">
                            <ul class="list_none footer_link text-center text-md-right">
                                <li><a href="{{ route('landing') }}">Sistema de Administración de Personal Recursos Humanos</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>