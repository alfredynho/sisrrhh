<section class="partner-area ptb-100 bg-f4f9fd">
    <div class="container">
        <div class="section-title">
            <h2>Nuestros Convenios</h2>
        </div>

        <div class="customers-partner-list">
            @if(count($convenios) >= 1)
                @foreach ($convenios as $conv)
                    <div class="partner-item">
                        <a href="{{ route('detalle_convenio',[$conv->slug]) }}">
                            @if($conv->image)
                                <img src="{{ Storage::url('Convenios/'.$conv->image) }}" alt="{{ $conv->title}}" title="{{ $conv->title }}">
                            @else
                                <img src="{{ asset('frontend/img/convenio_cemtic.png') }}" alt="{{ $conv->title}}" title="{{ $conv->title }}">
                            @endif
                        </a>
                    </div>
                @endforeach
            @else
                <div class="alert alert-primary">
                    <strong> <i class="fa fa-exclamation-triangle"></i> Convenios - </strong> sin contenido disponible, nos encontramos creando el mejor contenido para ti
                </div>
            @endif
        </div>
    </div>
</section>
