  @if (Session::has('contactos-footer'))
      <div data-pushbar-id="pushbar-notification" class="pushbar from_bottom pushbar-notification">
        <div class="btn-close">
           	<button data-pushbar-close><i class="fa fa-times-circle"></i></button>
        </div>
        <div class="contenedor">
            <div class="main-title">
                <div class="title-main-page">
                <div class="container text-center">
                    <h5>{{ (Session::get('contactos-footer')) }} Su mensaje fue enviado exitosamente ... </h5>
                    <button data-pushbar-close type="button" class="btn btn-modern btn-rounded btn-primary mb-2">Ciencias de la Educación</button>
                </div>
                </div>
            </div>
            <br>
            </div>
        </div>
      </div>
  @endif

    @if(count($errors)>0)
      <!-- Pushboar -->
        <div data-pushbar-id="pushbar-notification" class="pushbar from_bottom pushbar-notification">
          <div class="btn-close">
             	<button data-pushbar-close><i class="fa fa-times-circle"></i></button>
          </div>
              <div class="contenedor">
                  <div class="main-title">
                      <div class="title-main-page">
                      <div class="container text-center">
                          <h5>
                            @foreach ($errors->all() as $error)
                                {{ $error}}
                            @endforeach
                          </h5>
                          <button data-pushbar-close type="button" class="btn btn-modern btn-rounded btn-primary mb-2">Ciencias de la Educación</button>
                          </div>
                      </div>
                  </div>
                  <br>
                  </div>
              </div>
        </div>
      <!-- Pushboar -->
    @endif


      <!-- Pushboar -->
        <div data-pushbar-id="pushbar-notification1" class="pushbar from_bottom pushbar-notification">
          <div class="btn-close">
             	<button data-pushbar-close><i class="fa fa-times-circle"></i></button>
          </div>
              <div class="contenedor">
                  <div class="main-title">
                      <div class="title-main-page">
                      <div class="container text-center">
                          <h5>
                            Hola mi mensaje
                          </h5>
                            <button data-pushbar-close type="button" class="btn btn-modern btn-rounded btn-primary mb-2">Ciencias de la Educación</button>
                          </div>
                      </div>
                  </div>
                  <br>
                  </div>
              </div>
        </div>
      <!-- Pushboar -->
