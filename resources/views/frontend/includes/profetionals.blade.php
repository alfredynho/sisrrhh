<div class="section pb_70">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                <div class="heading_s1 text-center">
                  <h2>El equipo de profesionales de CEMTIC SRL</h2>
                </div>
            	<p class="text-center leads">Aprende con los mejores profesionales</p>
            </div>
        </div>
        @if(count($profesionales) >= 1)
            <div class="row justify-content-center">
                @foreach ($profesionales as $prof)
                    <div class="col-lg-3 col-sm-6 animation" data-animation="fadeInUp" data-animation-delay="0.3s">
                        <div class="team_box team_style2 radius_all_10">
                            <div class="team_img"> 
                                @if ($prof->image)
                                    <img src="{{ $prof->image }}" alt="{{ $prof->name }}">
                                @else
                                    <img src="{{ asset('frontend/images/docentes.jpg') }}" alt="{{ $prof->name }}">
                                @endif
                                <ul class="social_icons social_white">
                                    <li><a href="{{ route('detail_profesional',[$prof->slug]) }}"><i class="ion-eye"></i></a></li>
                                </ul>
                            </div>
                            <div class="team_content">
                                <div class="team_title">
                                    <h5>{{ strtoupper($prof->name) }}</h5>
                                    <span>{{ strtoupper($prof->grado) }}</span> 
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div class="alert alert-primary text-center" role="alert">
                Nos encontramos preparando el mejor contenido para ti - CEMTIC SRL
            </div>
        @endif
	</div>
</div>