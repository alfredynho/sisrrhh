<div class="banner_section staggered-animation-wrap">
    <div id="carouselExampleControls" class="carousel slide carousel-fade light_arrow carousel_style2" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active background_bg overlay_bg_60" data-img-src="{{ asset('frontend/images/slider1.jpg') }}" alt="RRHH">
                <div class="banner_slide_content">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-7 col-md-12 col-sm-12 text-center">
                            	<div class="banner_content text_white">
                            		<h2 class="staggered-animation" data-animation="fadeInUp" data-animation-delay="0.2s">Sistema Web para el Control y Administración de Recursos Humanos</h2>
                            		<a class="btn btn-default staggered-animation" href="{{ route('courses') }}" data-animation="fadeInUp" data-animation-delay="0.6s">Ver Cursos</a> 
                            		<a class="btn btn-white staggered-animation" href="{{ route('courses') }}" data-animation="fadeInUp" data-animation-delay="0.6s">Publicaciones</a> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="carousel-item background_bg overlay_bg_60" data-img-src="{{ asset('frontend/images/slider2.jpg') }}" alt="RRHH">
                <div class="banner_slide_content">
                  <div class="container">
                    <div class="row justify-content-center">
                      <div class="col-lg-7 col-md-12 col-sm-12 text-center">
                        <div class="banner_content text_white">
                          <h2 class="staggered-animation" data-animation="fadeInUp" data-animation-delay="0.2s">Sistema Web para el Control y Administración de Recursos Humanos</h2>
                          <a class="btn btn-default staggered-animation" href="{{ route('courses') }}" data-animation="fadeInUp" data-animation-delay="0.6s">Ver Cursos</a> 
                          <a class="btn btn-white staggered-animation" href="{{ route('courses') }}" data-animation="fadeInUp" data-animation-delay="0.6s">Publicaciones</a> </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="carousel-item background_bg overlay_bg_60" data-img-src="{{ asset('frontend/images/slider3.jpg') }}" alt="RRHH">
                <div class="banner_slide_content">
                  <div class="container">
                    <div class="row justify-content-center">
                      <div class="col-lg-7 col-md-12 col-sm-12 text-center">
                        <div class="banner_content text_white">
                          <h2 class="staggered-animation" data-animation="fadeInUp" data-animation-delay="0.2s">Sistema Web para el Control y Administración de Recursos Humanos</h2>
                          <a class="btn btn-default staggered-animation" href="{{ route('courses') }}" data-animation="fadeInUp" data-animation-delay="0.6s">Ver Cursos</a>
                          <a class="btn btn-white staggered-animation" href="{{ route('courses') }}" data-animation="fadeInUp" data-animation-delay="0.6s">Publicaciones</a> </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    	<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"><i class="ion-chevron-left"></i></a> 
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"><i class="ion-chevron-right"></i></a> 
    </div>
</div>
