<div class="breadcrumb_section background_bg overlay_bg_50 page_title_light" data-img-src="{{ asset('frontend/images/banner.png') }}">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title">
            		<h1>RRHH</h1>
                </div>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('landing') }}">Inicio</a></li>
                    <li class="breadcrumb-item active">Nosotros</li>
                </ol>
            </div>
        </div>
    </div>
</div>