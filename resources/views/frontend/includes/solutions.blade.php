<div class="section pb_70">
	<div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                <div class="heading_s1 text-center">
                  <h2>Soluciones Cemtic</h2>
                </div>
            	<p class="text-center leads">Nuestras soluciones para cada tipo de institución</p>
            </div>
        </div>

        <div class="row justify-content-center">
        	<div class="col-lg-4 col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.2s">
            	<div class="icon_box icon_box_style1 box_shadow1 text-center">
                	<div class="icon ibc_orange">
                        <img src="{{ asset('frontend/images/cursos.png') }}" alt="curso cemtic">
                    </div>
                    <div class="icon_box_content">
                    	<h5>Unidades Educativas</h5>
                        <p>Inicial,Primaria,Secundaria</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.3s">
            	<div class="icon_box icon_box_style1 box_shadow1 text-center">
                	<div class="icon ibc_green">
                        <img src="{{ asset('frontend/images/docentes_cemtic.png') }}" alt="curso cemtic">
                    </div>
                    <div class="icon_box_content">
                    	<h5>Formación y Capacitación Laboral</h5>
                        <p>Capacitación a Instituciones, Universidades, Empresas</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 animation" data-animation="fadeInUp" data-animation-delay="0.4s">
            	<div class="icon_box icon_box_style1 box_shadow1 text-center">
                	<div class="icon ibc_pink">
                        <img src="{{ asset('frontend/images/participantes_cemtic.png') }}" alt="curso cemtic">
                    </div>
                    <div class="icon_box_content">
                    	<h5>Educación Superior</h5>
                        <p>Cursos, Diplomados, Maestrias, Doctorado</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>