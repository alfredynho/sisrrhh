<script src="{{ asset('frontend/js/jquery-1.12.4.min.js') }}"></script> 
<script src="{{ asset('frontend/bootstrap/js/bootstrap.min.js') }}"></script> 
<script src="{{ asset('frontend/owlcarousel/js/owl.carousel.min.js') }}"></script> 
<script src="{{ asset('frontend/js/magnific-popup.min.js') }}"></script> 
<script src="{{ asset('frontend/js/waypoints.min.js') }}"></script> 
<script src="{{ asset('frontend/js/parallax.js') }}"></script> 
<script src="{{ asset('frontend/js/jquery.countdown.min.js') }}"></script> 
<script src="{{ asset('frontend/js/jquery.countTo.js') }}"></script> 
<script src="{{ asset('frontend/js/imagesloaded.pkgd.min.js') }}"></script> 
<script src="{{ asset('frontend/js/isotope.min.js') }}"></script> 
<script src="{{ asset('frontend/js/jquery.appear.js') }}"></script> 
<script src="{{ asset('frontend/js/jquery.dd.min.js') }}"></script>
<script src="{{ asset('frontend/js/slick.min.js') }}"></script> 
<script src="{{ asset('frontend/js/scripts.js') }}"></script>

<script src="{{ asset('frontend/js/jquery.flurry.min.js') }}"></script>

<script>
    $('.decorator').flurry({
        character: '❄❅❆*',
        speed: 3000,
        height: 480,
        frequency: 60,
        small: 12,
        large: 50,
        rotation: 90,
        rotationVariance: 20,
        startRotation: 90,
        wind: 10,
        windVariance: 100,
        opacityEasing: "cubic-bezier(1,0,.96,.9)"
    });

</script>
