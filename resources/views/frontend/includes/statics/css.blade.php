<link rel="stylesheet" href="{{ asset('frontend/css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/bootstrap/css/bootstrap.min.css') }}">
<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet"> 
<link rel="stylesheet" href="{{ asset('frontend/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/ionicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/themify-icons.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/linearicons.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/flaticon.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/owlcarousel/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/owlcarousel/css/owl.theme.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/owlcarousel/css/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">
<link rel="stylesheet" id="layoutstyle" href="{{ asset('frontend/color/theme-yellow.css') }}">