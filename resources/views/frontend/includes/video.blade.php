<div class="{{ flurryEffect() }} section background_bg overlay_bg_70 fixed_bg" data-img-src="{{ asset('frontend/images/slider3.jpg') }}">
	<div class="container">
    	<div class="row justify-content-center">
        	<div class="col-xl-6 col-lg-8 col-md-10">
            	<div class="text-center animation" data-animation="fadeInUp" data-animation-delay="0.2s">
                	<a href="{{ getSocial('VIDEO') }}" class="video_popup">
                    	<span class="ripple"><i class="ion-play"></i></span>
                    </a>
                    <div class="video_text animation" data-animation="fadeInUp" data-animation-delay="0.3s">
                        <span class="text_default">CEMTIC SRL</span>
                        <h2>Explora, Aprende, Aplica</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
