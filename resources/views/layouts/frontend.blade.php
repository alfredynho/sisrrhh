<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="Templatemanja" name="SISTEMA DE ADMINISTRACION DE PERSONAL RECURSOS HUMANOS">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="SISTEMA DE ADMINISTRACION DE PERSONAL RECURSOS HUMANOS.">
<meta name="keywords" content="SISTEMA DE ADMINISTRACION DE PERSONAL RECURSOS HUMANOS.">

<title>RRHH</title>

<link rel="shortcut icon" type="image/x-icon" href="{{ asset('frontend/images/favicon.png') }}">
    <!-- START css -->
        @include('frontend.includes.statics.css')
    <!-- END css -->

    @yield('extracss')
        <!-- EXTRA CSS -->
    @show('extracss')

</head>

<body>

<!-- LOADER -->
    <div id="preloader">
        <div class="lds-ellipsis">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
<!-- END LOADER --> 

<!-- START HEADER -->
    <header class="header_wrap fixed-top dark_skin main_menu_uppercase header_with_topbar">
        @include('frontend.includes.header')
    </header>
<!-- END HEADER --> 

<!-- START CONTENT -->
    @yield('content')
        <!-- content page web -->
    @show('content')
<!-- END CONTENT -->

<!-- START FOOTER -->
    <footer class="bg_dark footer_dark">
        @include('frontend.includes.footer')
    </footer>
<!-- END FOOTER --> 

    <a href="#" class="scrollup" style="display: none;"><i class="ion-ios-arrow-up"></i></a> 

    @include('frontend.includes.statics.js')
    
    @yield('extrajs')
        <!-- EXTRA JS -->
    @show('extrajs')
</body>
</html>