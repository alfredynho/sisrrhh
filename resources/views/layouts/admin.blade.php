
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>RRHH RECURSOS HUMANOS</title>
      <meta name="description" content="SISTEMA DE ADMINISTRACION DE PERSONAL RECURSOS HUMANOS" />
      <meta name="keywords" content="SISTEMA DE ADMINISTRACION DE PERSONAL RECURSOS HUMANOS">
      <meta name="author" content="SISTEMA DE ADMINISTRACION DE PERSONAL RECURSOS HUMANOS" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
      <meta name="theme-color" content="#005C1F">
  
      <link rel="shortcut icon" href="{{ asset('admin/images/favicon.ico') }}" />
      @include('dashboard.includes.statics.css')
    </head>
  <body class="  ">

    @include('dashboard.includes.loader')

    <div class="wrapper">
      <div class="mm-sidebar  sidebar-default ">
          <div class="mm-sidebar-logo d-flex align-items-center justify-content-between">
              <a href="" class="header-logo">
                  <img src="{{ asset('admin/images/rrhh.png') }}" class="img-fluid rounded-normal light-logo" alt="logo">
                  <img src="{{ asset('admin/images/rrhh.png')}}" class="img-fluid rounded-normal darkmode-logo" alt="logo">
              </a>
              <div class="side-menu-bt-sidebar">
                  <i class="las la-bars wrapper-menu"></i>
              </div>
          </div>
          <div class="data-scrollbar" data-scroll="1">
              <nav class="mm-sidebar-menu">
                @include('dashboard.includes.navigation')
              </nav>
              <div class="pt-5 pb-2"></div>
          </div>
      </div>       
      
      <div class="mm-top-navbar">
        @include('dashboard.includes.header')
      </div>
      
      <div class="content-page">
        <div class="container-fluid">
            @yield('content')
        </div>
      </div>
    </div>

    <footer class="mm-footer">
        @include('dashboard.includes.footer')
    </footer>    

    @include('dashboard.includes.statics.js')

</body>
</html>