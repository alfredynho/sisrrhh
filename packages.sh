# universal-install
package=$1
apt=`command -v apt-get`
yum=`command -v yum`

if [ -n "$apt" ]; then
    apt-get update
    apt-get -y install $package
elif [ -n "$yum" ]; then
    yum -y install $package
else
    echo "Err: no path to apt-get or yum" >&2;
    exit 1;
fi
