<?php

    // <<<<<   URL PARA CONSUMIR LOS MAPAS: AJAX, VUEJS, BLADE, OPENSTREETMAP, AXIOS, LEAFLET >>>>>> //

    Route::group(['as' => 'api.', 'namespace' => 'Api'], function () {
        Route::get('maps', 'OutletController@index')->name('maps.index');
    });
