<?php

Route::get('/', 'CemticController@index')->name('landing');

Route::get('/publicaciones/{slug}', 'CemticController@detail_publication')->name('detail_publication');

Route::get('/cemtic','CemticController@cemtic')->name('cemtic');

Route::get('/eventos','CemticController@events')->name('events');

Route::get('/blog', 'CemticController@blogs')->name('blogs');
Route::get('/noticias', 'CemticController@noticias')->name('news');
Route::get('/anuncios', 'CemticController@anuncios')->name('anuncios');

Route::get('/anuncio/{id}', 'CemticController@detail_anuncio')->name('detail_anuncio');

Route::get('/programa/{id}', 'CemticController@detail_program')->name('detail_programa');

Route::get('/admision/{id}', 'CemticController@detail_admision')->name('detail_admision');

Route::get('/profesional/{slug}', 'CemticController@detail_profesional')->name('detail_profesional');

Route::get('/blog/{id}', 'CemticController@detail_new')->name('detail_new');

Route::get('/galeria', 'CemticController@gallery')->name('gallery');

Route::get('/programas', 'CemticController@programs')->name('programas');

Route::get('/descargas', 'CemticController@download')->name('descargas');

Route::get('/faqs', 'CemticController@faqs')->name('faqs');

Route::get('/admisiones', 'CemticController@admisiones')->name('admisiones');

Route::get('/acreditacion', 'CemticController@acreditacion')->name('acreditacion');

Route::get('/admisiones/detalle', 'CemticController@admisiones_detalle')->name('admisiones_detalle');

Route::get('/docentes', 'CemticController@profesionales')->name('docentes');

Route::get('/autoridad/{id}', 'CemticController@detail_autoridad')->name('detail_autoridades');

Route::get('/nosotros', 'CemticController@nosotros')->name('nosotros');

Route::get('/marco_normativo', 'CemticController@normativo')->name('normativo');

Route::get('/capacitaciones', 'CemticController@capacitacion')->name('capacitacion');

Route::get('/paquetes', 'CemticController@paquetes')->name('paquetes');

Route::get('/contactos-cemtic', 'CemticController@contactoscemtic')->name('contactos');

Route::post('/contactos', 'Dashboard\ContactController@store')->name('dashboard.contactos.store');

Route::get('/servicios', 'CemticController@servicios')->name('servicios_cemtic');

Route::get('/servicio/{id}', 'CemticController@detail_service')->name('detail_service');

Route::get('/convenios', 'CemticController@convenios')->name('convenios');

Route::get('lang/{lang}', 'LanguageController@swap')->name('lang.swap');

Route::get('blog-cemtic','CemticController@Blogs')->name('blogs');

Route::get('/convenio/{id}', 'CemticController@detalle_convenio')->name('detalle_convenio');

Route::get('/paquete/{id}', 'CemticController@detail_paquete')->name('detail_paquete');

Route::post('/paquete', 'Dashboard\PackageController@store')->name('dashboard.package.store');
Route::post('/contactos', 'Dashboard\ContactController@store')->name('dashboard.contactos.store');

Route::get('/descarga-documento-inso/{file_download}', 'CemticController@downloadDocuments')->name('descarga-documentos');

Route::get('/cursos-rrhh', 'CemticController@courses')->name('courses');

Route::get('/clientes', 'CemticController@clients')->name('clientes');

Route::get('/galeria', 'CemticController@gallery')->name('galeria');

Route::get('/cliente/{id}', 'CemticController@detail_client')->name('detail_client');

Route::get('/curso/{id}', 'CemticController@detail_course')->name('detail_course');

Route::get('/faqs', 'CemticController@faqs')->name('faqs');

Route::get('/calendario', 'CemticController@calendar')->name('calendar');

Route::get('/rrhh', 'CemticController@rrhh')->name('rrhh');


Route::get('/clear/{cod}',function($cod){
    if($cod=='Cemtic.2020+'){
        Artisan::call('cache:clear');
        Artisan::call('config:clear');
        Artisan::call('route:clear');
        Artisan::call('view:clear');
        return '<h1> caches borrados Exitosamente - CEMTIC </h1>';
    }else{
        return back();
    }
});

Route::get('/clear-bot/{cod}',function($cod){
    if($cod=='Cemtic.2020+'){
        Artisan::call('config:cache');
        Artisan::call('config:clear');
        return '<h1> caches borrados Exitosamente - CEMTIC </h1>';
    }else{
        return back();
    }
});

// --> Route chatbot Facebook
Route::get('/webhook', 'WebhookController@getWebhook');
Route::post('/webhook', 'WebhookController@postWebhook');


Route::get('/faqs/{slug}', 'CemticController@detail_category_frontend')->name('questionsfaqs');

Route::get('cities', 'CemticController@citys_index')->name('cities-index');
