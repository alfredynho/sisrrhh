<?php

Route::get('/dashboard', 'Dashboard\DashboardController@index')->name('dashboard');


// urls Course
Route::get('/dashboard/personal', 'Dashboard\PersonalController@index')->name('dashboard.course.index');
Route::delete('/dashboard/personal/{id}', 'Dashboard\PersonalController@destroy')->name('dashboard.course.destroy');
Route::put('/dashboard/personal/{id}', 'Dashboard\PersonalController@update')->name('dashboard.course.update');
Route::get('/dashboard/modificar-personal/{slug}', 'Dashboard\PersonalController@show')->name('dashboard.course.show');
Route::post('/dashboard/personal', 'Dashboard\PersonalController@store')->name('dashboard.course.store');
Route::get('/dashboard/personal-crear', 'Dashboard\PersonalController@create')->name('dashboard.course.create');



// urls socials
Route::get('/dashboard/redes', 'Dashboard\SocialController@index')->name('dashboard.social.index');
Route::delete('/dashboard/redes/{id}', 'Dashboard\SocialController@destroy')->name('dashboard.social.destroy');
Route::put('/dashboard/redes/{id}','Dashboard\SocialController@update')->name('dashboard.social.update');
Route::post('/dashboard/redes', 'Dashboard\SocialController@store')->name('dashboard.social.store');

// urls Regionales
Route::get('/dashboard/regionales', 'Dashboard\RegionalController@index')->name('dashboard.regional.index');
Route::delete('/dashboard/regionales/{id}', 'Dashboard\RegionalController@destroy')->name('dashboard.regional.destroy');
Route::put('/dashboard/regionales/{id}', 'Dashboard\RegionalController@update')->name('dashboard.regional.update');
Route::post('/dashboard/regionales', 'Dashboard\RegionalController@store')->name('dashboard.regional.store');

Route::get('/dashboard/crear-regional/', 'Dashboard\RegionalController@create')->name('dashboard.regional.create');

Route::get('/dashboard/modificar-regional/{id}', 'Dashboard\RegionalController@show')->name('dashboard.regional.show');

// urls Profesionales
Route::get('/dashboard/profesionales', 'Dashboard\ProfetionalController@index')->name('dashboard.profetional.index');
Route::delete('/dashboard/profesionales/{id}', 'Dashboard\ProfetionalController@destroy')->name('dashboard.profetional.destroy');
Route::put('/dashboard/profesionales/{id}', 'Dashboard\ProfetionalController@update')->name('dashboard.profetional.update');
Route::post('/dashboard/profesionales', 'Dashboard\ProfetionalController@store')->name('dashboard.profetional.store');


// url para descarga de csv
Route::get('dashboard/export', 'Dashboard\BlogController@export')->name('export-csv-blog');

// urls categoria de Cursos
Route::get('/dashboard/categoria-cursos', 'Dashboard\CategoryCourseController@index')->name('dashboard.ccourse.index');
Route::delete('/dashboard/categoria-cursos/{id}', 'Dashboard\CategoryCourseController@destroy')->name('dashboard.ccourse.destroy');
Route::put('/dashboard/categoria-cursos/{id}', 'Dashboard\CategoryCourseController@update')->name('dashboard.ccourse.update');
Route::post('/dashboard/categoria-cursos', 'Dashboard\CategoryCourseController@store')->name('dashboard.ccourse.store');


Route::get('/dashboard/messenger','Dashboard\MessengerController@index')->name('admin-messenger');
Route::delete('/dashboard/messenger/{id}','Dashboard\MessengerController@destroy')->name('admin-messenger-destroy');

// Detalle de preguntas por slug de categoria
Route::get('/dashboard/questions/{slug}', 'Dashboard\BotController@detail_category')->name('dashboard.dtquestion.index');

// urls para Eventos
Route::get('/dashboard/eventos', 'Dashboard\EventosController@index')->name('dashboard.eventos.index');
Route::delete('/dashboard/eventos/{id}', 'Dashboard\EventosController@destroy')->name('dashboard.eventos.destroy');
Route::put('/dashboard/eventos/{id}', 'Dashboard\EventosController@update')->name('dashboard.eventos.update');
Route::post('/dashboard/eventos', 'Dashboard\EventosController@store')->name('dashboard.eventos.store');
Route::get('/dashboard/crear-eventos/', 'Dashboard\EventosController@create')->name('dashboard.eventos.create');
Route::get('/dashboard/modificar-eventos/{id}', 'Dashboard\EventosController@show')->name('dashboard.eventos.show');


Route::get('/dashboard/parentesco', 'Dashboard\ParentescoController@index')->name('dashboard.parentesco.index');
Route::delete('/dashboard/parentesco/{id}', 'Dashboard\ParentescoController@destroy')->name('dashboard.parentesco.destroy');
Route::put('/dashboard/parentesco/{id}', 'Dashboard\ParentescoController@update')->name('dashboard.parentesco.update');
Route::post('/dashboard/parentesco', 'Dashboard\ParentescoController@store')->name('dashboard.parentesco.store');


Route::get('/dashboard/cargo', 'Dashboard\CargoController@index')->name('dashboard.cargo.index');
Route::delete('/dashboard/cargo/{id}', 'Dashboard\CargoController@destroy')->name('dashboard.cargo.destroy');
Route::put('/dashboard/cargo/{id}', 'Dashboard\CargoController@update')->name('dashboard.cargo.update');
Route::post('/dashboard/cargo', 'Dashboard\CargoController@store')->name('dashboard.cargo.store');
Route::get('/imprimir', 'Dashboard\CargoController@reporte')->name('dashboard.cargo.reporte');


Route::get('/dashboard/unidad', 'Dashboard\UnidadController@index')->name('dashboard.unidad.index');
Route::delete('/dashboard/unidad/{id}', 'Dashboard\UnidadController@destroy')->name('dashboard.unidad.destroy');
Route::put('/dashboard/unidad/{id}', 'Dashboard\UnidadController@update')->name('dashboard.unidad.update');
Route::post('/dashboard/unidad', 'Dashboard\UnidadController@store')->name('dashboard.unidad.store');


Route::get('/dashboard/profesion', 'Dashboard\ProfesionesController@index')->name('dashboard.profesion.index');
Route::delete('/dashboard/profesion/{id}', 'Dashboard\ProfesionesController@destroy')->name('dashboard.profesion.destroy');
Route::put('/dashboard/profesion/{id}', 'Dashboard\ProfesionesController@update')->name('dashboard.profesion.update');
Route::post('/dashboard/profesion', 'Dashboard\ProfesionesController@store')->name('dashboard.profesion.store');


Route::get('/dashboard/contratos', 'Dashboard\ContratosController@index')->name('dashboard.contrato.index');
Route::delete('/dashboard/contratos/{id}', 'Dashboard\ContratosController@destroy')->name('dashboard.contrato.destroy');
Route::put('/dashboard/contratos/{id}', 'Dashboard\ContratosController@update')->name('dashboard.contrato.update');
Route::post('/dashboard/contratos', 'Dashboard\ContratosController@store')->name('dashboard.contrato.store');

Route::get('/dashboard/personal', 'Dashboard\PersonalController@index')->name('dashboard.personal.index');
Route::delete('/dashboard/personal/{id}', 'Dashboard\PersonalController@destroy')->name('dashboard.personal.destroy');
Route::put('/dashboard/personal/{id}', 'Dashboard\PersonalController@update')->name('dashboard.personal.update');
Route::post('/dashboard/personal', 'Dashboard\PersonalController@store')->name('dashboard.personal.store');
Route::get('/dashboard/crear-personal/', 'Dashboard\PersonalController@create')->name('dashboard.personal.create');
Route::get('/dashboard/modificar-personal/{id}', 'Dashboard\PersonalController@show')->name('dashboard.personal.show');
Route::get('/reporte/{id}', 'Dashboard\PersonalController@reporte_funcionario')->name('dashboard.personal.reporte_funcionario');
Route::get('/detalle-personal/{id}', 'Dashboard\PersonalController@detalle')->name('dashboard.personal.detalle');


Route::get('/dashboard/ausencias', 'Dashboard\AusenciasController@index')->name('dashboard.ausencias.index');
Route::delete('/dashboard/ausencias/{id}', 'Dashboard\AusenciasController@destroy')->name('dashboard.ausencias.destroy');
Route::put('/dashboard/ausencias/{id}', 'Dashboard\AusenciasController@update')->name('dashboard.ausencias.update');
Route::post('/dashboard/ausencias', 'Dashboard\AusenciasController@store')->name('dashboard.ausencias.store');
Route::get('/dashboard/crear-ausencias/', 'Dashboard\AusenciasController@create')->name('dashboard.ausencias.create');
Route::get('/dashboard/modificar-ausencias/{id}', 'Dashboard\AusenciasController@show')->name('dashboard.ausencias.show');


Route::get('/dashboard/estudios', 'Dashboard\EstudiosController@index')->name('dashboard.estudios.index');
Route::delete('/dashboard/estudios/{id}', 'Dashboard\EstudiosController@destroy')->name('dashboard.estudios.destroy');
Route::put('/dashboard/estudios/{id}', 'Dashboard\EstudiosController@update')->name('dashboard.estudios.update');
Route::post('/dashboard/estudios', 'Dashboard\EstudiosController@store')->name('dashboard.estudios.store');
