<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::group(['middleware' => 'auth'], function () {
    require __DIR__ . '/backend.php';
});

Auth::routes();
require __DIR__ . '/frontend.php';
