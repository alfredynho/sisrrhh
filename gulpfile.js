var path_frontend_static = "public/cemtic/img/originales/*"
var path_img_webp = "public/cemtic/img/webps/"
var path_frontend_css = "public/cemtic/cp/*"
var path_frontend_js = "public/cemtic/cp/*"


var path_backend_static = "public/admin/cp/*"
var path_backend_css = "public/admin/cp/*"
var path_backend_js = "public/admin/cp/*"


var gulp = require('gulp');
var minifycss = require('gulp-minify-css');

gulp.task('css',function(){
    return gulp.src('public/bot/css/*.css')
    .pipe(minifycss())
    .pipe(gulp.dest('public/bot/css/min'))
});



var uglify = require('gulp-uglify');

// Gulp task to minify JavaScript files
gulp.task('js', function() {
    return gulp.src('public/bot/js/*.js')
      // Minify the file
      .pipe(uglify())
      // Output
      .pipe(gulp.dest('public/bot/js/min'))
  });

// Convertir imagenes png, jpeg a formato webp
var cwebp = require('gulp-cwebp');

gulp.task('img', function () {
    gulp.src(path_frontend_static)
        .pipe(cwebp())
        .pipe(gulp.dest(path_img_webp));
});
