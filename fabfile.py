# encoding: utf-8

from fabric.api import *
from fabric.contrib.files import exists, upload_template


# the user to use for the remote commands
env.user = 'root'
# the servers where the commands are executed
env.hosts = ['root@45.77.29.20']
site_root = "/var/www/html/"
site_root_project = "/var/www/html/sisrrhh/"


env.project_name = '$(cloudbot)'
env.database_password = '$(db_password)'
env.site_media_prefix = "site_media"
env.admin_media_prefix = "admin_media"
env.newsapps_media_prefix = "na_media"
env.path = '/var/www/html/'
env.log_path = '/home/newsapps/logs/%(project_name)s' % env
env.env_path = '%(path)s/env' % env
env.repo_path = '%(path)s/repository' % env
env.apache_config_path = '/home/newsapps/sites/apache/%(project_name)s' % env
env.python = 'python2.6'

#install_dir = "/etc/nginx/sites-available"

#@task
def _upload_conf():
    template = 'devzone'
    conf_path = '/etc/nginx/sites-available'
    upload_template(template, conf_path, {'worker_processes':1, 'server_name':"develop"},   use_sudo=True)
    sudo('ln -s /etc/nginx/sites-available/devzone /etc/nginx/sites-enabled/')


def prueba():
    sudo('apt-get install webp -y')


def _upgrade():
    """
    Update and upgrade server.
    """

    sudo('apt-get update')
    sudo('apt-get upgrade -y')

def _nginx():
    """
    Install Nginx.
    """

    sudo('apt-get install nginx -y')
    run('systemctl start nginx.service')


def _db():

    sudo('apt-get install mariadb-server mariadb-client -y')
    sudo('systemctl start mariadb.service')
    sudo('mysql_secure_installation')
    sudo('systemctl restart mariadb.service')


def _php():
    """
        Install Dependencies PHP and php-fpm
    """

    sudo('apt install php-fpm php-common php-mbstring php-xmlrpc php-soap php-gd php-xml php-mysql php-cli php-zip -y')

def _git():
    """
    """
    sudo('apt install git -y')

def _composer():
    """
        Install composer for laravel
    """

    sudo('apt install composer -y')


def _deploy():
    """
    Clone Project
    """
    with cd(site_root):
        run('git clone git@bitbucket.org:alfredynho/sisrrhh.git')

def _deps():
    with cd(site_root_project):
        # sudo('rm -rf .env')
        # sudo('cp -r .env.example .env')
        sudo('apt install make -y')
        sudo('composer install')
        sudo('make clear')
        sudo('service nginx restart')
        sudo('chmod 777 -R storage')

def _bot():
    with cd(site_root_project):
        sudo('make bot-clear')


def _ssl():
    sudo('apt-get install certbot python-certbot-nginx -y')
    sudo('certbot --nginx')


@task(alias="i")
def install():
    _upgrade()
    _nginx()
    _upload_conf()
    _git()
    _php()
    _composer()
    _deploy()
    _deps()
    _ssl()
    # _bot()
    #prueba()
