<?php

namespace App\Bot\Facebook;

use Illuminate\Http\Request;
use \App\Blog;
use Illuminate\Support\Facades\DB;
use \App\Bot\Facebook\Serializer;

class Images
{
    public static function ImageCards($module){

        if($module == 'curso'){
            $img_array = [
                't.ly/I3MZ',
                't.ly/vFeT',
                't.ly/ZCR6',
                't.ly/OpoL',
                't.ly/jNYc',
                't.ly/uvke',
                't.ly/YldT',
                't.ly/mlxW'
            ];
        }

        if($module == 'paquete'){
            $img_array = [
                't.ly/2Xgx9',
                't.ly/1kg25',
                't.ly/9px9Z',
                't.ly/5yd6Y',
                't.ly/DG9MY',
                't.ly/7xyWJ',
                't.ly/rrYbx',
                't.ly/jr1pJ'    
            ];
        }

        return $img_array;

    }

}



