<?php

namespace App\Bot\Facebook;

use Illuminate\Http\Request;
use \App\Blog;
use \App\Models\Course;
use Illuminate\Support\Facades\DB;
use \App\Bot\Facebook\Serializer;
use Illuminate\Support\Facades\Storage;
use \App\Bot\Facebook\Images;


class Card
{
    public static function itemQuicReplace()
    {
        $results = [];

        $blog = Blog::orderBy('id', 'desc')->take(3)
            ->where('blogs.status', '=', '1')
            ->get();

        if (20 > 200) {
            dump("True");
            foreach ($blog as $bg) {
                $results[] = [
                    'content_type' => 'text',
                    'title' => $bg->title,
                    'payload' => $bg->author,
                    'image_url' => 'https://res.cloudinary.com/due8e2c3a/image/upload/v1559674847/alfredynho/mldjango.png'
                ];
            }
            return $results;
        } else {
            dump("false");

            $default[] = [
                'content_type' => 'text',
                'title' => "sin Registros",
                'payload' => "START",
                'image_url' => 'https://res.cloudinary.com/due8e2c3a/image/upload/v1559674847/alfredynho/mldjango.png'
            ];

            dump($default);

            return $default;
        }
    }


    public static function quickReplace($sender)
    {
        $data = [
            'json' =>
            [
                'recipient' => ['id' => $sender],
                'message' => [
                    'text' => 'tienes las siguientes opciones!',
                    'quick_replies' => Card::itemQuicReplace()
                ]
            ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;
    }


    public static function itemGenericTemplate($rqhome, $queryset, $module)
    {
        $images_array = Images::ImageCards($module);
        $results = [];

        if (count($queryset) >= 1) {
            foreach ($queryset as $key => $qr) {
                $results[] = [
                    "title" => $qr->title,
                    "item_url" => $rqhome.'/'.$module.'/'.$qr->slug,
                    "image_url" => $images_array[$key],
                    "subtitle" => "CEMTIC ".strtoupper($module),
                    'buttons' => [
                        [
                            'type' => 'web_url',
                            'url' => $rqhome.'/'.$module.'/'.$qr->slug,
                            'title' => 'Ver '.$module
                        ]
                    ]
                ];
            }
            return $results;
        } else {

            $default[] = [
                "title" => "CEMTIC",
                "item_url" => env('APP_URL'),
                "image_url" => 't.ly/B9WWp',
                "subtitle" => "CCE UPEA",
                'buttons' => [
                    [
                        'type' => 'web_url',
                        'url' => $rqhome,
                        'title' => 'CEMTIC '.$module
                    ]
                ]
            ];


            return $default;
        }
    }

    public static function genericTemplate($sender, $rqhome, $queryset, $module)
    {
        $data = [
            'json' =>
            [
                'recipient' => ['id' => $sender],
                'message' => [
                    'attachment' => [
                        'type' => 'template',
                        'payload' => [
                            'template_type' => 'generic',
                            'elements' => Card::itemGenericTemplate($rqhome, $queryset, $module)
                        ]
                    ]
                ]
            ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;
    }

    public static function genericTemplateUpea($sender)
    {
        $data = [
            'json' =>
            [
                'recipient' => ['id' => $sender],
                'message' => [
                    'attachment' => [
                        'type' => 'template',
                        'payload' => [
                            'template_type' => 'generic',
                            'elements' => [
                                [
                                    "title" => "Libros",
                                    "item_url" => "http://upea-ccevirtual.edu.bo",
                                    "image_url" => "https://res.cloudinary.com/due8e2c3a/image/upload/v1569244728/slider%20otro%20bot/sistemas.png",
                                    "subtitle" => "cce LA PAZ.",
                                    'buttons' => [
                                        [
                                            'type' => 'web_url',
                                            'url' => 'http://upea-ccevirtual.edu.bo',
                                            'title' => 'Ver Carrera'
                                        ]
                                    ]
                                ],

                                [
                                    "title" => "Libros",
                                    "item_url" => "http://upea-ccevirtual.edu.bo",
                                    "image_url" => "https://res.cloudinary.com/due8e2c3a/image/upload/v1569244729/slider%20otro%20bot/secre.png",
                                    "subtitle" => "Biblioteca Virtual",
                                    'buttons' => [
                                        [
                                            'type' => 'web_url',
                                            'url' => 'http://upea-ccevirtual.edu.bo',
                                            'title' => 'Ver libro'
                                        ]
                                    ]
                                ],

                                [
                                    "title" => "libro1",
                                    "item_url" => "http://upea-ccevirtual.edu.bo",
                                    "image_url" => "https://res.cloudinary.com/due8e2c3a/image/upload/v1569244727/slider%20otro%20bot/conta.png",
                                    "subtitle" => "Biblioteca",
                                    'buttons' => [
                                        [
                                            'type' => 'web_url',
                                            'url' => 'http://upea-ccevirtual.edu.bo',
                                            'title' => 'Ver libro'
                                        ]
                                    ]
                                ],
                                [
                                    "title" => "libro3",
                                    "item_url" => "http://upea-ccevirtual.edu.bo",
                                    "image_url" => "https://res.cloudinary.com/due8e2c3a/image/upload/v1569244719/slider%20otro%20bot/idiomas.png",
                                    "subtitle" => "Biblioteca",
                                    'buttons' => [
                                        [
                                            'type' => 'web_url',
                                            'url' => 'http://upea-ccevirtual.edu.bo',
                                            'title' => 'Ver Libro'
                                        ]
                                    ]
                                ],
                                [
                                    "title" => "Libro4",
                                    "item_url" => "http://upea-ccevirtual.edu.bo",
                                    "image_url" => "https://res.cloudinary.com/due8e2c3a/image/upload/v1569244738/slider%20otro%20bot/comercio.png",
                                    "subtitle" => "Biblioteca",
                                    'buttons' => [
                                        [
                                            'type' => 'web_url',
                                            'url' => 'http://upea-ccevirtual.edu.bo',
                                            'title' => 'Ver Libro'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;
    }


    public static function listTemplate($sender)
    {

        $data = [
            'json' =>
            [
                'recipient' => ['id' => $sender],

                "message" => [
                    "attachment" => [
                        "type" => "template",
                        "payload" => [
                            "template_type" => "list",
                            "top_element_style" => "compact",
                            "elements" => [
                                [
                                    "title" => "Classic T-Shirt Collection",
                                    "subtitle" => "See all our colors",
                                    "image_url" => "https://peterssendreceiveapp.ngrok.io/img/collection.png",
                                    "buttons" => [
                                        [
                                            "title" => "View",
                                            "type" => "web_url",
                                            "url" => "https://peterssendreceiveapp.ngrok.io/collection",
                                            "messenger_extensions" => true,
                                            "webview_height_ratio" => "tall",
                                            "fallback_url" => "https://peterssendreceiveapp.ngrok.io/"
                                        ]
                                    ]
                                ],
                                [
                                    "title" => "Classic White T-Shirt",
                                    "subtitle" => "See all our colors",
                                    "default_action" => [
                                        "type" => "web_url",
                                        "url" => "https://peterssendreceiveapp.ngrok.io/view?item=100",
                                        "messenger_extensions" => false,
                                        "webview_height_ratio" => "tall"
                                    ]
                                ],
                                [
                                    "title" => "Classic Blue T-Shirt",
                                    "image_url" => "https://peterssendreceiveapp.ngrok.io/img/blue-t-shirt.png",
                                    "subtitle" => "100% Cotton, 200% Comfortable",
                                    "default_action" => [
                                        "type" => "web_url",
                                        "url" => "https://peterssendreceiveapp.ngrok.io/view?item=101",
                                        "messenger_extensions" => true,
                                        "webview_height_ratio" => "tall",
                                        "fallback_url" => "https://peterssendreceiveapp.ngrok.io/"
                                    ],
                                    "buttons" => [
                                        [
                                            "title" => "Shop Now",
                                            "type" => "web_url",
                                            "url" => "https://peterssendreceiveapp.ngrok.io/shop?item=101",
                                            "messenger_extensions" => true,
                                            "webview_height_ratio" => "tall",
                                            "fallback_url" => "https://peterssendreceiveapp.ngrok.io/"
                                        ]
                                    ]
                                ]
                            ],
                            "buttons" => [
                                [
                                    "title" => "View More",
                                    "type" => "postback",
                                    "payload" => "payload"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;
    }
}
