<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfetionalRequest extends FormRequest
{
    public function messages()
    {
        return [
            'grado.required'          => 'El grado es requerido',
            'name.unique'             => 'El nombre ingresado no puede ser usado ya esta registrado!',
            'name.required'           => 'El nombre es requerido',
            'name.min'                => 'El nombre es muy corto se debe tener al menos 6 caracteres',
            'biography.required'      => 'El campo de biografia es requerido',
            // 'image.mimes'             => 'Pruebe cargando imagenes con extension .png .jpg .jpeg',
            // 'image.max'               => 'Archivo demasiado grande intente con otra imagen',
            // 'image.uploaded'          => 'La imagen no se puede subir el archivo es muy grande',
            'gender.required'         => 'El genero es requerido'
        ];
    }

    public function rules()
    {
        return [
            'grado'           => 'required',
            'name'            => 'required|min:2|unique:profetionals,name,' . $this->id,
            'biography'       => 'required',
            // 'image'           => 'mimes:jpg,png,jpeg|max:1048',
            'is_published'    => 'required',
            'gender'          => 'required'

        ];
    }

}
