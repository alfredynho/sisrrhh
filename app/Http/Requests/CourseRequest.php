<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    public function messages()
    {
        return [
            'title.required'        => 'El título es requerido',
            'title.unique'          => 'El título ingresado no puede ser usado ya esta registrado!',
            'title.min'             => 'El titulo es muy corto se debe tener al menos 6 caracteres',
            'title.max'             => 'El titulo es muy largo se debe tener un maximo de 150 caracteres',
            'description.min'       => 'Descripción muy corta, debe tener por lo menos 6 digitos',
            'description.required'  => 'El campo descripción es requerido',
            'is_published.required' => 'El estado del registro es requerido',
            'id_teacher'            => 'El docente es requerido',
        ];
    }

    public function rules()
    {
        return [
            'title'        => 'required|min:3|max:150|unique:courses,title,'.$this->id,
            'description'  => 'required|min:3',
            'slug'         => 'unique:courses',
            'is_published' => 'required',
            'type'         => 'required',
            'id_teacher'   => 'required'
        ];
    }
}
