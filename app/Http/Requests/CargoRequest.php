<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CargoRequest extends FormRequest
{
    public function messages()
    {
        return [
            'nombre.required'    => 'El nombre es requerido',
            'codigo.required'    => 'El codigo es requerido',
            'estado.required'    => 'El estado del registro es requerido',
            'id_unidad.required' => 'La unidad es requerida',
        ];
    }

    public function rules()
    {
        return [
            'codigo'    => 'required',
            'nombre'    => 'required',
            'estado'    => 'required',
            'id_unidad' => 'required'
        ];
    }
}
