<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NormativaRequest extends FormRequest
{
    public function messages()
    {
        return [
            'title.required' => 'El titulo es requerido',
            'title.unique' => 'El titulo ingresado no puede ser usado ya esta registrado!',
            'title.min' => 'El titulo es muy corto se debe tener al menos 6 dígitos',
            'file.required' => 'El documento es requerido',
            'file.mimes'  => 'Solo se aceptan documentos pdf, xls, csv',
            'file.max'    => 'El tamaño maximo es 5MB'
        ];
    }

    public function rules()
    {
        return [
            'title' => 'required|unique:normativas,title,'.$this->id,
            'file' => 'required|mimes:pdf,xlx,csv|max:5120',
        ];
    }

}
