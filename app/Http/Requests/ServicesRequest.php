<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServicesRequest extends FormRequest
{
    public function messages()
    {
        return [
            'name.required'        => 'El nombre es requerido',
            'name.unique'          => 'El nombre ingresado no puede ser usado ya esta registrado!',
            'name.min'             => 'El nombre es muy corto se debe tener al menos 6 caracteres',
            'name.max'             => 'El nombre es muy largo se debe tener un maximo de 50 caracteres',
            'description.min'       => 'Descripción muy corta, debe tener por lo menos 6 digitos',
            'description.required'  => 'El campo descripción es requerido',
            'is_published.required' => 'El estado del registro es requerido',
        ];
    }

    public function rules()
    {
        return [
            'name'        => 'required|min:6|max:50|unique:services,name,' . $this->id,
            'description'  => 'required|min:6',
            'slug'         => 'unique:services',
            'is_published' => 'required',
        ];
    }
}
