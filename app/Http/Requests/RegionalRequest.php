<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegionalRequest extends FormRequest
{
    public function messages()
    {
        return [
            'name.required'           => 'El nombre es requerido',
            'name.unique'             => 'El nombre ingresado no puede ser usado ya esta registrado!',
            'description.required'    => 'El campo descripcion es requerido',
            'in_charge.required'      => 'El encargado es requerido',
            'is_published.required'   => 'El estado es requerido',
            'latitude.required'       => 'La latitud es requerido',
            'longitude.required'      => 'La longitud es requerido'
        ];
    }

    public function rules()
    {
        return [
            'name'           => 'required|unique:regionals,name,' . $this->id,
            'description'    => 'required',
            'latitude'       => 'required',
            'longitude'      => 'required',
            'in_charge'      => 'required',
            'is_published'   => 'required',
        ];
    }
}
