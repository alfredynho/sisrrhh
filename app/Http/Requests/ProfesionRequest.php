<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfesionRequest extends FormRequest
{
    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es requerido',
            'codigo.required' => 'El codigo es requerido',
            'estado.required' => 'El estado del registro es requerido',
        ];
    }

    public function rules()
    {
        return [
            'codigo'  => 'required',
            'nombre'  => 'required',
            'estado'  => 'required',
        ];
    }
}
