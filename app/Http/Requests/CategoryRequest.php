<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    public function messages()
    {
        return [
            'name.required' => 'El nombre es requerido',
            'name.unique' => 'El nombre ingresado no puede ser usado ya esta registrado!',
            'status' => 'Estado registro es requerido',
        ];
    }

    public function rules()
    {
        return [
            'name' => 'required|unique:categorys,name,'.$this->id,
            'status' => 'required',
            'slug'  => 'unique:categorys',
        ];
    }

}
