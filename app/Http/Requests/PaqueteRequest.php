<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaqueteRequest extends FormRequest
{
    public function messages()
    {
        return [
            'title.required'        => 'El título es requerido',
            'title.unique'          => 'El título ingresado no puede ser usado ya esta registrado!',
            'title.min'             => 'El titulo es muy corto se debe tener al menos 6 caracteres',
            'title.max'             => 'El titulo es muy largo se debe tener un maximo de 50 caracteres',
            'description.min'       => 'Descripción muy corta, debe tener por lo menos 6 digitos',
            'description.required'  => 'El campo descripción es requerido',
            'services.required'     => 'El campo servicios es requerido',
            'services.min'          => 'Servicios es muy corta, debe tener por lo menos 6 digitos',
            'promotion.required'    => 'Estado promocion es requerido',
            'is_published.required' => 'El estado del registro es requerido',
        ];
    }

    public function rules()
    {
        return [
            'title'        => 'required|min:6|max:50|unique:paquetes,title,' . $this->id,
            'description'  => 'required|min:6',
            'services'     => 'required|min:6',
            'slug'         => 'unique:paquetes',
            'promotion'    => 'required',
            'is_published' => 'required',
        ];
    }
}
