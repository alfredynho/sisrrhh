<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImagesRequest extends FormRequest
{
    public function messages()
        {
            return [
                'name'            => 'El titulo es requerido',
                'name.unique'     => 'El titulo ingresado no puede ser usado ya esta registrado!',
                'name.min'        => 'El titulo es muy corto se debe tener al menos 6 dígitos',
                'category_id '    => 'La categoria es requerida',
                'is_published'    => 'El estado de la publicación es requerido',
            ];
        }

        public function rules()
        {
            return [
                'name'          => 'required|min:6|unique:images',
                'category_id'   => 'requerido',
                'is_published'  => 'requerido'
            ];
        }

}
