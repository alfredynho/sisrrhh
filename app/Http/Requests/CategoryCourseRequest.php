<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryCourseRequest extends FormRequest
{
    public function messages()
    {
        return [
            'title.required'          => 'El título es requerido',
            'title.unique'            => 'El títutlo ingresado ya fue registrado',
            'title.min'               => 'El título es muy corto se debe tener al menos 3 caracteres',
            'description.required'    => 'La descripcón es requerida',
            'status.required'         => 'El estado es requerido',
        ];
    }

    public function rules()
    {
        return [
            'title'         => 'required|min:3|unique:categorycourses,title,'.$this->id,
            'description'   => 'required',
            'status'        => 'required',
        ];
    }
}
