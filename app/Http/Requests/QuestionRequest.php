<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionRequest extends FormRequest
{
    public function messages()
    {
        return [
            'question.required'  => 'El título es requerido',
            'question.unique'    => 'La preguta ingresada no puede ser usado ya esta registrado!',
            'status'             => 'Estado registro es requerido',
        ];
    }

    public function rules()
    {
        return [
            'question' => 'required|unique:questions',
            'status'   => 'required',
        ];
    }
}
