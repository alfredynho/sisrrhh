<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContratoRequest extends FormRequest
{
    public function messages()
    {
        return [
            'tipo.required' => 'El tipo es requerido',
            'codigo.required' => 'El codigo es requerido',
            'estado.required' => 'El estado del registro es requerido',
        ];
    }

    public function rules()
    {
        return [
            'codigo' => 'required',
            'tipo'   => 'required',
            'estado' => 'required',
        ];
    }
}
