<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PackageRequest extends FormRequest
{
    public function messages()
    {
        return [
            'name.required'    => 'El nombre es requerido',
            'email.required'   => 'El email es requerido',
            'subject.required'  => 'El asunto es requerido',
            'message.required' => 'El comentario es requerido',
            'name.min'         => 'El nombre es muy corto se debe tener al menos 3 caracteres',
            'message.min'      => 'El campo comentario debe tener por lo menos 3 caracteres',
        ];
    }

    public function rules()
    {
        return [
            'name'     => 'required|min:3',
            'email'    => 'required',
            'message'  => 'required|min:3',
        ];
    }
}
