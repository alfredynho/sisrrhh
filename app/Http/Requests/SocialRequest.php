<?php

namespace App\Http\Requests\Social;

use Illuminate\Foundation\Http\FormRequest;

class SocialRequest extends FormRequest
{
    public function messages()
    {
        return [
            'name.required'    => 'El nombre es requerido',
            'name.unique'      => 'El nombre ingresado no puede ser usado ya esta registrado!',
            'url.required'     =>'El contenido de la red social es requerido',
            'status.required'  => 'Estado registro es requerido',
        ];
    }

    public function rules()
    {
        return [
            'name'      => 'required|unique:socials,name,' . $this->id,
            'url'       => 'required',
            'status'    => 'required',
        ];
    }
}
