<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Blog;
use \App\Category;
use \App\Edubot;
use \App\Models\Profesional;
use \App\Models\Convenios;
use \App\Models\Course;
use \App\Memories;
use \App\Models\Paquetes;
use \App\Models\Clients;
use \App\Models\Cities;

use DB;

use Response;

class CemticController extends Controller
{
    public function index(Request $request)
    {
        $blogs = Blog::orderBy('id','desc')->take(6)
        ->where('blogs.is_published','1')
        ->get();

        $memories = Memories::orderBy('id','desc')->take(6)
        ->where('memories.status','1')
        ->get();

        return view('index',[
            'blogs'         => $blogs,
            'memories'      => $memories,
        ]);
    }

    public function paquetes(Request $request)
    {
        $title_page = "Paquetes de Servicios";

        $paquetes = Paquetes::where('is_published','1')->orderBy('id', 'desc')->simplePaginate(6);

        return view('frontend.pages.paquetes', [
            'title_page' => $title_page,
            'paquetes'   => $paquetes
        ]);
    }

    public function gallery(Request $request)
    {
        $title_page = "Galeria de imagenes";

        return view('frontend.pages.gallery', [
            'title_page' => $title_page,
        ]);
    }

    public function rrhh(Request $request)
    {
        $title_page = "Recursos Humanos";

        return view('frontend.pages.rrhh', [
            'title_page' => $title_page,
        ]);
    }

    public function faqs(Request $request)
    {
        $title_page = "Categoria preguntas Frecuentes";

        $faqs = Category::where('status',1)->get();

        return view('frontend.pages.faqs', [
            'title_page' => $title_page,
            'faqs'       => $faqs
        ]);
    }

    public function calendar(Request $request)
    {
        $title_page = "Calendario Cemtic";

        $faqs = Category::where('status',1)->get();

        return view('frontend.pages.calendar', [
            'title_page' => $title_page,
            'faqs'       => $faqs
        ]);
    }

    public function detail_category_frontend($slug)
    {
        $title_page = "Preguntas Frecuentes";

        $dt_cqa = Category::where('slug', '=', $slug)->firstOrFail();

        $questions = Edubot::join('categorys as ct', 'questions.action_id', '=', 'ct.id')
            ->select('questions.*')
            ->where('questions.action_id', $dt_cqa->id)
            ->get();

        return view('frontend.pages.faqs_detail', [
             'questions' => $questions,
             'categoria' => $dt_cqa,
             'title_page' => $title_page
         ]);
    }

    public function detail_paquete($slug)
    {
        $meta_property = true;

        $paquete = Paquetes::where('slug','=', $slug)->firstOrFail();
        $paquetes = Paquetes::where('is_published',1)->take(3)->get();

        $title_page = "Paquete CEMTIC";

        return view('frontend.pages.detalle_paquete',[
            'paquete' =>$paquete,
            'meta_property' => $meta_property,
            'paquetes' =>$paquetes,
            'title_page' => $title_page
        ]);
    }

    public function contactoscemtic(Request $request)
    {
        $title_page = "Contactos";

        return view('frontend.pages.contactos', [
            'title_page' => $title_page
        ]);
    }

    public function servicios(Request $request)
    {
        $title_page = "Servicios CEMTIC";

        $servicios = Paquetes::where('is_published','1')->orderBy('id', 'desc')->simplePaginate(6);

        return view('frontend.pages.servicios', [
            'servicios'  => $servicios,
            'title_page' => $title_page
        ]);
    }

    public function detail_service($slug)
    {
        $meta_property = true;

        $post = Paquetes::where('slug','=', $slug)->firstOrFail();
        
        $servicios = Paquetes::where('is_published',1)->take(4)->get();

        $title_page = "Servicio CEMTIC";

        return view('frontend.pages.detail_services',[
            'post' =>$post,
            'servicios'  => $servicios,
            'meta_property' => $meta_property,
            'title_page' => $title_page
        ]);
    }

    public function convenios(Request $request)
    {
        $title_page = "Convenios CEMTIC";

        $convenios = Convenios::orderBy('id','desc')
        ->where('convenios.is_published','1')
        ->get();

        return view('frontend.pages.convenios', [
            'title_page'  => $title_page,
            'convenios'   => $convenios
        ]);
    }

    public function download(Request $request)
    {
        $year_services = date("Y")-1944;

        $data=array('menu'=>'providers','title_template'=>'CEMTIC');

        return view('frontend.pages.descargas', $data)->with(compact('year_services'));
    }

    public function detail_new($slug)
    {
        $meta_property = true;

        $post = Blog::where('slug','=', $slug)->firstOrFail();
        $blogs = Blog::where('is_published',1)->take(4)->get();

        $title_page = "Publicación CEMTIC";

        return view('frontend.pages.detalle_blog',[
            'post' =>$post,
            'meta_property' => $meta_property,
            'blogs' =>$blogs,
            'title_page' => $title_page
        ]);
    }

    public function detalle_convenio($slug)
    {
        $meta_property = true;

        $convenio = Convenios::where('slug','=', $slug)->firstOrFail();
        $convenios = Convenios::where('is_published',1)->take(4)->get();

        $title_page = "Convenio CEMTIC";

        return view('frontend.pages.detalle_convenio',[
            'convenio' =>$convenio,
            'meta_property' => $meta_property,
            'convenios' =>$convenios,
            'title_page' => $title_page
        ]);
    }

    public function blogs(Request $request)
    {
        $title_page = "Publicaciones";

        $blog = Blog::where('is_published','1')->where('category','BLOG')->paginate(6);
        $blogs_list = Blog::where('is_published','1')->take(6)->get();
        $c_anuncio = Blog::where('category','=','ANUNCIO')->count();
        $c_blog = Blog::where('category','=','BLOG')->count();

        return view('frontend.pages.blogs',[
            'blog' => $blog,
            'blogs_list' => $blogs_list,
            'title_page' => $title_page
            ]);
    }


    public function cemtic(Request $request)
    {
        $title_page = "Cemtic";

        $profesionales = Profesional::orderBy('id', 'desc')
        ->where('profetionals.is_published','=','1')->take(4)
        ->get();

        $memories = Memories::orderBy('id','desc')->take(6)
        ->where('memories.status','1')
        ->get();

        $blog = Blog::where('is_published','1')->where('category','BLOG')->paginate(6);

        return view('frontend.pages.cemtic',[
            'blog' => $blog,
            'title_page' => $title_page,
            'profesionales' => $profesionales,
            'memories' => $memories
        ]);
    }

    public function events(Request $request)
    {
        $title_page = "eventos";

        $blog = Blog::where('is_published','1')->where('category','BLOG')->paginate(6);

        return view('frontend.pages.events',[
            'blog' => $blog,
            'title_page' => $title_page
            ]);
    }

    public function detail_blogs($slug)
    {
        $meta_property = true;

        $post = Blog::where('slug','=', $slug)->firstOrFail();
        $blog = Blog::where('is_published',1)->where('highlight',1)->get();

        return view('frontend.pages.detalle',[
            'post' =>$post,
            'meta_property' => $meta_property,
            'blog' =>$blog
        ]);
    }

    public function medicinatrabajo(Request $request)
    {
        $title_page = "Servicios Medicina del Trabajo";

        return view('frontend.pages.medicina_trabajo', [
            'title_page' => $title_page
        ]);
    }


    public function profesionales(Request $request)
    {
        $title_page = "Profesionales";
        $profesionales = Profesional::where('is_published','1')->simplePaginate(8);

        return view('frontend.pages.profetionals', [
            'title_page'     => $title_page,
            'profesionales'  => $profesionales
        ]);
    }


    public function formulario_docentes(Request $request)
    {
        $title_page = "Formulario Docentes Cemtic";

        return view('frontend.pages.form_teacher', [
            'title_page' => $title_page,
        ]);
    }


    public function detail_profesional($slug)
    {
        $title_page = "Detalle Profesionales";
        $dt_profesional = Profesional::where('slug', '=', $slug)->firstOrFail();

        return view('frontend.pages.detail_profetional', [
            'dt_profesional' => $dt_profesional,
            'title_page'     => $title_page
        ]);
    }

    public function courses(Request $request)
    {
        $title_page = "Cursos";

        $blog = Blog::where('is_published','1')->where('category','BLOG')->take(5)->get();
        $courses = Course::where('is_published','1')->orderBy('id', 'desc')->get();

        return view('frontend.pages.courses', [
            'title_page'  => $title_page,
            'courses'     => $courses,
            'blog'        => $blog
        ]);
    }

    public function clients(Request $request)
    {
        $title_page = "Clientes CEMTIC";

        $clients = Clients::where('is_published','1')->where('is_published','1')->get();

        return view('frontend.pages.clients', [
            'title_page'  => $title_page,
            'clients'        => $clients
        ]);
    }


    public function detail_client($slug)
    {
        $title_page = "Cliente";

        $dt_client = Clients::where('slug', '=', $slug)->firstOrFail();

        return view('frontend.pages.detail_client', [
            'dt_client' => $dt_client,
            'title_page'     => $title_page,
        ]);
    }


    public function detail_course($slug)
    {
        $title_page = "Cursos";

        $dt_course = DB::table('courses')
        ->where('courses.slug','=',$slug)
        ->join('profetionals','courses.id_teacher','=','profetionals.id')
        ->select('courses.*','profetionals.name as nombre_docente','profetionals.biography as biography_profetional','profetionals.grado', 'profetionals.image as imagen_docente', 'profetionals.slug as slug_profetional')
        ->first();

        $courses = Course::where('is_published','1')->orderBy('id', 'asc')->take(6)->get();

        //$dt_course = Course::where('slug', '=', $slug)->firstOrFail();

        return view('frontend.pages.detail_course', [
            'dt_course' => $dt_course,
            'title_page'     => $title_page,
            'courses'    => $courses
        ]);
    }


    public function downloadDocuments($file_download)
    {
        $file= storage_path('app/public/Documents/'.$file_download);

        $headers = array(
                'Content-Type: application/pdf',
                );

        return Response::download($file, $file_download, $headers);
    }

    public function citys_index()
    {
        $cities = Cities::whereHas('countries', function($query) {
            $query->whereId(request()->input('countries_id', 0));
        })
        ->pluck('name', 'id');

        return response()->json($cities);

        dump($cities);
    }

}
