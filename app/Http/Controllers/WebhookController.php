<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Action;
use \App\Question;
use \App\Messenger;
use \App\Bot\Facebook\Message;
use \App\Bot\Facebook\Card;
use \App\Bot\Facebook\User;
use \App\Bot\Facebook\Button;
use \App\Bot\Facebook\Images;
use \App\Models\Course;
use \App\Models\Paquetes;


use DialogFlow\Client;

class WebhookController extends Controller
{
    public function getWebhook(Request $request)
    {
        if ($request->get('hub_mode') == 'subscribe' and $request->get('hub_verify_token') === env('HUB_VERIFY_TOKEN')) {
            return response($request->get('hub_challenge'));
        }
        return response('Error, verify token doesn\'t match', 400);
    }

    public function postWebhook(Request $request)
    {
        $content = json_decode($request->getContent(), true);

        $jess = $content['entry'][0]['messaging'] ? $content['entry'][0]['messaging'] : null;
        $sender = $jess[0]['sender']['id'];

        // Texto de tipo Mensaje
        if (isset($jess[0]['message']))
        {
            //dump("message");
        }

        // Texto de tipo Postback
        elseif(isset($jess[0]['postback']))
        {
            //dump("postback");
            $has_postback = $jess[0]['postback']['payload'];

            if($has_postback == 'BOT_CEMTIC_START')
            {
                $rqhome = $request->root();
                $response = Message::typing($sender);
                $_user = User::getUser($sender);
                $response = Message::sendToFbMessenger(
                    $sender,
                    'Hola soy un asistente 😀 , puedo ayudarte a aclarar dudas que tengas respecto al CEMTIC ...');
                //$response = Card::genericTemplate($sender,$rqhome);

            }

            if($has_postback == 'CREDITOS_BOT')
            {
                $response = Message::typing($sender);
                $response = Message::sendToFbMessenger($sender, "CEMTIC EMPRESA es mi creador 😀  ");
            }

            if ($has_postback == 'BOT_AYUDA') {
                $response = Message::typing($sender);
                $response = Message::sendToFbMessenger($sender, "En que puedo ayudarte ???");
            }


        }
        else
        {

            //Algun otro Tipo de Evento
            // dump("otro");
        }

        $postArray = isset($content['entry'][0]['messaging']) ? $content['entry'][0]['messaging'] : null;

        $response = [];
        $has_message = false;
        $is_echo = true;

        if (!is_null($postArray)) {
            $sender = $postArray[0]['sender']['id'];
            $has_message = isset($postArray[0]['message']['text']);

            $is_echo = isset($postArray[0]['message']['is_echo']);
        }
        if ($has_message && !$is_echo) {

            $reply = $postArray[0]['message']['text'];

            try {
                $client = new Client(env('DIALOGFLOW_CLIENT'));

                $query = $client->get('query', [
                    'query' => $reply,
                    'sessionId' => time(),
                ]);

                $response = json_decode((string) $query->getBody(), true);

                $speech = $response['result']['fulfillment']['speech'] ?: json_encode($response['result']['fulfillment']['messages'][0]['payload']);

                $_action = $response['result']['action'];
            } catch (\Exception $error) {
                error_log("un error");
            }

            if (1 > 110) {
                foreach ($array_actions as $key => $value) {
                    if ($value == $_action) {
                        $dialog = $this->searchQuestions($key);
                        $response = $this->typing($sender);
                        $response = $this->sendToFbMessenger($sender, $dialog);
                    }
                }
            } else {

                try {

                    $_user = User::getUser($sender);
                    $response = Message::typing($sender);
                    $response = Message::sendToFbMessenger($sender, $speech);


                    if($_action == 'CURSOS_CEMTIC'){

                        $courses = Course::orderBy('id', 'desc')->take(8)
                        ->where('courses.is_published', '=', '1')
                        ->get();

                        dump(count($courses));

                        $response = Message::sendToFbMessenger($sender, 'Contamos con los siguientes cursos para ti 🎓 ');
                        $rqhome   = $request->root();
                        $response = Card::genericTemplate($sender,$rqhome, $courses, 'curso');
                    }

                    if($_action == 'CURSOS_CEMTIC_NUEVOS'){

                        $courses = Course::orderBy('id', 'desc')->take(8)
                        ->where('courses.is_published', '=', '1')
                        ->where('courses.destacado','=','1')
                        ->get();

                        $response = Message::sendToFbMessenger($sender, 'Contamos con los siguientes cursos para ti 🎓 ');
                        $rqhome = $request->root();
                        $response = Card::genericTemplate($sender,$rqhome, $courses, 'curso');
                    }

                    if($_action == 'PAQUETES_SERVICIOS'){

                        $paquetes = Paquetes::orderBy('id', 'desc')->take(8)
                        ->where('paquetes.is_published', '=', '1')
                        ->get();

                        $response = Message::sendToFbMessenger($sender,  'Estos son nuestros paquetes: ');
                        $rqhome = $request->root();
                        $response = Card::genericTemplate($sender,$rqhome, $paquetes, 'paquete');
                    }

                    if($_action == 'CEMTIC_INFO'){
                        $response = Message::sendImage($sender);
                    }


                    if($_action == 'CEMTIC_SUCURSALES'){
                        $response = Message::sendImage($sender);
                    }

                } catch (\Exception $error) {
                    error_log("error en usuarios/o mensajes ...");
                }

            }
        }
        return response($response, 200);
    }


    private function searchQuestions($key)
    {
        $questions = Question::where('questions.status', '1')
            ->select('questions.*')
            ->get();

        $array_questions = array();

        foreach ($questions as $question) {
            $key = $question->id;
            $value = $question->answer;

            $array_questions[(string) $key] = $value;
        }

        $responses = "";
        foreach ($array_questions as $_key => $_value) {
            if ($_key == $key) {
                $responses = $_value;
            }
        }

        return $responses;
    }
}
