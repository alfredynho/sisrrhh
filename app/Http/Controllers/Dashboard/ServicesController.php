<?php

namespace App\Http\Controllers\Dashboard;

use Session;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Models\Services;
use App\Http\Requests\ServicesRequest;


class ServicesController extends Controller
{
    public function index(Request $request)
    {
        $services = Services::orderBy('id','desc')->get();

        return view('dashboard.pages.services.index', [
            'services' => $services,
        ]);
    }

    public function store(ServicesRequest $request)
    {
        $services = new Services();

        $services->name = $request->get('name');
        $services->description = $request->get('description');

        $services->creador = auth()->user()->id;
        $services->slug = hSlug($request->name);

        $services->is_published = $request->get('is_published');
        $services->save();

        Session::flash('create','Registro creado con Éxito');
        return Redirect()->route('dashboard.services.index');
    }


    public function update(ServicesRequest $request, $id)
    {
        $services = services::findOrFail($id);

        $services->name = $request->input('name');
        $services->description = $request->input('description');

        $services->creador = auth()->user()->id;
        $services->slug = hSlug($request->name);

        $services->is_published = $request->input('is_published');

        $services->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.services.index');
    }


    public function show($id)
    {
        $services = Services::where('id', '=', $id)->firstOrFail();

        return view('dashboard.pages.services.show', [
            'services' => $services
        ]);
    }


    public function create(Request $request)
    {
        $message = "Crear Servicio";

        return view('dashboard.pages.services.create', [
            'message' => $message
        ]);
    }


	public function destroy($id)
    {
        $services = Services::findOrFail($id);

        Session::flash('delete','Registro '.$services->name.' eliminado con Éxito');

        $services->delete();

        return Redirect()->route('dashboard.services.index');
    }
}
