<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Unidad;
use App\Models\Estudios;
use App\Models\Personal;
use App\Models\Profesion;
use DB;
use Session;

class EstudiosController extends Controller
{
    public function index()
    {
        $personal = Personal::all();
        $profesion = Profesion::all();


        $estudios = DB::table('estudios')
        ->join('profesiones','estudios.id_personal','=','profesiones.id')
        ->select('estudios.*','profesiones.nombre as nombre_profesion')
        ->get();

        // $personal = DB::table('personal')
        // ->join('estudios','personal.id','=','estudios.id')
        // ->select('estudios.*','profesiones.nombre as nombre_profesion')
        // ->get();


        return view('dashboard.pages.estudios.index', [
            'estudios' => $estudios,
            'personal' => $personal,
            'profesion' => $profesion
        ]);
    }

    public function store(request $request)
    {
        $estudios = new Estudios();

        $estudios->institucion = strtoupper($request->get('institucion'));
        $estudios->mencion = strtoupper($request->get('mencion'));
        $estudios->grado = $request->get('grado');
        $estudios->profesion = $request->get('profesion');
        $estudios->id_personal = $request->get('id_personal');
        $estudios->save();

        Session::flash('create', 'Registro creado con Éxito');
        return Redirect()->route('dashboard.estudios.index');
    }

    public function update(request $request, $id)
    {
        $unidades = Unidad::findOrFail($id);

        $unidades->codigo = strtoupper($request->input('codigo'));
        $unidades->nombre = strtoupper($request->input('nombre'));
        $unidades->estado = $request->input('estado');
        $unidades->update();

        Session::flash('update', 'Registro modificado con Éxito');
        return Redirect()->route('dashboard.estudios.index');
    }

    public function destroy($id)
    {
        $unidades = Unidad::findOrFail($id);

        Session::flash('delete', 'Registro ' . $unidades->nombre . ' eliminado con Éxito');

        $unidades->delete();

        return Redirect()->route('dashboard.estudios.index');
    }
}