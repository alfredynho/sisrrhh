<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Agreements;
use Session;

use App\Http\Requests\AgreementsRequest;
use Illuminate\Support\Facades\Storage;


class AgreementsController extends Controller
{
    public function index(Request $request)
    {
        $agreements = Agreements::orderBy('id','desc')->get();

        return view('dashboard.pages.agreements.index', [
            'agreements' => $agreements,
        ]);
    }


    public function store(AgreementsRequest $request)
    {
        $agreements = new Agreements();

        $agreements->title = $request->get('title');
        $agreements->content = $request->get('content');

        $img = $request->file('image');
        $code = generaCode();

        if($img!=null)
        {
            Storage::disk('public')->delete('/Agreements/'.$gallery->image);

            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($code.".".$extension);
            \Storage::disk('local')->put('public/Agreements/'.$archivo, \File::get($img));

            $webPName = $code.".webp";

            //comprimimos la imagen a formato webp
            //formato cwebp -q 100 image1.jpg -o image1.webp >> en linux
            exec("cwebp -q 100 ".storage_path("/app/public/Agreements/".$archivo)." -o ".storage_path("/app/public/Agreements/".$webPName));

            $gallery->image = $webPName;

            Storage::disk('public')->delete('/Agreements/'.$archivo);

        }

        $agreements->creador = auth()->user()->id;
        $_slug = mb_strtolower((str_replace(" ","-",$request->title)),'UTF-8');
        $agreements->slug = preg_replace('/[^A-Za-z0-9\-]/', '', $_slug);

        $agreements->is_published = $request->get('status');
        $agreements->save();

        Session::flash('create','Registro creado con Éxito');
        return Redirect()->route('dashboard.agreements.index');
    }


    public function update(AgreementsRequest $request, $id)
    {
        $agreements = Agreements::findOrFail($id);

        $agreements->title = $request->input('title');
        $agreements->content = $request->input('content');
    
        $img = $request->file('image');
        $code = generaCode();

        if($img!=null)
        {
            Storage::disk('public')->delete('/Agreements/'.$agreements->image);

            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($code.".".$extension);
            \Storage::disk('local')->put('public/Agreements/'.$archivo, \File::get($img));

            $webPName = $code.".webp";

            //comprimimos la imagen a formato webp
            //formato cwebp -q 100 image1.jpg -o image1.webp >> en linux
            exec("cwebp -q 100 ".storage_path("/app/public/Agreements/".$archivo)." -o ".storage_path("/app/public/Agreements/".$webPName));

            $agreements->image = $webPName;

            Storage::disk('public')->delete('/Agreements/'.$archivo);

        }

        $agreements->creador = auth()->user()->id;
        $_slug = mb_strtolower((str_replace(" ","-",$request->title)),'UTF-8');
        $agreements->slug = preg_replace('/[^A-Za-z0-9\-]/', '', $_slug);

        $agreements->is_published = $request->input('status');

        $agreements->update();

        Session::flash('update','Registro modificado con Éxito');

        return Redirect()->route('dashboard.agreements.index');
    }


	public function destroy($id)
    {
        $_agreements = Agreements::findOrFail($id);

        Session::flash('delete','Registro '.$_agreements->title.' eliminado con Éxito');

        $_agreements->delete();

        return Redirect()->route('dashboard.agreements.index');
    }
}
