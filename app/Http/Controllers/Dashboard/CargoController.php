<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cargo;
use App\Models\Unidad;
use Session;
use DB;
use PDF;

use App\Http\Requests\CargoRequest;

class CargoController extends Controller
{
    public function index()
    {
        // $cargos = Cargo::where('estado','=',1)->get();
        $unidades = Unidad::where('estado','=',1)->get();
        $cargos = DB::table('cargo')
        ->join('unidad','cargo.id_unidad','=','unidad.id')
        ->select('cargo.*','unidad.nombre as nombre_unidad')
        ->orderBy('cargo.id','desc')->get();


        // $courses = DB::table('courses')
        // ->join('profetionals','courses.id_teacher','=','profetionals.id')
        // ->select('courses.*','profetionals.name as nombre_docente')
        // ->orderBy('courses.id','desc')->get();

        return view('dashboard.pages.cargo.index', [
            'cargos' => $cargos,
            'unidades' => $unidades
        ]);
    }

    public function store(CargoRequest $request)
    {
        $cargos = new Cargo();

        $cargos->codigo = strtoupper($request->get('codigo'));
        $cargos->nombre = strtoupper($request->get('nombre'));
        $cargos->id_unidad = strtoupper($request->get('id_unidad'));

        $cargos->estado = $request->get('estado');
        $cargos->save();

        Session::flash('create', 'Registro creado con Éxito');
        return Redirect()->route('dashboard.cargo.index');
    }


    public function update(CargoRequest $request, $id)
    {
        $cargos = Cargo::findOrFail($id);

        $cargos->codigo = strtoupper($request->input('codigo'));
        $cargos->nombre = strtoupper($request->input('nombre'));
        $cargos->estado = $request->input('estado');
        $cargos->id_unidad = $request->input('id_unidad');

        $cargos->update();

        Session::flash('update', 'Registro modificado con Éxito');
        return Redirect()->route('dashboard.cargo.index');
    }

    public function destroy($id)
    {
        $cargos = Cargo::findOrFail($id);

        Session::flash('delete', 'Registro ' . $cargos->nombre . ' eliminado con Éxito');

        $cargos->delete();

        return Redirect()->route('dashboard.cargo.index');
    }

    public function reporte(Request $request){

        $cargos = DB::table('cargo')
        ->join('unidad','cargo.id','=','unidad.id')
        ->select('cargo.*','unidad.nombre as nombre_unidad')
        ->orderBy('cargo.id','desc')->get();

        $pdf = PDF::loadView('dashboard.pages.cargo.boleta',compact('cargos'));
        // ->setPaper('a4', 'landscape');
        // ->setPaper('a4', 'landscape');
        return $pdf->download('cargo.pdf');
    }
}