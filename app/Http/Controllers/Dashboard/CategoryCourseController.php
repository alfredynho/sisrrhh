<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\CategoryCourse;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryCourseRequest;
use App\Http\Controllers\Controller;
use Session;


class CategoryCourseController extends Controller
{
    public function index()
    {
        $c_course = CategoryCourse::where('status','=',1)->get();

        return view('dashboard.pages.ccourse.index', [
            'c_course'  => $c_course
        ]);
    }

    public function store(CategoryCourseRequest $request)
    {

        $c_course = new CategoryCourse();

        $c_course->title = $request->get('title');
        $c_course->description = $request->get('description');
        $c_course->status = $request->get('status');
        $c_course->save();

        Session::flash('create', 'Registro creado con Éxito');
        return Redirect()->route('dashboard.ccourse.index');
    }


    public function update(CategoryCourseRequest $request, $id)
    {
        $c_course = CategoryCourse::findOrFail($id);

        $c_course->title = $request->input('title');
        $c_course->description = $request->input('description');
        $c_course->status = $request->input('status');
        $c_course->update();

        Session::flash('update', 'Registro modificado con Éxito');
        return Redirect()->route('dashboard.ccourse.index');
    }

    public function destroy($id)
    {
        $c_course = CategoryCourse::findOrFail($id);

        Session::flash('delete', 'Registro ' . $c_course->title . ' eliminado con Éxito');

        $c_course->delete();

        return Redirect()->route('dashboard.ccourse.index');
    }
}
