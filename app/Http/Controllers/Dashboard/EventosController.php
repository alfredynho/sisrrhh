<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Events;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\BlogRequest;

use Session;


class EventosController extends Controller
{
    public function index(Request $request)
    {
        $events = Events::orderBy('id','desc')->get();

        return view('dashboard.pages.events.index', [
            'events' => $events,
        ]);
    }

    public function store(BlogRequest $request)
    {
        $blog = new Events();

        $blog->title = $request->get('title');
        $blog->description = $request->get('description');
        $_imgCategory = $blog->category = $request->get('category');

        $blog->creador = auth()->user()->id;
        $blog->slug = hSlug($request->title);

        $blog->is_published = $request->get('is_published');
        $blog->destacado = $request->get('destacado');
        $blog->save();

        Session::flash('create','Registro creado con Éxito');
        return Redirect()->route('dashboard.blog.index');
    }


    public function update(BlogRequest $request, $id)
    {
        $blog = Blog::findOrFail($id);

        $blog->title = $request->input('title');
        $blog->description = $request->input('description');
        $_imgCategory = $blog->category = $request->input('category');

        $blog->creador = auth()->user()->id;
        $blog->slug = hSlug($request->title);

        $blog->is_published = $request->input('is_published');
        $blog->destacado = $request->input('destacado');

        $blog->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.blog.index');
    }


    public function show($id)
    {
        $blog = Blog::where('id', '=', $id)->firstOrFail();

        return view('dashboard.pages.blog.show', [
            'blog' => $blog
        ]);
    }


    public function create(Request $request)
    {
        $message = "Crear Blog";

        return view('dashboard.pages.blog.create', [
            'message' => $message
        ]);
    }


	public function destroy($id)
    {
        $_blog = Blog::findOrFail($id);

        Session::flash('delete','Registro '.$_blog->title.' eliminado con Éxito');

        $_blog->delete();

        return Redirect()->route('dashboard.blog.index');
    }

}
