<?php

namespace App\Http\Controllers\Dashboard;

use Session;

use App\Social;
use App\Http\Controllers\Controller;
use App\Http\Requests\SocialsRequest;

class SocialController extends Controller
{
    public function index()
    {
        $socials = Social::orderBy('id','desc')->get();

        return view('dashboard.pages.social.index', [
            'socials' => $socials
        ]);
    }

    public function update(SocialsRequest $request, $id)
    {
        $socials = Social::findOrFail($id);

        $_name = strtoupper(str_replace(' ','_',$request->input('name')));

        $socials->name = $_name;
        $socials->url = $request->input('url');

        $socials->status = $request->input('status');
        $socials->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.social.index');

    }
}
