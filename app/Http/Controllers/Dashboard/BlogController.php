<?php
namespace App\Http\Controllers\Dashboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

use App\Blog;
use App\Http\Requests\BlogRequest;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $anuncio = Blog::orderBy('id','desc')->get();

        return view('dashboard.pages.blog.index', [
            'anuncio' => $anuncio,
        ]);
    }

    public function store(BlogRequest $request)
    {
        $anuncio = new Blog();

        $anuncio->title = $request->get('title');
        $anuncio->description = $request->get('description');

        $anuncio->creador = auth()->user()->id;
        $anuncio->slug = hSlug($request->title);
        $anuncio->save();

        Session::flash('create','Registro creado con Éxito');
        return Redirect()->route('dashboard.blog.index');
    }


    public function update(BlogRequest $request, $id)
    {
        $anuncio = Blog::findOrFail($id);

        $anuncio->title = $request->input('title');
        $anuncio->description = $request->input('description');

        $anuncio->creador = auth()->user()->id;
        $anuncio->slug = hSlug($request->title);
        $anuncio->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.blog.index');
    }


    public function show($id)
    {
        $message = "Modificar Registro";
        $anuncio = Blog::where('id', '=', $id)->firstOrFail();

        return view('dashboard.pages.blog.show', [
            'anuncio' => $anuncio,
            'message' => $message,
        ]);
    }

    public function create(Request $request)
    {
        $message = "Crear Anuncio";

        return view('dashboard.pages.blog.create', [
            'message' => $message
        ]);
    }

	public function destroy($id)
    {
        $anuncio = Blog::findOrFail($id);

        Session::flash('delete','Registro '.$anuncio->title.' eliminado con Éxito');

        $anuncio->delete();

        return Redirect()->route('dashboard.blog.index');
    }

}
