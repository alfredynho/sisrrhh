<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Profesion;
use Session;

use App\Http\Requests\ProfesionRequest;

class ProfesionesController extends Controller
{
    public function index()
    {
        $profesiones = Profesion::where('estado','=',1)->get();

        return view('dashboard.pages.profesion.index', [
            'profesiones' => $profesiones,
        ]);
    }

    public function store(ProfesionRequest $request)
    {
        $profesiones = new Profesion();

        $profesiones->codigo = strtoupper($request->get('codigo'));
        $profesiones->nombre = strtoupper($request->get('nombre'));

        $profesiones->estado = $request->get('estado');
        $profesiones->save();

        Session::flash('create', 'Registro creado con Éxito');
        return Redirect()->route('dashboard.profesion.index');
    }


    public function update(Request $request, $id)
    {
        $profesiones = Profesion::findOrFail($id);

        $profesiones->codigo = strtoupper($request->input('codigo'));
        $profesiones->nombre = strtoupper($request->input('nombre'));
        $profesiones->estado = $request->input('estado');

        $profesiones->update();

        Session::flash('update', 'Registro modificado con Éxito');
        return Redirect()->route('dashboard.profesion.index');
    }

    public function destroy($id)
    {
        $profesiones = Profesion::findOrFail($id);

        Session::flash('delete', 'Registro ' . $profesiones->nombre . ' eliminado con Éxito');

        $profesiones->delete();

        return Redirect()->route('dashboard.profesion.index');
    }
}
