<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Course;
use App\Models\Profesional;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\CourseRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use DB;

use Session;

class CourseController extends Controller
{
    public function index(Request $request)
    {
        $courses = DB::table('courses')
        ->join('profetionals','courses.id_teacher','=','profetionals.id')
        ->select('courses.*','profetionals.name as nombre_docente')
        ->orderBy('courses.id','desc')->get();

        return view('dashboard.pages.course.index', [
            'courses' => $courses
        ]);
    }

    public function store(CourseRequest $request)
    {
        $course = new Course();
        $course->title = strtoupper($request->get('title'));
        $course->description = $request->get('description');
        $course->content = $request->get('content');
        $course->type = $request->get('type');

        $course->form = $request->get('form');
        $course->destacado = $request->get('destacado');

        $course->date = $request->get('date');
        $course->is_new = $request->get('is_new');

        $course->author = auth()->user()->id;
        $course->id_teacher = $request->get('id_teacher');
        $course->slug = hSlug($request->title);

        $course->image = $request->get('image');

        $course->is_published = $request->get('is_published');

        $course->numparticipants = $request->get('numparticipants');
        $course->save();

        Session::flash('create','Registro creado con Éxito');
        return Redirect()->route('dashboard.course.index');
    }

    public function create(Request $request)
    {
        $title = "Nuevo Curso";

        $profetionals = Profesional::where('is_published','1')->orderBy('id','desc')->get();

        return view('dashboard.pages.course.create', [
            'title' => $title,
            'profetionals' => $profetionals
        ]);
    }


    public function show($slug)
    {
        $title = "Modificar Curso";

        $course = Course::where('slug', '=', $slug)->firstOrFail();
        $profetionals = Profesional::where('is_published','1')->orderBy('id','desc')->get();

        return view('dashboard.pages.course.edit', [
            'course'       => $course,
            'title'        => $title,
            'profetionals' => $profetionals
        ]);

    }

    public function update(CourseRequest $request, $id)
    {
        $course = Course::findOrFail($id);

        $course->title = strtoupper($request->input('title'));
        $course->content = $request->input('content');
        $course->description = $request->input('description');
        $course->id_teacher = $request->input('id_teacher');

        $course->date = $request->input('date');
        $course->is_new = $request->input('is_new');

        $course->author = auth()->user()->id;
        $course->type = $request->input('type');
        $course->form = $request->input('form');
        $course->destacado = $request->input('destacado');

        $_slug = mb_strtolower((str_replace(" ","-",$request->title)),'UTF-8');
        $course->slug = preg_replace('/[^A-Za-z0-9\-]/', '', $_slug);

        $course->image = $request->input('image');

        $course->is_published = $request->input('is_published');

        $course->numparticipants = $request->input('numparticipants');
        $course->duration = $request->input('duration');
        $course->level = $request->input('level');
        $course->certification = $request->input('certification');
        $course->price = $request->input('price');
        $course->modules = $request->input('modules');

        $course->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.course.index');
    }


	public function destroy($id)
    {
        $course = Course::findOrFail($id);

        Session::flash('delete','Registro '.$course->title.' eliminado con Éxito');

        $course->delete();

        return Redirect()->route('dashboard.course.index');
    }
}
