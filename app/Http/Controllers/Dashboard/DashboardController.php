<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Profesion;
use App\Models\Profesional;
use App\Models\Unidad;
use App\Models\Cargo;
use App\Models\Personal;
use DB;


class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $cargos = Cargo::all();
        $personal = Personal::all();
        $unidades = Unidad::all();
        $profesiones = Profesion::all();

        $_cargos = DB::table('cargo')
        ->join('unidad','cargo.id_unidad','=','unidad.id')
        ->select('cargo.*','unidad.nombre as nombre_unidad')
        ->orderBy('cargo.id','desc')->take(15)->get();


        return view('dashboard', [
            'unidades' => $unidades,
            'personal' => $personal,
            'profesiones' => $profesiones,
            'cargos' => $cargos,
            '_cargos' => $_cargos,
        ]);
        

    }
}
