<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ClientRequest;
use App\Models\Clients;
use Session;

class ClientsController extends Controller
{
    public function index(Request $request)
    {
        $clients = Clients::orderBy('id','desc')->get();

        return view('dashboard.pages.clients.index', [
            'clients' => $clients
        ]);
    }

    public function store(ClientRequest $request)
    {
        $clients = new Clients();
        $clients->name = strtoupper($request->get('name'));
        $clients->description = $request->get('description');
        $clients->message = $request->get('message');
        $clients->destacado = $request->get('destacado');

        $clients->creador = auth()->user()->id;
        $clients->slug = hSlug($request->name);

        $clients->image = $request->get('image');

        $clients->is_published = $request->get('is_published');
        $clients->save();

        Session::flash('create','Registro creado con Éxito');
        return Redirect()->route('dashboard.clients.index');
    }

    public function create(Request $request)
    {
        $title = "Nuevo Cliente";

        $clients = Clients::where('is_published','1')->orderBy('id','desc')->get();

        return view('dashboard.pages.clients.create', [
            'title' => $title,
            'clients' => $clients
        ]);
    }

    public function show($slug)
    {
        $title = "Modificar Cliente";

        $clients = Clients::where('slug', '=', $slug)->firstOrFail();
        $profetionals = Clients::where('is_published','1')->orderBy('id','desc')->get();

        return view('dashboard.pages.clients.edit', [
            'clients'       => $clients,
            'title'         => $title,
            'profetionals'  => $profetionals
        ]);
    }

    public function update(ClientRequest $request, $id)
    {
        $clients = Clients::findOrFail($id);

        $clients->name = strtoupper($request->input('name'));
        $clients->description = $request->input('description');
        $clients->destacado = $request->input('destacado');

        $clients->creador = auth()->user()->id;
        $clients->message = $request->input('message');

        $_slug = mb_strtolower((str_replace(" ","-",$request->name)),'UTF-8');
        $clients->slug = preg_replace('/[^A-Za-z0-9\-]/', '', $_slug);

        $clients->image = $request->input('image');

        $clients->is_published = $request->input('is_published');

        $clients->update();

        Session::flash('update','Registro modificado con Éxito');
        return Redirect()->route('dashboard.clients.index');
    }

	public function destroy($id)
    {
        $clients = Clients::findOrFail($id);

        Session::flash('delete','Registro '.$clients->name.' eliminado con Éxito');

        $clients->delete();

        return Redirect()->route('dashboard.clients.index');
    }
}

