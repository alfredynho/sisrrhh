<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Profesional;
use Session;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfetionalRequest;
use Illuminate\Support\Facades\Storage;

class TeacherController extends Controller
{
    public function index()
    {
        $profetional = Profesional::orderBy('id', 'desc')->get();

        return view('dashboard.pages.profetionals.index', [
            'profetional' => $profetional
        ]);
    }

    public function store(ProfetionalRequest $request)
    {
        $profetional = new Profesional();

        $profetional->name = $request->get('name');
        $profetional->cargo = $request->get('cargo');
        $profetional->biography = $request->get('biography');
        $profetional->gender = $request->get('gender');
        $profetional->creathor = auth()->user()->id;
        $profetional->slug = hSlug($request->name);

        $img = $request->file('image');
        $code = generaCode();

        if ($img != null) {
            Storage::disk('public')->delete('/Profetional/' . $profetional->image);

            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($code . "." . $extension);
            \Storage::disk('local')->put('public/Profetional/' . $archivo, \File::get($img));

            $profetional->image = $code.".".$extension;
        }

        $profetional->is_published = $request->get('is_published');
        $profetional->save();

        Session::flash('create', 'Registro creado con Éxito');
        return Redirect()->route('dashboard.profetional.index');
    }


    public function update(ProfetionalRequest $request, $id)
    {
        $profetional = Profesional::findOrFail($id);

        $profetional->name = $request->input('name');
        $profetional->cargo = $request->input('cargo');
        $profetional->biography = $request->input('biography');
        $profetional->gender = $request->input('gender');
        $profetional->creador = auth()->user()->id;
        $profetional->slug = hSlug($request->name);

        $img = $request->file('image');
        $code = generaCode();

        if ($img != null) {
            Storage::disk('public')->delete('/Profetional/' . $profetional->image);

            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($code . "." . $extension);
            \Storage::disk('local')->put('public/Profetional/' . $archivo, \File::get($img));

            $profetional->image = $code . "." . $extension;
        }

        $profetional->is_published = $request->input('is_published');

        $profetional->update();

        Session::flash('update', 'Registro modificado con Éxito');
        return Redirect()->route('dashboard.profetional.index');
    }

    public function destroy($id)
    {
        $profetional = Profesional::findOrFail($id);

        Session::flash('delete', 'Registro ' . $profetional->name . ' eliminado con Éxito');
        Storage::disk('public')->delete('/Profetional/' . $profetional->image);

        $profetional->delete();

        return Redirect()->route('dashboard.profetional.index');
    }
}
