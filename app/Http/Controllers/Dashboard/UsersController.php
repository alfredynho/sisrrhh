<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $meta_property = true;

        return view('dashboard.pages.users.index', [
            'meta_property' => $meta_property,
        ]);
    }

}
