<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Ausencia;
use Illuminate\Http\Request;
use App\Models\Personal;
use App\Models\Ausencias;
use DB;
use Session;


class AusenciasController extends Controller
{

    public function index()
    {
        // $ausencias = Ausencia::all();

        // $personal = DB::table('personal')
        // ->join('cargo','personal.id_cargo','=','cargo.id')
        // ->select('personal.*','cargo.nombre as nombre_cargo')
        // ->get();


        $ausencias = DB::table('ausencias')
        ->join('personal','ausencias.personal_id','=','personal.id')
        ->select('ausencias.*','personal.nombres as nombres','personal.paterno as paterno','personal.cedula as cedula','personal.materno as materno')
        ->get();

        return view('dashboard.pages.ausencias.index', [
            'ausencias' => $ausencias,
        ]);
    }

    public function store(Request $request)
    {
        $ausencias = new Ausencia();
        $ausencias->personal_id = $request->get('personal_id');
        $ausencias->fecha_ausencia = $request->get('fecha_ausencia');
        $ausencias->justificado = $request->get('justificado');
        $ausencias->observaciones = $request->get('observaciones');
        $ausencias->save();

        Session::flash('create', 'Registro creado con Éxito');
        return Redirect()->route('dashboard.ausencias.index');
    }

    public function show($id)
    {
        $personal = Personal::where('id', '=', $id)->firstOrFail();

        return view('dashboard.pages.ausencias.show', [
            'personal' => $personal
        ]);
    }


    public function create(Request $request)
    {
        $message = "Crear Ausencias";
        $personal = Personal::all();

        return view('dashboard.pages.ausencias.create', [
            'message' => $message,
            'personal' => $personal,
        ]);
    }

    public function destroy($id)
    {
        $personal = Personal::findOrFail($id);

        Session::flash('delete', 'Registro ' . $personal->nombre . ' eliminado con Éxito');

        $personal->delete();

        return Redirect()->route('dashboard.ausencias.index');
    }
}
