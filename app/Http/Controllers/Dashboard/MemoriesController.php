<?php

namespace App\Http\Controllers\Dashboard;

use App\Memories;
use App\Models\Course;

use Illuminate\Http\Request;
use App\Http\Requests\Testimonie\TestimonieRequest;
use App\Http\Controllers\Controller;
use Session;

class MemoriesController extends Controller
{
    public function index()
    {
        $courses = Course::where('is_published','=',1)
        ->where('author','=', currentUser())
        ->get();

        $memories = Memories::orderBy('id', 'desc')->get();

        return view('dashboard.pages.memories.index', [
            'memories' => $memories,
            'courses'  => $courses
        ]);
    }

    public function store(Request $request)
    {

        $memories = new Memories();

        $memories->message = $request->get('message');
        $memories->author = $request->get('author');
        $memories->status = $request->get('status');
        $memories->save();

        Session::flash('create', 'Registro creado con Éxito');
        return Redirect()->route('dashboard.memories.index');
    }


    public function update(Request $request, $id)
    {
        $memories = Memories::findOrFail($id);

        $memories->message = $request->input('message');
        $memories->author = $request->input('author');
        $memories->status = $request->input('status');
        $memories->update();

        Session::flash('update', 'Registro modificado con Éxito');
        return Redirect()->route('dashboard.memories.index');
    }

    public function destroy($id)
    {
        $memories = Memories::findOrFail($id);

        Session::flash('delete', 'Registro ' . $memories->author . ' eliminado con Éxito');

        $memories->delete();

        return Redirect()->route('dashboard.memories.index');
    }
}
