<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Requests\UnidadRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Unidad;
use Session;



class UnidadController extends Controller
{
    public function index()
    {
        $unidades = Unidad::where('estado','=',1)->get();

        return view('dashboard.pages.unidad.index', [
            'unidades' => $unidades,
        ]);
    }

    public function store(UnidadRequest $request)
    {
        $unidades = new Unidad();

        $unidades->codigo = strtoupper($request->get('codigo'));
        $unidades->nombre = strtoupper($request->get('nombre'));
        $unidades->estado = $request->get('estado');
        $unidades->save();

        Session::flash('create', 'Registro creado con Éxito');
        return Redirect()->route('dashboard.unidad.index');
    }

    public function update(UnidadRequest $request, $id)
    {
        $unidades = Unidad::findOrFail($id);

        $unidades->codigo = strtoupper($request->input('codigo'));
        $unidades->nombre = strtoupper($request->input('nombre'));
        $unidades->estado = $request->input('estado');
        $unidades->update();

        Session::flash('update', 'Registro modificado con Éxito');
        return Redirect()->route('dashboard.unidad.index');
    }

    public function destroy($id)
    {
        $unidades = Unidad::findOrFail($id);

        Session::flash('delete', 'Registro ' . $unidades->nombre . ' eliminado con Éxito');

        $unidades->delete();

        return Redirect()->route('dashboard.unidad.index');
    }
}

