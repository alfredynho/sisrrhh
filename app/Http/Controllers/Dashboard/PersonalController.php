<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;
use App\Models\Personal;
use App\Models\Unidad;
use App\Models\Profesion;
use App\Models\Contrato;
use App\Models\Parentesco;
use App\Models\Afp;
use App\Models\Capacitacion;
use App\Models\Cargo;
use App\Models\Estudios;
use DB;
use PDF;

class PersonalController extends Controller
{
    public function index()
    {
        $personal = DB::table('personal')
        ->join('cargo','personal.id_cargo','=','cargo.id')
        ->select('personal.*','cargo.nombre as nombre_cargo')
        ->get();

        return view('dashboard.pages.personal.index', [
            'personal' => $personal,
        ]);
    }

    public function store(Request $request)
    {
        $personal = new Personal();

        // $ff = $request->get('f_ingreso');

        // dump("Diferencia ".$ff);

        // $hoy = Carbon::now();

        // dump("hora actual ".$hoy);

    //    $alf = $hoy->diffInYears( Carbon::parse($ff));

        $personal->cedula = strtoupper($request->get('cedula'));
        $personal->nombres = strtoupper($request->get('nombres'));
        $personal->paterno = strtoupper($request->get('paterno'));
        $personal->materno = strtoupper($request->get('materno'));
        $personal->caja = $request->get('caja');
        $personal->id_cargo = $request->get('id_cargo');
        $personal->id_profesion = $request->get('id_profesion');
        $personal->correo = $request->get('correo');
        $personal->fecha_ingreso = $request->get('f_ingreso');
        $personal->fecha_nacimiento = $request->get('f_nacimiento');

        $_slug = mb_strtolower((str_replace(" ","-",$request->nombres.''.$request->paterno.''.$request->materno)),'UTF-8');
        $personal->slug = preg_replace('/[^A-Za-z0-9\-]/', '', $_slug);


        $personal->estado_civil = $request->get('estado_civil');
        $personal->celular = $request->get('celular');
        $personal->genero = $request->get('genero');
        $personal->direccion = $request->get('direccion');
        $personal->afp = $request->get('afp');
        $personal->seguro = $request->get('seguro');
        $personal->save();

        Session::flash('create', 'Registro creado con Éxito');
        return Redirect()->route('dashboard.personal.index');
    }

    public function update(Request $request, $id)
    {
        $profesiones = Profesion::findOrFail($id);

        $profesiones->codigo = strtoupper($request->input('codigo'));
        $profesiones->nombre = strtoupper($request->input('nombre'));
        $profesiones->estado = $request->input('estado');

        $profesiones->update();

        Session::flash('update', 'Registro modificado con Éxito');
        return Redirect()->route('dashboard.profesion.index');
    }

    public function destroy($id)
    {
        $personal = Personal::findOrFail($id);

        Session::flash('delete', 'Registro ' . $personal->nombre . ' eliminado con Éxito');

        $personal->delete();

        return Redirect()->route('dashboard.personal.index');
    }


    public function show($id)
    {
        $personal = Personal::where('id', '=', $id)->firstOrFail();

        return view('dashboard.pages.personal.show', [
            'personal' => $personal
        ]);
    }


    public function create(Request $request)
    {
        $message = "Crear Personal";
        $unidad = Unidad::all();
        $profesion = Profesion::all();
        $contrato = Contrato::all();

        $cargos = DB::table('cargo')
        ->join('unidad','cargo.id_unidad','=','unidad.id')
        ->select('cargo.*','unidad.nombre as nombre_unidad')
        ->orderBy('cargo.id','desc')->get();

        return view('dashboard.pages.personal.create', [
            'message' => $message,
            'unidad' => $unidad,
            'profesion' => $profesion,
            'contrato' => $contrato,
            'cargos'  => $cargos
        ]);
    }

    public function detalle($id){

        $message = 'Detalle de Personal';

        $personal = DB::table('personal')
        ->join('cargo','personal.id_cargo','=','cargo.id')
        ->join('profesiones','personal.id_profesion','=','profesiones.id')
        ->select('personal.*','cargo.nombre as nombre_cargo','profesiones.nombre as nombre_profesion')
        ->where('personal.id', '=', $id)->first();

        $ausencias = DB::table('ausencias')
        ->join('personal','ausencias.personal_id','=','personal.id')
        ->select('ausencias.*')
        ->where('personal.id', '=', $id)->get();

        $estudios = Estudios::all();

        return view('dashboard.pages.personal.detalle',[
            'personal' => $personal,
            'ausencias' => $ausencias,
            'message' => $message,
            'estudios' => $estudios,
        ]);
    }

    public function reporte_funcionario($id){

        $personal = DB::table('personal')
        ->join('cargo','personal.id_cargo','=','cargo.id')
        ->select('personal.*','cargo.nombre as nombre_cargo')
        ->where('personal.id', '=', $id)->first();

        $ausencias = DB::table('ausencias')
        ->join('personal','ausencias.personal_id','=','personal.id')
        ->select('ausencias.*')
        ->where('personal.id', '=', $id)->get();

        $pdf = PDF::loadView('dashboard.pages.personal.funcionario',compact('personal','ausencias'));
        // ->setPaper('a4', 'landscape');
        // ->setPaper('a4', 'landscape');
        return $pdf->stream($personal->nombres.'.pdf');
    }
}
