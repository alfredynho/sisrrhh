<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contrato;
use Session;

use App\Http\Requests\ContratoRequest;

class ContratosController extends Controller
{
    public function index()
    {
        $contratos = Contrato::where('estado','=',1)->get();

        return view('dashboard.pages.contrato.index', [
            'contratos' => $contratos,
        ]);
    }

    public function store(ContratoRequest $request)
    {
        $contratos = new Contrato();

        $contratos->codigo = strtoupper($request->get('codigo'));
        $contratos->tipo = strtoupper($request->get('tipo'));

        $contratos->estado = $request->get('estado');
        $contratos->save();

        Session::flash('create', 'Registro creado con Éxito');
        return Redirect()->route('dashboard.contrato.index');
    }


    public function update(ContratoRequest $request, $id)
    {
        $contratos = Contrato::findOrFail($id);

        $contratos->codigo = strtoupper($request->input('codigo'));
        $contratos->tipo = strtoupper($request->input('tipo'));
        $contratos->estado = $request->input('estado');

        $contratos->update();

        Session::flash('update', 'Registro modificado con Éxito');
        return Redirect()->route('dashboard.contrato.index');
    }

    public function destroy($id)
    {
        $contratos = Contrato::findOrFail($id);

        Session::flash('delete', 'Registro ' . $contratos->tipo . ' eliminado con Éxito');

        $contratos->delete();

        return Redirect()->route('dashboard.contrato.index');
    }
}