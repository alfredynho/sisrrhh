 <?php

    use Carbon\Carbon;
    use App\Social;
    use App\Question;
    use App\Blog;
    use App\Models\Country;
    use App\Models\Cities;


    function generaCode()
    {
        $uniqid = uniqid();
        $rand_start = rand(1,5);
        $name_image = substr($uniqid,$rand_start,8);

        return $name_image;
    }

    function formaterDate($date)
    {
        $form_date = Carbon::createFromFormat('m-d-Y', $date);
        $format_date = $form_date->format('Y-m-d');

        return $format_date;
    }

    function getSocial($name)
    {
        try {
            $url_social = Social::where('name', $name)->firstOrFail();

            if ($url_social->status == 1) {
                return $url_social->url;
            } else {
                return 'https://www.cemtic.com.bo';
            }

        } catch (\Exception $error) {
            return 'https://www.cemtic.com.bo';
        }
    }

    function BlogDestacado()
    {
        $responses = Blog::where('destacado', '1')->count();
        return $responses;
    }

    function getBlogs()
    {
        $blogs = Blog::orderBy('id', 'asc')->get()->take(3);
        return $blogs;
    }

    function mesLiteral($month)
    {
        switch ($month) {
            case "1":  return "Enero";      break;
            case "2":  return "Febrero";    break;
            case "3":  return "Marzo";      break;
            case "4":  return "Abril";      break;
            case "5":  return "Mayo";       break;
            case "6":  return "Junio";      break;
            case "7":  return "Julio";      break;
            case "8":  return "Agosto";     break;
            case "9":  return "Septiembre"; break;
            case "10": return "Octubre";    break;
            case "11": return "Noviembre";  break;
            case "12": return "Diciembre";  break;
            default:
                echo "Mes";
        }
    }

    function cantidadRegFaqs($id)
    {
        $c_questions = Question::where('action_id','=',$id)->count();

        return $c_questions;
    }

    function LongTimeFilter($date) {
        if ($date == null) {
            return "Sin fecha";
        }

        $start_date = $date;
        $since_start = $start_date->diff(new \DateTime(date("Y-m-d") . " " . date("H:i:s")));

        if ($since_start->y == 0) {
            if ($since_start->m == 0) {
                if ($since_start->d == 0) {
                    if ($since_start->h == 0) {
                        if ($since_start->i == 0) {
                            if ($since_start->s == 0) {
                                $result = $since_start->s . ' segundos';
                            } else {
                                if ($since_start->s == 1) {
                                    $result = $since_start->s . ' segundo';
                                } else {
                                    $result = $since_start->s . ' segundos';
                                }
                            }
                        } else {
                            if ($since_start->i == 1) {
                                $result = $since_start->i . ' minuto';
                            } else {
                                $result = $since_start->i . ' minutos';
                            }
                        }
                    } else {
                        if ($since_start->h == 1) {
                            $result = $since_start->h . ' hora';
                        } else {
                            $result = $since_start->h . ' horas';
                        }
                    }
                } else {
                    if ($since_start->d == 1) {
                        $result = $since_start->d . ' día';
                    } else {
                        $result = $since_start->d . ' días';
                    }
                }
            } else {
                if ($since_start->m == 1) {
                    $result = $since_start->m . ' mes';
                } else {
                    $result = $since_start->m . ' meses';
                }
            }
        } else {
            if ($since_start->y == 1) {
                $result = $since_start->y . ' año';
            } else {
                $result = $since_start->y . ' años';
            }
        }

        return "Hace " . $result;
    }


    function currentUser()
    {
        return auth()->user()->id;
    }


    function hSlug($name)
    {
        $_slug = mb_strtolower((str_replace(" ","-",$name)),'UTF-8');
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $_slug);
        return $slug;
    }

    function nameFile($file, $slug)
    {
        $extension = $file->getClientOriginalExtension();
        $extension = strtolower($extension);
        $archive = strtolower($slug.".".$extension);
        return $archive;
    }


    function flurryEffect()
    {
        try {
            $flutty = Social::where('name', 'NAVIDAD')->firstOrFail();

            if ($flutty->status == 1) {
                return 'decorator';
            } else {
                return "navidad";
            }

        } catch (\Exception $error) {
            return "Feliz Navidad!!";
        }
    }

