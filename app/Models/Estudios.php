<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estudios extends Model
{
    protected $table = 'estudios';

    protected $fillable = [
        'institucion','mencion','grado','profesion','id_personal'
    ];
}
