<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryInso extends Model
{
    protected $table = 'categoryinsos';

    protected $fillable = [
        'name','creathor','trigger','status'
    ];
}
