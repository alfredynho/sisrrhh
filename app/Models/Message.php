<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'phrases';

    protected $fillable = [
        'author','message','is_published'
    ];
}
