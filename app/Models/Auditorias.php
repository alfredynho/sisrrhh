<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Auditorias extends Model
{
    protected $table = 'auditorias';

    protected $fillable = [
        'title','file','creador','is_published'
    ];
}
