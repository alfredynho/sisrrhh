<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

    protected $fillable = [
        'id',
        'title',
        'description',
        'content',
        'type',
        'author',
        'slug',
        'finished_at',
        'is_published',
        'id_teacher',
        'form',
        'destacado',
        'date',
        'is_new',
        'numparticipants',
        'duration',
        'level',
        'certification',
        'price',
        'modules',
    ];

}
