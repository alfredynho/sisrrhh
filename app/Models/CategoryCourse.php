<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryCourse extends Model
{
    protected $table = 'categorycourses';

    protected $fillable = [
        'title','description','status'
    ];
}


