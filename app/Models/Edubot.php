<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Edubot extends Model
{
    protected $table = 'questions';

    protected $fillable = [
        'action_id', 'answer', 'question', 'is_published'
    ];

}
