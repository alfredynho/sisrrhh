<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biblioteca extends Model
{
    protected $table = 'bibliotecas';

    protected $fillable = [
        'title', 'resto_del_titulo','author','edicion','lugar_publicacion','nombre_editor','fecha_publicacion','coda','codb',
        'codq',  'isbn', 'barcode','paginas','dimensiones','ubicacion'
    ];

}
