<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Convenios extends Model
{
    protected $table = 'convenios';

    protected $fillable = [
        'title','description','creador','image','is_published','slug'
    ];
}
