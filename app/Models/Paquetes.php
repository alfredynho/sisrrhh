<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paquetes extends Model
{
    protected $table = 'paquetes';

    protected $fillable = [
        'title','description','services','creador','is_published','promotion','slug'
    ];
}
