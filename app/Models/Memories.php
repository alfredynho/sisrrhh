<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Memories extends Model
{
    protected $table = 'memories';

    protected $fillable = [
        'message','author','grade','status'
    ];
}
