<?php

namespace App\Models;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
    protected $table = 'personal';

    protected $fillable = [
        'cedula',
        'nombres',
        'paterno',
        'materno',
        'fecha_nacimiento',
        'caja',
        'item',
        'slug',
        'modalidad',
        'id_cargo',
        'id_profesion',
        'correo',
        'fecha_ingreso',
        'estado_civil',
        'celular',
        'genero',
        'direccion',
        'afp',
        'seguro',
    ];

    public function fecha_ingreso()
    {
        $f_ingreso = $this->f_ingreso;
        $hoy = Carbon::now();
        return $hoy->diffInYears( Carbon::parse($f_ingreso));
    }
}