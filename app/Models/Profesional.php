<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profesional extends Model
{
    protected $table = 'profetionals';

    protected $fillable = [
        'id','name','grado','biography','creathor','gender','image','is_published'
    ];

}
