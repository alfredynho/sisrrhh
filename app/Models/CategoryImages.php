<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryImages extends Model
{
    protected $table = 'categoryimages';

    protected $fillable = [
        'name','creathor','slug','status'
    ];
}
