<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agreements extends Model
{

    protected $table = 'agreements';

    protected $fillable = [
        'title', 'content', 'image', 'creador', 'is_published', 'slug'
    ];

}
