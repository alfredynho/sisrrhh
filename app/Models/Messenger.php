<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messenger extends Model
{
    protected $fillable = [
        'user_id','first_name','last_name','gender','timezone','image','registration_date','status'
    ];
}
