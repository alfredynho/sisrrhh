<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    protected $table = 'unidad';

    protected $fillable = [
        'codigo','nombre','estado','id_cargo'
    ];
}
