<?php

use Illuminate\Database\Seeder;
use App\Models\Convenios;
use Carbon\Carbon;


class ConveniosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Convenios::truncate();

        $handle = fopen(storage_path('convenios.csv'), "r");

        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            Convenios::create([
                'id' => $csvLine[0],
                'title' => $csvLine[1],
                'description' => $csvLine[2],
                'image' => $csvLine[3],
                'creador' => $csvLine[4],
                'slug' => $csvLine[5],
                'is_published' => $csvLine[6],
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
