<?php

use Illuminate\Database\Seeder;
use App\Models\Normativas;
use Carbon\Carbon;

class NormativaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Normativas::truncate();

        $handle = fopen(storage_path('normativas.csv'), "r");

        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            Normativas::create([
                "id" => $csvLine[0],
                'title' => $csvLine[1],
                'creador' => $csvLine[2],
                'file' => $csvLine[3],
                'is_published' => $csvLine[4],
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
    }
