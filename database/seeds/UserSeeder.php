<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // DB::table('users')->truncate();

        $handle = fopen(storage_path('users.csv'), "r");

        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            User::create([
                'id'   => $csvLine[0],
                'name' => $csvLine[1],
                'paterno' => $csvLine[2],
                'materno' => $csvLine[3],
                'email' => $csvLine[4],
                'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'password' => bcrypt($csvLine[5]), // password
                'bloqueado' => $csvLine[6],
                'is_admin'  => $csvLine[7],
            ]);
            //Cualquier registro que tenga la columna 7 sera registrado como ADMINISTRADOR
            // if (isset($csvLine[7])) {
            //     $user->rol()->attach('ADM');
            // }
        }
    }
}


