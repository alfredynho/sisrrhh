<?php

use Illuminate\Database\Seeder;
use App\Social;
use Carbon\Carbon;

class SocialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Social::truncate();

        $handle = fopen(storage_path('socials.csv'), "r");

        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            Social::create([
                "id" => $csvLine[0],
                'name' => $csvLine[1],
                'url' => $csvLine[2],
                'status' => $csvLine[3],
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
