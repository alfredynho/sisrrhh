<?php

use Illuminate\Database\Seeder;
use App\Models\Paquetes;
use Carbon\Carbon;

class PaquetesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Paquetes::truncate();

        $handle = fopen(storage_path('paquetes.csv'), "r");

        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            Paquetes::create([
                "id" => $csvLine[0],
                'title' => $csvLine[1],
                'description' => $csvLine[2],
                'services' => $csvLine[3],
                'creador' => $csvLine[4],
                'slug' => $csvLine[5],
                'is_published' => $csvLine[6],
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
