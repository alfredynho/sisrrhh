<?php

use Illuminate\Database\Seeder;
use App\Memories;
use Carbon\Carbon;

class MemoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Memories::truncate();

        $handle = fopen(storage_path('memories.csv'), "r");

        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            Memories::create([
                "id" => $csvLine[0],
                'message' => $csvLine[1],
                'grade' => $csvLine[2],
                'author'  => $csvLine[3],
                'status' => $csvLine[4],
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
