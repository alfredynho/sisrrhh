<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class AuthoritiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        for ($i = 0; $i < 20; $i++) {         
            DB::table('authorities')->insert([
                'name' => Str::random(10),
                'cargo' => Str::random(10),
                'biography' => '<p> Nacido el 17 de junio de 1985 en la Prov. Nor Chichas del departamento de Potosí, actualmente tiene 35 años.
                                    Primaria.- En la Escuela Simón Bolívar de Toropalca Nor Chichas.
                                    Secundaria.- Colegio Carlos Medinaceli de la ciudad de Potosí.
                                    Egresado de la Carrera de Ingeniería Civil de la Universidad Autónoma Tomás Frías de Potosí.</p>',
                'creathor' => 1,
                'is_published' => 1,
                'slug' => Str::random(25),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }

}
