<?php

use Illuminate\Database\Seeder;
use App\Blog;
use Carbon\Carbon;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Blog::truncate();

        $handle = fopen(storage_path('blogs.csv'), "r");

        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            Blog::create([
                "id" => $csvLine[0],
                'title' => $csvLine[1],
                'description' => $csvLine[2],
                'category' => $csvLine[3],
                'creador' => $csvLine[4],
                'slug' => $csvLine[5],
                'destacado' => $csvLine[6],
                'is_published' => $csvLine['7'],
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
