<?php

use App\Memories;
use App\Models\Cargo;
use App\Models\Profesion;
use App\Profetional;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DbSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            //ProfetionalSeeder::class,
            //CourseSeeder::class,
            ProfesionesSeeder::class,
            UnidadSeeder::class,
            CargoSeeder::class,
            UsersTableSeeder::class,
            //UserSeeder::class
        ]);

    }
}
