<?php

use Illuminate\Database\Seeder;
use App\Models\Cargo;
use Carbon\Carbon;
use Illuminate\Support\Str;


class CargoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $handle = fopen(storage_path('cargos.csv'), "r");
        $cont = 1;
        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            Cargo::create([
                'id' => $cont,
                'nombre' => $csvLine[0],
                'estado' => $csvLine[3],
                'id_unidad' => $csvLine[2],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        $cont = $cont + 1;
        }
    }
}
