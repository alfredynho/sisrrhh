<?php

use Illuminate\Database\Seeder;

use App\Models\Profesional;
use Carbon\Carbon;

class ProfetionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Profesional::truncate();

        $handle = fopen(storage_path('profesionales.csv'), "r");

        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            Profesional::create([
                "id" => $csvLine[0],
                'name' => $csvLine[1],
                'grado' => $csvLine[2],
                'biography' => $csvLine[3],
                'creathor' => $csvLine[4],
                'is_published' => $csvLine[5],
                'slug' => $csvLine[6],
                'gender' => $csvLine[7],
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
