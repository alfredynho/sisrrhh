<?php

use Illuminate\Database\Seeder;
use App\Models\Course;
use Carbon\Carbon;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Course::truncate();

        $handle = fopen(storage_path('courses.csv'), "r");

        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            Course::create([
                'id' => $csvLine[0],
                'title' => $csvLine[1],
                'description' => $csvLine[2],
                'content' =>  $csvLine[3],
                'type' => $csvLine[4],
                'author' => $csvLine[5],
                'slug' => $csvLine[6],
                'quota' => $csvLine[7],
                'cost' => $csvLine[8],
                'is_cost' => $csvLine[9],
                'image' => $csvLine[10],
                'is_published' => $csvLine[11],
                'finished_at' => Carbon::now()->format('Y-m-d H:i:s'),
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
