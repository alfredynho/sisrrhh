<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Jaime',
            'paterno' => 'Flores',
            'materno' => 'Pereira',
            'email' => 'jaime@email.com',
            'password' => bcrypt('jaime12345'),
            'bloqueado' => 1,
            'is_admin' => 1
        ]);

    }
}
