<?php

use Illuminate\Database\Seeder;
use App\Models\Regionals;
use Carbon\Carbon;

class RegionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Regionals::truncate();

        $handle = fopen(storage_path('regionales.csv'), "r");

        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            Regionals::create([
                "id" => $csvLine[0],
                'name' => $csvLine[1],
                'in_charge' => $csvLine[2],
                'description' => $csvLine[3],
                'latitude' => $csvLine[4],
                'longitude' => $csvLine[5],
                'is_published' => $csvLine[6],
                'slug' => $csvLine[7],
                'creathor' => $csvLine[8],
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
