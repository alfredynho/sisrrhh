<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cedula')->index()->unique();
            $table->string('nombres');
            $table->string('paterno');
            $table->string('materno');
            $table->date('fecha_nacimiento');	
            $table->string('caja');
            // $table->string('item');
            $table->string('slug');
            // $table->string('modalidad'); // Refiere a si es de Planta. Eventual, Consultor en Linea o compra de servicio.
            $table->integer('id_cargo')->unsigned();
            $table->foreign('id_cargo')->references('id')->on('cargo')->onDelete('cascade');

            $table->integer('id_profesion')->unsigned();
            $table->foreign('id_profesion')->references('id')->on('profesiones')->onDelete('cascade');

            $table->string('correo');

            $table->date('fecha_ingreso');	

            // $table->integer('id_contrato')->unsigned();
            // $table->foreign('id_contrato')->references('id')->on('contratos')->onDelete('cascade');

            $table->boolean('estado_civil')->default(0);

            $table->string('celular');

            // $table->integer('id_experiencial')->unsigned();
            // $table->foreign('id_experiencial')->references('id')->on('experiencialaboral')->onDelete('cascade');
            
            $table->boolean('genero')->default(1);

            // $table->integer('id_parentesco')->unsigned();
            // $table->foreign('id_parentesco')->references('id')->on('parentesco')->onDelete('cascade');

            $table->longText('direccion');

            // $table->integer('id_afp')->unsigned();
            // $table->foreign('id_afp')->references('id')->on('afp')->onDelete('cascade');
            $table->boolean('afp')->default(1);

            $table->boolean('seguro')->default(1);

            // $table->integer('id_capacitacion')->unsigned();
            // $table->foreign('id_capacitacion')->references('id')->on('capacitacion')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal');
    }
}
