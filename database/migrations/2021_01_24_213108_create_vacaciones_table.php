<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacaciones', function (Blueprint $table) {
            $table->softDeletes();
            $table->increments('id');
            $table->unsignedBigInteger('personal_id')->unsigned();
            $table->foreign('personal_id')->references('id')->on('personal');
            $table->integer('cantidad_dias');
            $table->date('fecha_inicio');
            $table->date('fecha_finalizacion');
            $table->string('observaciones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacaciones');
    }
}
