<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBibliotecasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bibliotecas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->string('resto_del_titulo',255);
            $table->string('author', 255);
            $table->string('slug')->index()->unique();
            $table->string('edicion',255);
            $table->string('lugar_publicacion',255);
            $table->string('nombre_editor',255);
            $table->string('fecha_publicacion',255);
            $table->string('coda',150);
            $table->string('codb',150);
            $table->string('codq',150);
            $table->string('isbn',150);
            $table->string('barcode',200);
            $table->string('paginas',200);
            $table->string('dimensiones',120);
            $table->string('ubicacion',120);
            $table->boolean('destacado')->default(1);
            $table->boolean('is_published')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bibliotecas');
    }
}
